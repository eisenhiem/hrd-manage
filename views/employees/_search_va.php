<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Departments;

use kartik\date\DatePicker;

$dep = ArrayHelper::map(Departments::find()->all(), 'dep_id', 'dep_name');

/* @var $this yii\web\View */
/* @var $model app\models\EmployeesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employees-search">

    <?php $form = ActiveForm::begin([
        'action' => ['vacation'],
        'method' => 'get',
    ]); ?>

    <?php //= $form->field($model, 'emp_id') ?>

    <?php //= $form->field($model, 'pname') ?>

    <?php //= $form->field($model, 'fname') ?>

    <?php //= $form->field($model, 'lname') ?>

    <?php //= $form->field($model, 'position') ?>

    <?php // echo $form->field($model, 'level') ?>

    <?php // echo $form->field($model, 'position_type') ?>

    <?php // echo $form->field($model, 'birthdate') ?>

    <?php // echo $form->field($model, 'start_date') ?>

    <?php // echo $form->field($model, 'education') ?>

    <?php // echo $form->field($model, 'branch') ?>

    <?= $form->field($model, 'dep_id')->dropDownList($dep,['prompt'=>''])  ?>

    <?php // echo $form->field($model, 'job1') ?>

    <?php // echo $form->field($model, 'job2') ?>

    <?php // echo $form->field($model, 'job3') ?>

    <?php // echo $form->field($model, 'job4') ?>

    <?php // echo $form->field($model, 'job5') ?>

    <?php // echo $form->field($model, 'job6') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
