<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */

$this->title = $model->fname;
$this->params['breadcrumbs'][] = ['label' => 'บุคลากร', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="employees-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->emp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบข้อมูล', ['delete', 'id' => $model->emp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <b>ชื่อ - นามสกุล</b> <?= $model->pname.$model->fname.' '.$model->lname ?><br>
<b>ตำแหน่ง</b> <?= $model->position ?> <b>ระดับ</b> <?= $model->getLevelName() ?> <b>ประเภทตำแหน่ง</b> <?= $model->getTypeName() ?><br>
<b>อายุ</b> <?= $model->getAge()?> ปี <b>อายุราชการ</b> <?= $model->getAgeJob() ?> ปี <b>อายุราชการที่เหลือ</b> <?= 60-$model->getAge() ?> ปี<br>
<b>วุฒิการศึกษาสูงสุด</b> <?= $model->education ?> <b>สาขา</b> <?= $model->branch ?><br>
<b>กลุ่มงาน/งาน</b> <?= $model->getDepName() ?><br>
<b>งานที่รับผิดชอบ</b><br>
<div class="col-md-6">
    1. <?= $model->job1 ?>
</div>
<div class="col-md-6">
    2. <?= $model->job2 ?>
</div>
<div class="col-md-6">
    3. <?= $model->job3 ?>
</div>
<div class="col-md-6">
    4. <?= $model->job4 ?>
</div>
<div class="col-md-6">
    5. <?= $model->job5 ?>
</div>
<div class="col-md-6">
    6. <?= $model->job6 ?>
</div>
</p>

</div>
</br> 

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'plan_id',
//            'emp_id',
//            'r_id',
            'plan_development:ntext',
            'deverlop_by',
            'duration',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p>
        <?= Html::a('เพิ่มแผนพัฒนา', ['plan/create','emp_id'=>$model->emp_id], ['class' => 'btn btn-success']) ?>
    </p>