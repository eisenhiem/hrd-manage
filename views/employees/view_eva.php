<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Plan;
$plan = Plan::find()->where(['emp_id' => $model->emp_id])->all();
use app\models\Kpi;
$kpi = Kpi::find()->where(['emp_id' => $model->emp_id])->all();
use app\models\Competency;
$comp = Competency::find()->where(['emp_id' => $model->emp_id])->all();
use app\models\Rounds;
$round = Rounds::find()->all();
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */

$this->title = $model->fname;
$this->params['breadcrumbs'][] = ['label' => 'บุคลากร', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="employees-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->emp_id], ['class' => 'btn btn-primary']) ?>
    </p>


<div class="employees-view">

<b>ชื่อ - นามสกุล</b> <?= $model->pname.$model->fname.' '.$model->lname ?><br>
<b>ตำแหน่ง</b> <?= $model->position ?> <b>ระดับ</b> <?= $model->getLevelName() ?> <b>ประเภทตำแหน่ง</b> <?= $model->getTypeName() ?><br>
<b>อายุ</b> <?= $model->getAge()?> ปี <b>อายุราชการ</b> <?= $model->getAgeJob() ?> ปี <b>อายุราชการที่เหลือ</b> <?= 60-$model->getAge() ?> ปี<br>
<b>วุฒิการศึกษาสูงสุด</b> <?= $model->education ?> <b>สาขา</b> <?= $model->branch ?><br>
<b>กลุ่มงาน/งาน</b> <?= $model->getDepName() ?></br>
</p>

</div>
<?php 
    foreach($round as $r){ 
        $total_score = 0;
        $total_weight = 0;
        $core_score = 0;
        $core_weight = 0;
        $i=1;
        $by=0;
?>
    <hr>
    <table width="100%" border="1">
        <tr>
            <td style="text-align:center" colspan="10">
                <b><?= $r->r_detail ?></b>
            </td>
        </tr>
        <tr>
            <th style="text-align:center" width="5%">ลำดับ</th>
            <th style="text-align:center" width="35%">ตัวชี้วัด</th>
            <th style="text-align:center" width="5%">1</th>
            <th style="text-align:center" width="5%">2</th>
            <th style="text-align:center" width="5%">3</th>
            <th style="text-align:center" width="5%">4</th>
            <th style="text-align:center" width="5%">5</th>
            <th style="text-align:center" width="10%">คะแนน(ก)</th>
            <th style="text-align:center" width="10%">คะแนน(ข) (%)</th>
            <th style="text-align:center" width="15%">คะแนนรวม(ค)(ค=กxข)</th>
        </tr>
<?php foreach($kpi as $k) {
    if($k->r_id == $r->r_id){
        $total_weight = $total_weight+$k->kpi_weight;
        $total_score = $total_score+$k->kpi_total;
        $by = $k->evaluate_by;
        echo "<tr><td style='text-align:center'>".$k->kpi_no."</td>";
        echo "<td>".$k->kpi_name."</td>";
        echo "<td style='text-align:center'>".$k->kpi_1."</td>";
        echo "<td style='text-align:center'>".$k->kpi_2."</td>";
        echo "<td style='text-align:center'>".$k->kpi_3."</td>";
        echo "<td style='text-align:center'>".$k->kpi_4."</td>";
        echo "<td style='text-align:center'>".$k->kpi_5."</td>";
        echo "<td style='text-align:center'>".$k->kpi_score."</td>";
        echo "<td style='text-align:center'>".$k->kpi_weight." %</td>";
        echo "<td style='text-align:center'>".$k->kpi_total."</td></tr>";
    }
}
?>
        <tr>
            <td colspan="8" style="text-align:center">รวม</td>
            <td style='text-align:center'><?= $total_weight ?> %</td>
            <td style='text-align:center'><?= $total_score ?></td>
        </tr>
        <tr>
            <td colspan="9">
                แปลงคะแนนรวม(ค) ข้างต้น เป็นคะแนนการประเมินผลสัมฤทธิ์ของงานที่มีฐานคะแนนเต็มเป็น 100 คะแนน (โดยนำ 20 มาคูณ)
            </td>
            <td style='text-align:center'><?= $total_score*20 ?></td>
        </tr>
    </table>
    <br>
    <table width="100%" border="1">
        <tr>
            <th style="text-align:center" width="5%">ลำดับ</th>
            <th style="text-align:center" width="35%">สมรรถนะ</th>
            <th style="text-align:center" width="10%">ระดับที่คาดหวัง</th>
            <th style="text-align:center" width="10%">คะแนน(ก)</th>
            <th style="text-align:center" width="10%">น้ำหนัก(ข)</th>
            <th style="text-align:center" width="10%">คะแนนรวม(ค)(ค=กxข)</th>
            <th style="text-align:center" width="20%">บันทึกการประเมินโดยผู้ประเมิน(ถ้ามี)และในกรณีให้บันทึกลงในเอกสารหน้าหลัง</th>
        </tr>
<?php 
foreach($comp as $c) {
    if($c->r_id == $r->r_id){
        $core_weight = $core_weight+$c->weight;
        $core_score = $core_score+$c->total;
        echo "<tr><td style='text-align:center'>".$c->core_id."</td>";
        echo "<td>".$c->getCoreName()."</td>";
        echo "<td style='text-align:center'>".$c->level."</td>";
        echo "<td style='text-align:center'>".$c->evaluate."</td>";
        echo "<td style='text-align:center'>".$c->weight."</td>";
        echo "<td style='text-align:center'>".$c->total."</td>";
        echo "<td style='text-align:center'>".$c->comment."</td></tr>";
    }
}
?>
        <tr>
            <td colspan="4" style="text-align:center">รวม</td>
            <td style='text-align:center'><?= $core_weight ?> %</td>
            <td style='text-align:center'><?= $core_score ?></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5">
                แปลงคะแนนรวม(ค) ข้างต้น เป็นคะแนนการประเมินผลสัมฤทธิ์ของงานที่มีฐานคะแนนเต็มเป็น 100 คะแนน (โดยนำ 20 มาคูณ)
            </td>
            <td style='text-align:center'><?= $core_score*20 ?></td>
            <td></td>
        </tr>
    </table>
    <br>
    <table width="100%" border="1">
        <tr>
            <th style="text-align:center" width="5%">ลำดับ</th>
            <th style="text-align:center" width="35%">ความรู้/ทักษะ/สมรรถนะ ที่ต้องได้รับการพัฒนา</th>
            <th style="text-align:center" width="10%">วิธีการพัฒนา</th>
            <th style="text-align:center" width="10%">ช่วงเวลาที่ต้องการพัฒนา</th>
        </tr>
<?php 
foreach($plan as $p) {
    if($p->r_id == $r->r_id){
        echo "<tr><td style='text-align:center'>".$i."</td>";
        echo "<td>".$p->plan_development."</td>";
        echo "<td style='text-align:center'>".$p->deverlop_by."</td>";
        echo "<td style='text-align:center'>".$p->duration."</td></tr>";
    }
}
?>
    </table>
    <br>
    ผู้บังคับบัญชา/ผู้ประเมิน 
<?php 
    if($by <> 0){
        $eva = User::findOne($by);
        echo $eva->getName();
        echo "<div align='center'>".Html::a('พิมพ์', ['print', 'emp_id' => $model->emp_id,'r_id' => $r->r_id],['class' => 'btn btn-info','style' => ['width'=>'240px']])."</div>"; 
    }
    }
?>
