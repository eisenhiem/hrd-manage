<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\Eva;

$eva = Eva::find()->where(['emp_id' => $model->emp_id])->all();
/* @var $this yii\web\View */
/* @var $model app\models\Employees */

$this->title = $model->fname;
$this->params['breadcrumbs'][] = ['label' => 'บุคลากร', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="employees-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->emp_id], ['class' => 'btn btn-primary']) ?>
    </p>
    <div class="panel panel-default">
        <div class="panel-body">
            ข้อมูลส่วนตัว
        </div>
        <div class="panel-footer">


            <b>ชื่อ - นามสกุล</b> <?= $model->pname . $model->fname . ' ' . $model->lname ?><br>
            <b>ตำแหน่ง</b> <?= $model->position ?> <b>ระดับ</b> <?= $model->getLevelName() ?> <b>ประเภทตำแหน่ง</b> <?= $model->getTypeName() ?><br>
            <b>อายุ</b> <?= $model->getAge() ?> ปี <b>อายุราชการ</b> <?= $model->getAgeJob() ?> ปี <b>อายุราชการที่เหลือ</b> <?= 60 - $model->getAge() ?> ปี<br>
            <b>วุฒิการศึกษาสูงสุด</b> <?= $model->education ?> <b>สาขา</b> <?= $model->branch ?><br>
            <b>กลุ่มงาน/งาน</b> <?= $model->getDepName() ?><br>
        </div>
    </div>
    <b>งานที่รับผิดชอบ</b><br>
    <div class="col-md-6">
        1. <?= $model->job1 ?>
    </div>
    <div class="col-md-6">
        2. <?= $model->job2 ?>
    </div>
    <div class="col-md-6">
        3. <?= $model->job3 ?>
    </div>
    <div class="col-md-6">
        4. <?= $model->job4 ?>
    </div>
    <div class="col-md-6">
        5. <?= $model->job5 ?>
    </div>
    <div class="col-md-6">
        6. <?= $model->job6 ?>
    </div>
    </p>

</div>
<?php
foreach ($eva as $r) { ?>
    <hr>
    <table width="100%">
        <tr>
            <td style="text-align:center" colspan="7">
                <b><?= $r->getRoundName() ?></b>
            </td>
            <td style="text-align:right">
                <?= Html::a('พิมพ์', ['eva/print', 'id' => $r->eva_id], ['class' => 'btn btn-info']) ?>
            </td>
        </tr>
        <tr bgcolor="Cornsilk">
            <th style="text-align:center" width="20%">ความรู้/ทักษะ</th>
            <th style="text-align:center" width="8%">ค่ามาตรฐาน</th>
            <th style="text-align:center" width="8%">ระดับผลการประเมิน</th>
            <th style="text-align:center" width="4%">Gap</th>
            <th style="text-align:center" width="20%">ประเด็น</th>
            <th style="text-align:center" width="10%">พัฒนาด้วย<br>ตนเอง</th>
            <th style="text-align:center" width="10%">พัฒนาด้วย<br>ผู้บังคับบัญชา</th>
            <th style="text-align:center" width="20%">อบรม</th>
        </tr>
        <tr>
            <td colspan="8" bgcolor="Navy">
                <font size=3 color="white">&nbsp &nbsp<u><b>ความรู้</b></u></font>
            </td>
        </tr>
        <tr>
            <td>ความรู้ที่จำเป็นในการปฏิบัติงานตามตำแหน่ง</td>
            <td style="text-align:center">
                <?= $r->k1_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->k1_eva_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->k1_gap ?>
            </td>
            <td>
                <?= $r->k1_subject ?>
            </td>
            <td style="text-align:center">
                <?= $r->k1_self ?>
            </td>
            <td style="text-align:center">
                <?= $r->k1_boss ?>
            </td>
            <td>
                <?= $r->k1_workshop ?>
            </td>
        </tr>
        <tr>
            <td>ความรู้เกี่ยวกับกฎหมายและระเบียบราชการ</td>
            <td style="text-align:center">
                <?= $r->k2_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->k2_eva_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->k2_gap ?>
            </td>
            <td>
                <?= $r->k2_subject ?>
            </td>
            <td style="text-align:center">
                <?= $r->k2_self ?>
            </td>
            <td style="text-align:center">
                <?= $r->k2_boss ?>
            </td>
            <td>
                <?= $r->k2_workshop ?>
            </td>
        </tr>
        <tr>
            <td colspan="8" bgcolor="Navy">
                <font size=3 color="white">&nbsp &nbsp<u><b>ทักษะ</b></u></font>
            </td>
        </tr>
        <tr>
            <td>การใช้คอมพิวเตอร์</td>
            <td style="text-align:center">
                <?= $r->com_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->com_eva_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->com_gap ?>
            </td>
            <td>
                <?= $r->com_subject ?>
            </td>
            <td style="text-align:center">
                <?= $r->com_self ?>
            </td>
            <td style="text-align:center">
                <?= $r->com_boss ?>
            </td>
            <td>
                <?= $r->com_workshop ?>
            </td>
        <tr>
            <td>การใช้ภาษาอังกฤษ</td>
            <td style="text-align:center">
                <?= $r->eng_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->eng_eva_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->eng_gap ?>
            </td>
            <td>
                <?= $r->eng_subject ?>
            </td>
            <td style="text-align:center">
                <?= $r->eng_self ?>
            </td>
            <td style="text-align:center">
                <?= $r->eng_boss ?>
            </td>
            <td>
                <?= $r->eng_workshop ?>
            </td>
        </tr>
        <tr>
            <td>การคำนวน</td>
            <td style="text-align:center">
                <?= $r->cal_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->cal_eva_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->cal_gap ?>
            </td>
            <td>
                <?= $r->cal_subject ?>
            </td>
            <td style="text-align:center">
                <?= $r->cal_self ?>
            </td>
            <td style="text-align:center">
                <?= $r->cal_boss ?>
            </td>
            <td>
                <?= $r->cal_workshop ?>
            </td>
        </tr>
        <tr>
            <td>การจัดการข้อมูล</td>
            <td style="text-align:center">
                <?= $r->data_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->data_eva_score ?>
            </td>
            <td style="text-align:center">
                <?= $r->data_gap ?>
            </td>
            <td>
                <?= $r->data_subject ?>
            </td>
            <td style="text-align:center">
                <?= $r->data_self ?>
            </td>
            <td style="text-align:center">
                <?= $r->data_boss ?>
            </td>
            <td>
                <?= $r->data_workshop ?>
            </td>
        </tr>
    </table>
<?php
}
?>