<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\User;
$total = 0;
$core = 0;
foreach($kpi as $k){
    $total = $total+$k->kpi_total;
}
foreach($comp as $c){
    $core = $core+$c->total;
}

?>
<h2 align="center">แบบสรุปการประเมินผลการปฏิบัติราชการ</h2>

<p>
<u>ส่วนที่ 1 : ข้อมูลผู้รับการประเมิน</u>
</br>
<div>
รอบการประเมิน <?= $round->r_detail ?>
</div>
</br>
<table>
    <tr>
        <td colspan="2" height="50px">ชื่อผู้รับการประเมิน <?= $model->pname.$model->fname.' '.$model->lname ?></br></br></td>
    </tr>
    <tr>
        <td width="50%">ตำแหน่ง <?= $model->position ?> </td>
        <td>ประเภทตำแหน่ง <?= $model->getTypeName() ?> </td>
    </tr>
    <tr>
        <td height="50px">ระดับ <?= $model->getLevelName() ?></td>
        <td>สังกัด สำนักงานสาธารณสุขจังหวัดอุบลราชธานี</br></br></td>
    </tr>

    <tr>
        <td colspan="2" height="50px">ชื่อผู้ประเมิน <?= $eva->getName() ?></td>
    </tr>
    <tr>
        <td colspan="2">ตำแหน่ง <?= $eva->getPosition() ?></br></br></td>
    </tr>
</table>
</br>
</p>
<p>
<u>ส่วนที่ 2  การสรุปผลการประเมิน</u>
</br>
<table border="1" cellpadding="2" cellspacing="0">
    <tr>
        <th>องค์ประกอบการประเมิน</th>
        <th>คะแนน(ก)</th>
        <th>น้ำหนัก(ข)</th>
        <th>รวมคะแนน (ก)x (ข)</th>
    </tr>
    <tr>
        <td>องค์ประกอบที่ 1 : ผลสัมฤทธิของงาน</td>
        <td style='text-align:center'><?= $total*20 ?></td>
        <td style='text-align:center'> 70% </td>
        <td style='text-align:center'><?= $total*20*0.7 ?></td> 
    </tr>
    <tr>
        <td>องค์ประกอบ  ที่  2 : พฤติกรรมการปฏิบัติราชการ(สรรถนะ)</td>
        <td style='text-align:center'><?= $core*20 ?></td>
        <td style='text-align:center'> 30% </td>
        <td style='text-align:center'><?= $core*20*0.3 ?></td> 
    </tr>
    <tr>
        <td>องค์ประกอบอื่น  (ถ้ามี)</td>
        <td></td>
        <td></td>
        <td></td> 
    </tr>
    <tr>
        <td colspan="2">รวม</td>
        <td style='text-align:center'>100%</td>
        <td style='text-align:center'><?= ($total*20*0.7) + ($core*20*0.3) ?></td>
    </tr>
</table>
</br>
</p>
<p>
<u>ส่วนที่ 2  การสรุปผลการประเมิน</u>
</br>

<table>
    <tr>
        <td width="20%">O ดีเด่น</td>
        <td width="20%">O ดีมาก</td>
        <td width="20%">O ดี</td>
        <td width="20%">O พอใช้</td>
        <td width="20%">O ต้องปรับปรุง</td>
    </tr>
</table>
</p>
<p>
<u>ส่วนที่ 3 : แผนพัฒนาการปฏิบัติราชการรายบุคคล</u>
</br>
<table width="100%" border="1" cellpadding="2" cellspacing="0">
        <tr>
            <th style="text-align:center" width="5%">ลำดับ</th>
            <th style="text-align:center" width="35%">ความรู้/ทักษะ/สมรรถนะ ที่ต้องได้รับการพัฒนา</th>
            <th style="text-align:center" width="10%">วิธีการพัฒนา</th>
            <th style="text-align:center" width="10%">ช่วงเวลาที่ต้องการพัฒนา</th>
        </tr>
<?php 
$i=1;
foreach($plan as $p) {
        echo "<tr><td style='text-align:center'>".$i."</td>";
        echo "<td>".$p->plan_development."</td>";
        echo "<td style='text-align:center'>".$p->deverlop_by."</td>";
        echo "<td style='text-align:center'>".$p->duration."</td></tr>";
}
?>
</table>

</p>
<div style="page-break-before:always">&nbsp;</div> 
<p>
<u>ส่วนที่ 4 : การรับทราบผลการประเมิน</u>
</br>
<table width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
    <td>
    <table>    
    <tr>
        <td rowspan="4" width="50"></td>
        <td width="300" style="vertical-align:top">
        O ได้รับทราบผลการประเมินและแผนพัฒนา
        </td>
        <td>&emsp;&emsp;ลงชื่อ</td>
    </tr>
    <tr>
        <td rowspan="3" style="vertical-align:top">&emsp;&nbsp;&nbsp;การปฏิบัติราชการรายบุคคลแล้ว</td>
        <td align="center"><br>( <?= $model->pname.$model->fname.' '.$model->lname ?>)</td>
    </tr>
    <tr>
        <td align="center">ตำแหน่ง <?= $model->position.$model->getLevelName() ?></td>
    </tr>
    <tr><td align="center">วันที่ ........................ </td></tr>
    </table>
    </td>
    </tr>
    <tr><td>
        <table width="100%">
            <tr>
                <td colspan="3"><u>ผู้ประเมิน</u></td>
            </tr>
            <tr>
                <td rowspan="11" width="50"></td>
                <td rowspan="4" width="300" style="vertical-align:top">
                O ได้รับแจ้งผลการประเมินและผู้รับการประเมินได้ลงนามรับทราบ<br>
                O ได้แจ้งผลการประเมินเมื่อวันที่..........................<br>
                แต่ผู้รับการประเมินไม่ลงนามรับทราบ<br><br>
                โดยมี ....................................................... เป็นพยาน <br>
                </td>
                <td>&emsp;&emsp;ลงชื่อ</td>
            </tr>
            <tr><td align="center">( <?= $eva->getName() ?> )</td></tr>
            <tr><td align="center">ตำแหน่ง <?= $eva->getPosition() ?></br></br></td></tr>
            <tr><td align="center">วันที่ ........................ </td>
            <tr><td><br>&emsp;&emsp;&emsp;ลงชื่อ&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;พยาน</td><td></td><tr>
            <tr><td align="center">ตำแหน่ง ................................................</td><td></td><tr>
            <tr><td align="center">วันที่ ..........................................</td><td></td><tr>
        </table>
    </td></tr>
</table>
</p>
<p>
<u>ส่วนที่ 5 :  ความเห็นของผู้บังคับบัญชาเหนือขึ้นไป</u>
<table width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr><td>
    <table width="100%">
        <tr><td colspan="3">ผู้บังคับบัญชาเหนือขึ้นไป</td></tr>
        <tr>
            <td rowspan="8" width="50"></td>
            <td width="300">O เห็นด้วยกับผลการประเมิน</td>
            <td>&emsp;&emsp;ลงชื่อ</td>
        </tr>
        <tr>
            <td>O มีความเห็นต่าง ดังนี้</td>
            <td align="center">( นางสาวธรรมพร ปรัสพันธ์ )</td>
        </tr>
        <tr>
            <td>...........................................................................</td>
            <td align="center">ตำแหน่ง นายแพทย์ชำนาญการ</td>
        </tr>
        <tr>
            <td rowspan="3"  style="vertical-align:top">...........................................................................</td>
            <td align="center">รักษาการณ์ในตำแหน่งผู้อำนวยการ</td>
        </tr>
        <tr>
            <td  align="center">โรงพยาบาลเหล่าเสือโก้ก</td>
        </tr>
        <tr>
            <td align="center">วันที่ ........................ </td>
        </tr>    
    </table>
    </td></tr>
    <tr><td>
    <table width="100%">
        <tr><td colspan="3">ผู้บังคับบัญชาเหนือขึ้นไป(ถ้ามี)</td></tr>
        <tr>
            <td rowspan="4" width="50"></td>
            <td width="300">O เห็นด้วยกับผลการประเมิน</td>
            <td>&emsp;&emsp;ลงชื่อ</td>
        </tr>
        <tr>
            <td>O มีความเห็นต่าง ดังนี้</td>
            <td align="center">( ........................................... )</td>
        </tr>
        <tr>
            <td>...........................................................................</td>
            <td align="center">ตำแหน่ง ................................................</td>
        </tr>
        <tr>
            <td>...........................................................................</td>
            <td align="center">วันที่ ........................ </td>
        </tr>
        <tr>
            <td colspan="3">&emsp;</td>
        </tr>    
    </table>
    </td></tr>
</table>
<table width="100%" border="1" cellpadding="2" cellspacing="0">
<tr>
    <td style="font-size: 0.8em">คำชี้แจง<br>
แบบสรุปการประเมินผลการปฏิบัตราชการนี้มีด้วยกัน ๓ หน้า  ประกอบด้วย<br>
&emsp;ส่วนที่ ๑ : ข้อมูลของผู้รับการประเมินเพื่อระบุรายละเอียดต่างๆที่เกี่ยวข้องกับตัวผู้รับประเมิน<br>
&emsp;ส่วนที่ ๒ : สรุปผลการประเมิน  ใช้เพื่อกรอกค่าคะแนนการประเมินในองค์ประกอบด้านผลสัมฤทธิ์ของงาน<br> 
องค์ประกอบด้านพฤติกรรมการปฏิบัติราชการและน้ำหนักของสององค์ประกอบ  ในแบบสรุปส่วนที่ ๒ นี้   ยังใช้สำหรับคำนวนผล<br>
การปฏิบัติราชการรวมด้วย<br>
&emsp;&emsp;- สำหรับคะแนนองค์ประกอบด้านผลสัมฤทธิ์ของงาน ให้นำมาจากแบบประเมินผลสัมฤทธิ์ของงาน โดยให้แนบท้ายแบบสรุปนี้<br>
&emsp;&emsp;- สำหรับคะแนนองค์ประกอบด้านพฤติกรรมการปฏิบัติราชการ   ให้นำมาจากแบบประเมินสมรรถนะ  โดยให้แนบท้ายแบบสรุปนี้<br>
&emsp;ส่วนที่ ๓ : แผนปฏิบัติราชการรายบุคคล  ผู้ประเมินและผู้รับการประเมินร่วมกันจัดทำแผนพัฒนาผลการปฏิบัติราชการ<br>
&emsp;ส่วนที่ ๔ : การรับทราบผลการประเมิน  ผู้รับการปประเมินรับทราบผลการประเมิน<br>
&emsp;ส่วนที่ ๕ : ความเห็นของผู้บังคับบัญชาเหนือขึ้นไป  ผู้บังคับบัญชาเหนือขึ้นไปกลั่นกรองผลการประเมิน แผนพัฒนา<br>
ผลการปฏิบัติราชการและให้ความเห็น<br>
คำว่า "ผู้บังคับบัญชาเหนือขึ้นไป" สำหรับผู้ประเมินตามข้อ ๒ (๙) หมายถึง หัวหน้าส่วนราชการประจำจังหวัดผู้บังคับบัญชาของผู้รับการประเมิน 
    </td>
    </tr>
</table>
</p>