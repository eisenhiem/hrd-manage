<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Rounds;
use app\models\Competency;
use app\models\Kpi;
use app\models\Plan;

//echo $round->r_id;
/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'บุคลากร';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'emp_id',
            [
                'label' => 'ชื่อ-สกุล',
                'value' => function($model) { return $model->getFullName() ;},
            ],
            //'pname',
            //'fname',
            //'lname',
            'position',
            //'level',
            //'position_type',
            //'birthdate',
            //'start_date',
            //'education',
            //'branch',
            //'departments.dep_name',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'รายละเอียด',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{detail}',
                'buttons'=>[
                    'detail' => function($url,$model,$key){
                      return Html::a('รายละเอียด',['detail','id'=>$model->emp_id],['class' => 'btn btn-primary']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ประเมินผลสัมฤทธิ์',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{kpi}',
                'buttons'=>[
                    'kpi' => function($url,$model,$key){
                        $r = Rounds::find()->where(['is_active'=>'1'])->one();
                       
                       return Html::a('บันทึกผลการประเมินตัวชี้วัด',['kpi','emp_id'=>$model->emp_id,'r_id'=>$r->r_id],['class' => 'btn btn-success']);
                        //return !Kpi::find()->where(['emp_id'=>$model->emp_id,'r_id'=>$r->r_id]) ? Html::a('บันทึกผลการประเมินตัวชี้วัด',['kpi','emp_id'=>$model->emp_id,'r_id'=>$r->r_id],['class' => 'btn btn-success']) : Html::a('ประเมินแล้ว',['view','id'=>$model->emp_id],['class' => 'btn btn-danger']);
                    }
                ]
            ], 
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ประเมินสมรรถนะ',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{comp}',
                'buttons'=>[
                    'comp' => function($url,$model,$key){
                        $r = Rounds::find()->where(['is_active'=>'1'])->one();                       
                        return Yii::$app->user->identity->dep_id ? Html::a('บันทึกผลการประเมินสมรรถนะ',['comp','emp_id'=>$model->emp_id,'r_id'=>$r->r_id],['class' => 'btn btn-success']):'';
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'การเพิ่มสมรรถนะ/ความรู้/ทักษะ',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{plan}',
                'buttons'=>[
                    'plan' => function($url,$model,$key){
                        $r = Rounds::find()->where(['is_active'=>'1'])->one();                       
                        return Yii::$app->user->identity->dep_id ? Html::a('การเพิ่มสมรรถนะ/ความรู้/ทักษะ',['plan','emp_id'=>$model->emp_id,'r_id'=>$r->r_id],['class' => 'btn btn-success']):'';
                      }
                  ]
            ],
        ],
    ]); ?>


</div>
