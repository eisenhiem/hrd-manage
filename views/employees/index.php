<?php

use app\models\CoreCompetency;
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Rounds;
use app\models\Eva;
use app\models\Trainings;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'บุคลากร';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มบุคลากร', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'emp_id',
            [
                'label' => 'ชื่อ-สกุล',
                'value' => function($model) { return $model->getFullName() ;},
            ],
            //'pname',
            //'fname',
            //'lname',
            'position',
            //'level',
            //'position_type',
            //'birthdate',
            //'start_date',
            //'education',
            //'branch',
            'departments.dep_name',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'รายละเอียด',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{view}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                      return Html::a('รายละเอียด',['view','id'=>$model->emp_id],['class' => 'btn btn-primary']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ประเมินตนเอง',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{eva}',
                'buttons'=>[
                    'eva' => function($url,$model,$key){
                      return !Eva::find()->where(['emp_id'=>$model->emp_id,'r_id'=>Rounds::find()->select('r_id')->where(['is_active'=>'1'])])->all() ? Html::a('ทำแบบประเมิน',['eva/create','id'=>$model->emp_id],['class' => 'btn btn-warning']): Html::button('ประเมินแล้ว',['class' => 'btn btn-info']) ;
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Training Need',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{training}',
                'buttons'=>[
                    'training' => function($url,$model,$key){
                      return !Trainings::find()->where(['emp_id'=>$model->emp_id,'r_id'=>Rounds::find()->select('r_id')->where(['is_active'=>'1'])])->all() ? Html::a('Training Need',['trainings/create','id'=>$model->emp_id],['class' => 'btn btn-success']) : Html::a('รายละเอียด',['trainings/detail','id'=>$model->emp_id],['class' => 'btn btn-danger']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Core Competency',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{competency}',
                'buttons'=>[
                    'competency' => function($url,$model,$key){
                        if(!Yii::$app->user->isGuest){
                            return !CoreCompetency::find()->where(['emp_id'=>$model->emp_id,'r_id'=>Rounds::find()->select('r_id')->where(['is_active'=>'1'])])->all() ? Html::a('ประเมินสมรรถนะ',['core/create','id'=>$model->emp_id],['class' => 'btn btn-success']) : Html::a('รายละเอียด',['core/view','id'=>$model->emp_id],['class' => 'btn btn-info']);
                        } else {
                            return Html::a('รายละเอียด',['core/view','id'=>$model->emp_id],['class' => 'btn btn-info']);
                        }
                    }
                ]
            ],
        ],
    ]); ?>


</div>
