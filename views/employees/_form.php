<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Url;
use unclead\multipleinput\MultipleInputColumn;

use app\models\Levels;
use app\models\Types;
use app\models\Departments;

use kartik\date\DatePicker;

$pname = ['นาย'=> 'นาย','นาง'=> 'นาง','นางสาว'=> 'นางสาว'];
$level = ArrayHelper::map(Levels::find()->all(), 'level', 'level_name');
$type = ArrayHelper::map(Types::find()->all(), 'position_type', 'position_type_name');
$dep = ArrayHelper::map(Departments::find()->all(), 'dep_id', 'dep_name');

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employees-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-2'>
            <?= $form->field($model, 'finger_id')->textInput() ?>
        </div>
        <div class='col-md-1'>
            <?= $form->field($model, 'pname')->dropDownList($pname,['prompt'=>'']) ?>
        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'birthdate')->widget(DatePicker::ClassName(),
                [
                    'name' => 'birthdate', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันเกิด'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>    
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'level')->dropDownList($level,['prompt'=>'']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'position_type')->dropDownList($type,['prompt'=>'']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'start_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'start_date', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันที่เริ่มปฏิบัติงาน'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'education')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'branch')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'dep_id')->dropDownList($dep,['prompt'=>'']) ?>
        </div>
    </div>

    <table width="80%">
        <tr>
            <th colspan="2">งานที่รับผิดชอบ</th>
        </tr>
        <tr>
            <td width="20" style="vertical-align:middle"> <b>1.</b> </td>
            <td>
                <?= $form->field($model, 'job1')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td width="20" style="vertical-align:middle"> <b>2.</b> </td>
            <td>
                <?= $form->field($model, 'job2')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td width="20" style="vertical-align:middle"> <b>3.</b> </td>
            <td>
                <?= $form->field($model, 'job3')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td width="20" style="vertical-align:middle"> <b>4.</b> </td>
            <td>
                <?= $form->field($model, 'job4')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td width="20" style="vertical-align:middle"> <b>5.</b> </td>
            <td>
                <?= $form->field($model, 'job5')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td width="20" style="vertical-align:middle"> <b>6.</b> </td>
            <td>
                <?= $form->field($model, 'job6')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
    </table>        

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
