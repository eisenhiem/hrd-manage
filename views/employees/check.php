<?php

use app\models\CoreCompetency;
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Rounds;
use app\models\Eva;
use app\models\Trainings;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'บันทึกเวลาเข้าออก';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'ชื่อ-สกุล',
                'value' => function($model) { return $model->getFullName() ;},
            ],
            'departments.dep_name',
        ],
    ]); ?>


</div>
