<?php

use app\models\CoreCompetency;
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Rounds;
use app\models\Eva;
use app\models\Trainings;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'จัดการวันลาบุคลากร';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search_va', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'emp_id',
            [
                'label' => 'ชื่อ-สกุล',
                'value' => function($model) { return $model->getFullName() ;},
            ],
            //'pname',
            //'fname',
            //'lname',
            'position',
            //'level',
            //'position_type',
            //'birthdate',
            //'start_date',
            //'education',
            //'branch',
            'departments.dep_name',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'จัดการวันลา',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{vacation}',
                'buttons'=>[
                    'vacation' => function($url,$model,$key){
                      return Html::a('รายละเอียด',['vacation/detail','id'=>$model->emp_id],['class' => 'btn btn-primary']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'เพิ่มวันลา',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{la}',
                'buttons'=>[
                    'la' => function($url,$model,$key){
                        return Yii::$app->user->identity->id == 1 ? Html::a('เพิ่มวันลา',['vacation/create','id'=>$model->emp_id],['class' => 'btn btn-primary']):'';
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'แก้ไขวันลา',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{editla}',
                'buttons'=>[
                    'editla' => function($url,$model,$key){
                        return Yii::$app->user->identity->id == 1 ? Html::a('แก้ไขวันลา',['vacation/admin','id'=>$model->emp_id],['class' => 'btn btn-primary']):'';
                    }
                ]
            ],
        ],
    ]); ?>


</div>
