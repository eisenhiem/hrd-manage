<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */

$this->title = $model->fname;
$this->params['breadcrumbs'][] = ['label' => 'บุคลากร', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="employees-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->emp_id], ['class' => 'btn btn-primary']) ?>
    </p>

    <b>ชื่อ - นามสกุล</b> <?= $model->pname.$model->fname.' '.$model->lname ?><br>
<b>ตำแหน่ง</b> <?= $model->position ?> <b>ระดับ</b> <?= $model->getLevelName() ?> <b>ประเภทตำแหน่ง</b> <?= $model->getTypeName() ?><br>
<b>อายุ</b> <?= $model->getAge()?> ปี <b>อายุราชการ</b> <?= $model->getAgeJob() ?> ปี <b>อายุราชการที่เหลือ</b> <?= 60-$model->getAge() ?> ปี<br>
<b>วุฒิการศึกษาสูงสุด</b> <?= $model->education ?> <b>สาขา</b> <?= $model->branch ?><br>
<b>กลุ่มงาน/งาน</b> <?= $model->getDepName() ?><br>
<b>งานที่รับผิดชอบ</b><br>
<div class="col-md-6">
    1. <?= $model->job1 ?>
</div>
<div class="col-md-6">
    2. <?= $model->job2 ?>
</div>
<div class="col-md-6">
    3. <?= $model->job3 ?>
</div>
<div class="col-md-6">
    4. <?= $model->job4 ?>
</div>
<div class="col-md-6">
    5. <?= $model->job5 ?>
</div>
<div class="col-md-6">
    6. <?= $model->job6 ?>
</div>
</p>

</div>
</br> 

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'kpi_id',
            //'r_id',
            //'emp_id',
            'kpi_no',
            'kpi_name',
            //'kpi_template:ntext',
            'kpi_1',
            'kpi_2',
            'kpi_3',
            'kpi_4',
            'kpi_5',
            'kpi_score',
            'kpi_weight',
            'kpi_total',
            //'kpi_active',
            //'evaluate_by',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ประเมิน',
                'options'=>['style'=>'width:80px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                      return Yii::$app->user->identity->dep_id ? Html::a('ประเมินตัวชี้วัด',['kpi/update','id'=>$model->kpi_id],['class' => 'btn btn-primary']):'';
                    }
                ]
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <p>
        <?= Html::a('เพิ่มตัวชี้วัด', ['kpi/create','emp_id'=>$model->emp_id], ['class' => 'btn btn-success']) ?>
    </p>
