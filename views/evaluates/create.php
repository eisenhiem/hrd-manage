<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Evaluates */

$this->title = 'Create Evaluates';
$this->params['breadcrumbs'][] = ['label' => 'Evaluates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evaluates-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
