<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Evaluates */

$this->title = $model->emp_id;
$this->params['breadcrumbs'][] = ['label' => 'Evaluates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="evaluates-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'emp_id' => $model->emp_id, 'r_id' => $model->r_id, 'k_id' => $model->k_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'emp_id' => $model->emp_id, 'r_id' => $model->r_id, 'k_id' => $model->k_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'emp_id',
            'r_id',
            'k_id',
            'standard_score',
            'evaluate_score',
            'subject_training:ntext',
            'by_self',
            'by_boss',
            'workshop',
        ],
    ]) ?>

</div>
