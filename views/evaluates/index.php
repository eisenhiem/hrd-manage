<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EvaluatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Evaluates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evaluates-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Evaluates', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'emp_id',
            'r_id',
            'k_id',
            'standard_score',
            'evaluate_score',
            //'subject_training:ntext',
            //'by_self',
            //'by_boss',
            //'workshop',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
