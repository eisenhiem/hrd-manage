<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;

use app\models\Rounds;
use app\models\Knowledge;

$r = ArrayHelper::map(Rounds::find()->all(), 'r_id', 'r_detail');
$k = Knowledge::find()->all();
$score = [0 => '0', 1=>'1', 2=>'2', 3=>'3', 4=>'4', 5=>'5'];
$self = ['S'=>'เรียนรู้ด้วยตนเอง','C'=>'เข้ารับการศึกษาต่อนื่อง','E'=>'เรียนทางอิเล็คทรอนิก'];
$boss = ['O'=>'สอยขณะปฏิบัติงานจริง','A' => 'มอบหมายงาน/โครงการ', 'J' => 'การหมุนเวียนงาน'];
$workshop = [];

/* @var $this yii\web\View */
/* @var $model app\models\Evaluates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="evaluates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'emp_id')->textInput() ?>

    <?= $form->field($model, 'r_id')->dropDownList($r,['prompt'=>'']) ?>

    <table width="100%">
        <tr>
            <th width="20%">ความรู้/ทักษะ</th>
            <th width="10%">ค่ามาตรฐาน</th>
            <th width="10%">ค่าประเมิน</th>
            <th width="20%">ประเด็น</th>
            <th width="10%">พัฒนาด้วย<br>ตนเอง</th>
            <th width="10%">พัฒนาด้วย<br>ผู้บังคับบัญชา</th>
            <th width="20%">อบรม</th>
        </tr>

    <?php 
    
    foreach($k as $km){
        echo "<tr>";
        echo "<td>";
        echo $km->k_name." ";
        echo $form->field($model, 'k_id')->hiddenInput(['value'=> $km->k_id])->label(false);
        echo "</td>";
        echo "<td>";
        echo $form->field($model, 'standard_score')->dropDownList($score,['prompt'=>''])->label(false);
        echo "</td>";
        echo "<td>";        
        echo $form->field($model, 'evaluate_score')->dropDownList($score,['prompt'=>''])->label(false);
        echo "</td>";
        echo "<td>";
        echo $form->field($model, 'subject_training')->textarea(['rows' => 1])->label(false);
        echo "</td>";
        echo "<td>";
        echo $form->field($model, 'by_self')->dropDownList($self,['prompt'=>''])->label(false) ;
        echo "</td>";
        echo "<td>";
        echo $form->field($model, 'by_boss')->dropDownList($boss,['prompt'=>''])->label(false);
        echo "</td>";
        echo "<td>";
        echo $form->field($model, 'workshop')->textInput(['maxlength' => true])->label(false);
        echo "</td>";
        echo "</tr>";
        }
    ?>
    </table>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
