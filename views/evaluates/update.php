<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Evaluates */

$this->title = 'Update Evaluates: ' . $model->emp_id;
$this->params['breadcrumbs'][] = ['label' => 'Evaluates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->emp_id, 'url' => ['view', 'emp_id' => $model->emp_id, 'r_id' => $model->r_id, 'k_id' => $model->k_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="evaluates-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
