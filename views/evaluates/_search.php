<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EvaluatesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="evaluates-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'emp_id') ?>

    <?= $form->field($model, 'r_id') ?>

    <?= $form->field($model, 'k_id') ?>

    <?= $form->field($model, 'standard_score') ?>

    <?= $form->field($model, 'evaluate_score') ?>

    <?php // echo $form->field($model, 'subject_training') ?>

    <?php // echo $form->field($model, 'by_self') ?>

    <?php // echo $form->field($model, 'by_boss') ?>

    <?php // echo $form->field($model, 'workshop') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
