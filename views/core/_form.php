<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Core */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="core-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'core_id')->textInput() ?>

    <?= $form->field($model, 'core_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
