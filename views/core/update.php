<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Core */

$this->title = 'Update Core: ' . $model->core_id;
$this->params['breadcrumbs'][] = ['label' => 'Cores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->core_id, 'url' => ['view', 'id' => $model->core_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
