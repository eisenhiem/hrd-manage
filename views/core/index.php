<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CoreSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Core', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'core_id',
            'core_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
