<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Core */

$this->title = 'Create Core';
$this->params['breadcrumbs'][] = ['label' => 'Cores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
