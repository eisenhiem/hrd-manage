<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmpSalarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'บัญชีค่าตอบแทนเจ้าหน้าที่';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emp-salary-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'emp_id',
            [
                'header' => 'ชื่อ-สกุล',
                'value' => function($model){
                    return $model->getEmpName();
                } 
            ],
            [
                'header' => 'ตำแหน่ง',
                'value' => function($model){
                    return $model->emp->position;
                } 

            ], 
            [
                'header' => 'กลุ่มงาน',
                'value' => function($model){
                    return $model->getDepName();
                } 

            ],
            [
                'header' => 'จ่ายล่าสุด',
                'value' => function($model){
                    return $model->getLastPay() <> 'NA' ? $model->getThaiDate($model->getLastPay()):'ยังไม่มีประวัติจ่าย';
                }
            ],
            //'base_salary',
            //'ot_salary',
            //'v11_salary',
            //'pts_salary',
            //'other_salary',
            //'tax_pay',
            //'kbk_pay',
            //'sso_pay',
            //'om_pay',
            //'dt_pay',
            //'elec_pay',
            //'bank_pay',
            //'insurance_pay',
            //'other_pay',
            //'d_update',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'options'=>['style'=>'width:200px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{update} {paid}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                      return Html::a('แก้ไข',['update','id'=>$model->emp_id],['class' => 'btn btn-primary']);
                    },
                    'paid' => function($url,$model,$key){
                        return Html::a('บันทึกการจ่าย',['/salary/create','id'=>$model->emp_id],['class' => 'btn btn-success']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{trash}',
                'buttons'=>[
                    'trash' => function($url,$model,$key){
                      return Html::a('เลิกจ้าง/ย้าย/ลาออก',['dc','id'=>$model->emp_id],['class' => 'btn btn-danger']);
                    },
                ]
            ],
        ],
    ]); ?>


</div>
