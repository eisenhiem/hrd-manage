<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmpSalary */

$this->title = 'แก้ไข: ' . $model->getEmpName();
$this->params['breadcrumbs'][] = ['label' => 'บัญชีเงินเดือน', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getEmpName(), 'url' => ['view', 'id' => $model->emp_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="emp-salary-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
