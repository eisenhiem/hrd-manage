<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmpSalarySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emp-salary-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'emp_id') ?>

    <?= $form->field($model, 'base_salary') ?>

    <?= $form->field($model, 'ot_salary') ?>

    <?= $form->field($model, 'v11_salary') ?>

    <?= $form->field($model, 'pts_salary') ?>

    <?php // echo $form->field($model, 'other_salary') ?>

    <?php // echo $form->field($model, 'tax_pay') ?>

    <?php // echo $form->field($model, 'kbk_pay') ?>

    <?php // echo $form->field($model, 'sso_pay') ?>

    <?php // echo $form->field($model, 'om_pay') ?>

    <?php // echo $form->field($model, 'dt_pay') ?>

    <?php // echo $form->field($model, 'elec_pay') ?>

    <?php // echo $form->field($model, 'bank_pay') ?>

    <?php // echo $form->field($model, 'insurance_pay') ?>

    <?php // echo $form->field($model, 'other_pay') ?>

    <?php // echo $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
