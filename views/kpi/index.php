<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KpiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kpis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Kpi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kpi_id',
            'r_id',
            'emp_id',
            'kpi_no',
            'kpi_name',
            //'kpi_template:ntext',
            //'kpi_ratio',
            //'kpi_weight',
            //'kpi_1',
            //'kpi_2',
            //'kpi_3',
            //'kpi_4',
            //'kpi_5',
            //'kpi_total',
            //'kpi_active',
            //'evaluate_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
