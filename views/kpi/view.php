<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kpi */

$this->title = $model->kpi_id;
$this->params['breadcrumbs'][] = ['label' => 'Kpis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="kpi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->kpi_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->kpi_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kpi_id',
            'r_id',
            'emp_id',
            'kpi_no',
            'kpi_name',
            'kpi_template:ntext',
            'kpi_ratio',
            'kpi_weight',
            'kpi_1',
            'kpi_2',
            'kpi_3',
            'kpi_4',
            'kpi_5',
            'kpi_total',
            'kpi_active',
            'evaluate_by',
        ],
    ]) ?>

</div>
