<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KpiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kpi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'kpi_id') ?>

    <?= $form->field($model, 'r_id') ?>

    <?= $form->field($model, 'emp_id') ?>

    <?= $form->field($model, 'kpi_no') ?>

    <?= $form->field($model, 'kpi_name') ?>

    <?php // echo $form->field($model, 'kpi_template') ?>

    <?php // echo $form->field($model, 'kpi_ratio') ?>

    <?php // echo $form->field($model, 'kpi_weight') ?>

    <?php // echo $form->field($model, 'kpi_1') ?>

    <?php // echo $form->field($model, 'kpi_2') ?>

    <?php // echo $form->field($model, 'kpi_3') ?>

    <?php // echo $form->field($model, 'kpi_4') ?>

    <?php // echo $form->field($model, 'kpi_5') ?>

    <?php // echo $form->field($model, 'kpi_total') ?>

    <?php // echo $form->field($model, 'kpi_active') ?>

    <?php // echo $form->field($model, 'evaluate_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
