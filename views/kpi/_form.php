<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Rounds;

$r = ArrayHelper::map(Rounds::find()->where(['is_active'=>'1'])->all(), 'r_id', 'r_detail');
$score = [0 => '0', 1=>'1', 2=>'2', 3=>'3', 4=>'4', 5=>'5'];

/* @var $this yii\web\View */
/* @var $model app\models\Kpi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kpi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'r_id')->dropDownList($r,['prompt'=>$r[0]]) ?>

    <?= $form->field($model, 'kpi_no')->radioList([ 1 => '1', 2 => '2', 3 => '3', 4 => '4',5 => '5', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'kpi_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kpi_template')->textarea(['rows' => 6]) ?>

    <div class="row">
        <div class="col-md-2" style="vertical-align:center">
            <b>เกณฑ์ให้คะแนน</b>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'kpi_1')->textInput() ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'kpi_2')->textInput() ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'kpi_3')->textInput() ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'kpi_4')->textInput() ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'kpi_5')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'kpi_weight')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= Yii::$app->user->identity->dep_id ? $form->field($model, 'kpi_score')->textInput():'' ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
