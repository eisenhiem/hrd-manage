<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Er */

$this->title = 'Update Er: ' . $model->emp_id;
$this->params['breadcrumbs'][] = ['label' => 'Ers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->emp_id, 'url' => ['view', 'id' => $model->emp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="er-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
