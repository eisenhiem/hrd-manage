<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Employees;

$r = ArrayHelper::map(Employees::find()->all(), 'emp_id', 'fname');
$ot = [600 => 'พยาบาล', 480=>'EMT'];
$bd = [240 => 'พยาบาล', 180=>'EMT'];

/* @var $this yii\web\View */
/* @var $model app\models\Er */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="er-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'emp_id')->dropDownList($r) ?>

    <?= $form->field($model, 'rate_ot')->radioList($ot) ?>

    <?= $form->field($model, 'rate_bd')->radioList($ot) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
