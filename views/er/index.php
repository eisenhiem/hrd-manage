<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ErSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'เจ้าหน้าที่ ER';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="er-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มเจ้าหน้าที่ ER', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('จัดเวร ER', ['work/index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'emp_id',
            [
                'label' => 'ชื่อ-สกุล',
                'value' => function($model) { return $model->getName() ;},
            ],
            'rate_ot',
            'rate_bd',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
