<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Er */

$this->title = 'Create Er';
$this->params['breadcrumbs'][] = ['label' => 'Ers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="er-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
