<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CheckSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การบันทึกเวลาทำงาน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel'=>[
            'before'=>' '
            ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'PCODE',
            'emp_name',
            //'dep_id',
            'REGIST_DATE',
            'REGIST_TIME',
            //'CHECKTYPE',
            [
                'attribute' => 'CHECKTYPE',
                'value' => function($model){
                    return $model->CHECKTYPE == 0 ? 'เข้า': 'ออก';
                }
            ]

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
