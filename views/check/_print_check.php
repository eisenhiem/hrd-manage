<h3 align="center">บัญชีลงชื่อปฏิบัติงาน โรงพยาบาลเหล่าเสือโก้ก<br>
<?= $emp->getFullName() ?> <?= $model->dep_name ?> ประจำเดือน <?= $workdate ?></h3>
<table width="90%" align="center"  border="1" cellpadding="2" cellspacing="0">
<tr>
<th width="80">ลำดับที่</th>
<th width="200">วันที่</th>
<th width="100">เวลา</th>
<th>ประเภทการลงเวลา</th>
</tr>
<?php
    $i=1;
    foreach($model as $r) {
        echo '<tr><td align=right>'.$i.'&ensp;</td><td align=center>'.$r->getRegisterDate().'</td><td align=center>'.$r->reg_time.'</td><td align=center>'.$r->status_name.'</td></tr>';
        $i++;
    }
?>
</table>
<br>
&emsp;หมายเหตุ .....................................................................................................................................................
<br>........................................................................................................................................................................
<br>

&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;ลงชื่อ &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ผู้ตรวจสอบ &emsp;&emsp;&emsp;&emsp; ลงชื่อ &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ผู้รับรอง <br>
&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;&emsp;(........................................) &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; (........................................)<br>
