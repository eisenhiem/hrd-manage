<h3 align="center">บัญชีสรุปวันทำการโรงพยาบาลเหล่าเสือโก้ก<br>
<?= $department->dep_name ?> ประจำวันที่ <?= $workdate ?></h3>
<table width="90%" align="center"  border="1" cellpadding="2" cellspacing="0">
<tr>
<th width="80">ลำดับที่</th>
<th width="250">ชื่อ-สกุล</th>
<th width="100">เวลา</th>
<th>ประเภทการลงเวลา</th>
</tr>
<?php
    $i=1;
    foreach($model as $r) {
        $x = $r->CHECKTYPE == '0' ? 'เข้า': 'ออก';
        echo '<tr><td align=right>'.$i.'&ensp;</td><td>&emsp;'.$r->emp_name.'</td><td align=center>'.$r->REGIST_TIME.'</td><td align=center>'.$x.'</td></tr>';
        $i++;
    }
?>
</table>
<br>
&emsp;หมายเหตุ .....................................................................................................................................................
<br>........................................................................................................................................................................
<br>

&emsp;<b><u>สรุป</u></b>
<table width>
<tr><td width="300"> 1) รวมจำนวน</td><td>คน</td></tr>
<tr><td>&emsp;- มาปฏิบัติงาน</td><td>คน</td></tr>
<tr><td>&emsp;- ลา</td><td>คน</td></tr>
<tr><td>&emsp;- ไปราชการ</td><td>คน</td></tr>
<tr><td> 2) มาสาย</td><td>คน</td></tr>
<tr><td> 3) ขาดงาน</td><td>คน</td></tr>
</table>
<br>

&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;ลงชื่อ &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ผู้ตรวจสอบ &emsp;&emsp;&emsp;&emsp; ลงชื่อ &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ผู้รับรอง <br>
&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;&emsp;(........................................) &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; (........................................)<br>
<br>
&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;&emsp;&emsp;ลงชื่อ &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ผู้อำนวยการ/ผู้ที่ได้รับมอบหมาย<br>
&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;&emsp;&emsp;&emsp; (........................................)
