<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Departments;
use app\models\Employees;
use kartik\date\DatePicker;

$dep = ArrayHelper::map(Departments::find()->all(), 'dep_id', 'dep_name');
$emp = ArrayHelper::map(Employees::find()->all(),'finger_id','fname');
/* @var $this yii\web\View */
/* @var $model app\models\CheckSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-search">

    <?php $form = ActiveForm::begin([
        'action' => ['report'],
        'method' => 'get',
    ]); ?>
    <div class="rows">
    <div class="col-md-4">
        <?= $form->field($model, 'dep_id')->dropDownList($dep,['prompt'=>'']) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'REGIST_DATE')->widget(DatePicker::ClassName(),
            [
                'name' => 'REGIST_DATE', 
                //'label' => 'ว้นที่เริ่มต้น',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่เริ่มต้น'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]); 
        ?>
    </div>
    <div>
    <?php // echo $form->field($model, 'CHECKTYPE') ?>
    <div class="row">
    <div class="col-md-6">
    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
