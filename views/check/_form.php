<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Check */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dep_id')->dropDownList($dep,['prompt'=>'']) ?>

    <?= $form->field($model, 'REGIST_DATE')->textInput() ?>

    <?= $form->field($model, 'REGIST_TIME')->textInput() ?>

    <?= $form->field($model, 'CHECKTYPE')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
