<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Departments;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

// $dep = Departments::find()->all();
$dep = ArrayHelper::map(Departments::find()->all(), 'dep_id', 'dep_name');

/* @var $this yii\web\View */
/* @var $searchModel app\models\CheckSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายงานการบันทึกเวลาทำงาน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search2', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'departments.dep_name',
            'REGIST_DATE',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'พิมพ์',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{print}',
                'buttons'=>[
                    'print' => function($url,$model,$key){
                      return Html::a('พิมพ์',['print','dep'=>$model->dep_id,'date'=> $model->REGIST_DATE],['target' => '_blank','class' => 'btn btn-primary'],);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
