<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Check */

$this->title = $model->PCODE;
$this->params['breadcrumbs'][] = ['label' => 'Checks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="check-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'PCODE' => $model->PCODE, 'REGIST_DATE' => $model->REGIST_DATE, 'REGIST_TIME' => $model->REGIST_TIME], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'PCODE' => $model->PCODE, 'REGIST_DATE' => $model->REGIST_DATE, 'REGIST_TIME' => $model->REGIST_TIME], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PCODE',
            'emp_name',
            'dep_id',
            'REGIST_DATE',
            'REGIST_TIME',
            'CHECKTYPE',
        ],
    ]) ?>

</div>
