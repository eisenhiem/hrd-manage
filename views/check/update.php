<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Check */

$this->title = 'Update Check: ' . $model->PCODE;
$this->params['breadcrumbs'][] = ['label' => 'Checks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PCODE, 'url' => ['view', 'PCODE' => $model->PCODE, 'REGIST_DATE' => $model->REGIST_DATE, 'REGIST_TIME' => $model->REGIST_TIME]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="check-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
