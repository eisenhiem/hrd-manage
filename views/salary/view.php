<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Salary */

$this->title = $model->salary_id;
$this->params['breadcrumbs'][] = ['label' => 'Salaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="salary-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->salary_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->salary_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'salary_id',
            'emp_id',
            'salary_date',
            'base_salary',
            'ot_salary',
            'v11_salary',
            'pts_salary',
            'other_salary',
            'tax_pay',
            'kbk_pay',
            'sso_pay',
            'om_pay',
            'dt_pay',
            'elec_pay',
            'bank_pay',
            'insurance_pay',
            'other_pay',
        ],
    ]) ?>

</div>
