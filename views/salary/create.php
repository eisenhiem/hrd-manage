<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Salary */

$this->title = 'จ่ายเงินเดือน';
$this->params['breadcrumbs'][] = ['label' => 'รายการเงินเดือน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salary-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
