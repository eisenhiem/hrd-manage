<?php

use app\models\EmpSalary;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SalarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการจ่ายเงินเดือน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salary-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'header' => 'ชื่อ-สกุล',
                'value' => function($model){
                    return $model->getEmpName();
                } 
            ],
            [
                'header' => 'ตำแหน่ง',
                'value' => function($model){
                    return $model->emp->position;
                } 

            ], 
            [
                'header' => 'จ่ายวันที่',
                'value' => function($model){
                    return EmpSalary::getThaiDate($model->salary_date);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{print}',
                'buttons'=>[
                    'print' => function($url,$model,$key){
                      return Html::a('พิมพ์',['print','id'=>$model->salary_id],['class' => 'btn btn-info']);
                    },
                ]
            ],
        ],
    ]); ?>

</div>
