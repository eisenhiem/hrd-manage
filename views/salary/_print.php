<?php

use app\models\EmpSalary;
use yii\helpers\Html;

?>
<table width="300px" align="center">
    <tr>
        <td colspan="2" align="center">
        ใบรับรองการจ่ายเงินเดือนและเงินอื่น
        </td>
    </tr>
    <tr>
        <td width="150px" align="right">ประจำเดือน &emsp;</td>
        <td width="150px" style="border-bottom: 1px dotted;"><?= $model->getThaiMonth($model->salary_date) ?></td>
    </tr>
</table>
<table width="400px" align="center">
    <tr>
        <td width="80px" align="right">ชื่อ-สกุล </td>
        <td style="border-bottom: 1px dotted;"><?= $model->getEmpName(); ?></td>
    </tr>
    <tr>
        <td align="right">หน่วยงาน </td>
        <td style="border-bottom: 1px dotted;">โรงพยาบาลเหล่าเสือโก้ก</td>
    </tr>
    <tr>
        <td></td>
        <td style="border-bottom: 1px dotted;">สำนักงานปลัดกระทรวงสาธารณสุข</td>
    </tr>
    <tr>
        <td align="right">จังหวัด </td>
        <td style="border-bottom: 1px dotted;">อุบลราชธานี</td>
    </tr>
    <tr>
        <td align="right">โอนเงินเข้า </td>
        <td style="border-bottom: 1px dotted;"><?= $model->base->account_bank ?></td>
    </tr>
    <tr>
        <td align="right">เลขบัญชี </td>
        <td style="border-bottom: 1px dotted;"><?= $model->base->account_no ?></td>
    </tr>
</table>
<br>
<table width="400px" align="center" collapse="0" cellspacing="0">
    <tr>
        <th style="border:1px solid">ลำดับ</th>
        <th style="border:1px solid" width="240px">รายการ</th>
        <th style="border:1px solid">จำนวนเงิน(บาท)</th>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;"></td>
        <td>รายรับ</td>
        <td style="border-left:1px solid;border-right:1px solid;"></td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">1.</td>
        <td>เงินเดือน</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->base_salary ? number_format($model->base_salary,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">2.</td>
        <td>เงินเดือน(ตกเบิก)</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->extra_salary ? number_format($model->extra_salary,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">3.</td>
        <td>ไม่ทำเวชปฏิบัติ</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->vp_salary ? number_format($model->vp_salary,2): '-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">4.</td>
        <td>เบี้ยเลี้ยงเหมาจ่าย (ฉ11)</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->v11_salary ? number_format($model->v11_salary,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">5.</td>
        <td>เงิน พตส. (งบร.)</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->pts_salary ? number_format($model->pts_salary,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">6.</td>
        <td>เงิน พตส. (งปม.)</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->pts_extra_salary ? number_format($model->pts_extra_salary,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">7.</td>
        <td>เงินตอบแทนนอกเวลา</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->ot_salary ? number_format($model->ot_salary,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">8.</td>
        <td>อื่นๆ</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->other_salary ? number_format($model->other_salary,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;border-bottom:1px solid;text-align:center;"></td>
        <td style="text-align: right;border-bottom:1px solid;">รวม&emsp;</td>
        <td style="border:1px solid;text-align:right;"><?= number_format($model->getSumSalary(),2) ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;"></td>
        <td>รายจ่าย</td>
        <td style="border-left:1px solid;border-right:1px solid;"></td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">1.</td>
        <td>ภาษี</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->tax_pay ? number_format($model->tax_pay,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">2.</td>
        <td>ค่าทุนเรือนหุ้น-เงินกู้สหกรณ์</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->om_pay ? number_format($model->om_pay,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">3.</td>
        <td>ประกันสังคม</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->sso_pay ? number_format($model->sso_pay,2): '-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">4.</td>
        <td>ฌกส.</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->dt_pay ? number_format($model->dt_pay,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">5.</td>
        <td>กยศ.</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->bank_pay ? number_format($model->bank_pay,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">6.</td>
        <td>ค่าบ้านพัก</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->kbk_pay ? number_format($model->kbk_pay,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">7.</td>
        <td>ค่าไฟฟ้า</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->elec_pay ? number_format($model->elec_pay,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">8.</td>
        <td>ค่าประกันต่างๆ</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->insurance_pay ? number_format($model->insurance_pay,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;text-align:center;">9.</td>
        <td>อื่นๆ</td>
        <td style="border-left:1px solid;border-right:1px solid;text-align:right;"><?= $model->other_pay ? number_format($model->other_pay,2):'-' ?>&ensp;</td>
    </tr>
    <tr>
        <td style="border-left:1px solid;border-right:1px solid;border-bottom:1px solid;text-align:center;"></td>
        <td style="text-align: right;border-bottom:1px solid;">รวม&emsp;</td>
        <td style="border:1px solid;text-align:right;"><?= number_format($model->getSumPay(),2) ?>&ensp;</td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: right;">รับสุทธิ&emsp;</td>
        <td style="border:1px solid;text-align:right;"><?= number_format(($model->getSumSalary() - $model->getSumPay()),2) ?>&ensp;</td>
    </tr>
</table>
<br>
<br>

<table width="400px" align="center" style="text-align: center;">
<tr>
    <td>
    <?= Html::img('images/sign.png',['width' => '100px']);?><br>   
    &emsp;&emsp;&emsp;&emsp;&emsp;ลงชื่อ ........................................ ผู้มีหน้าที่จ่ายเงิน
    </td>
</tr>
<tr>
    <td><?= EmpSalary::getThaiDate($model->salary_date) ?></td>
</tr>
<tr>
    <td>วัน เดือน ปี ที่ออกหนังสือรับรอง</td>
</tr>
</table>