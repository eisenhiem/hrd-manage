<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Salary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="salary-form">

    <?php $form = ActiveForm::begin(); ?>



    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <?= $form->field($model, 'salary_date')->widget(
                        DatePicker::ClassName(),
                        [
                            'name' => 'วันที่บันทึก',
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'options' => ['placeholder' => 'ระบุวันจ่าย'],
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ]
                        ]
                    );
                    ?>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'base_salary')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'extra_salary')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'ot_salary')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'v11_salary')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'pts_salary')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'pts_extra_salary')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'other_salary')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    รายจ่าย
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'tax_pay')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'kbk_pay')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'sso_pay')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'om_pay')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'dt_pay')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'elec_pay')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'bank_pay')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'insurance_pay')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'other_pay')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="form-group">
                <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success btn-block']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>