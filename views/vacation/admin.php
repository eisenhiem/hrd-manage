<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VacationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการวันลา';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacation-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'emp_id',
                'label' => 'ชื่อ-สกุล',
                'value' => function($model) { return $model->employees->getFullName() ;},
            ],
            'yearbudget',
            [
                'attribute' => 'va_type',
                'value' => function($model) { return $model->getTypeName() ;},
            ],
            [
                'attribute' => 'register_date',
                'value' => function($model) { return $model->getThaiDate($model->register_date) ;},
            ],
            [
                'attribute' => 'start_date',
                'value' => function($model) { return $model->getThaiDate($model->start_date) ;},
            ],
            'total_day',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'แก้ไขวันลา',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Yii::$app->user->identity->id == 1 ? Html::a('แก้ไขวันลา',['update','id'=>$model->va_id],['class' => 'btn btn-primary']):'';
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ยกเลิกวันลา',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-danger'],
                'template'=>'{delete}',
                'buttons'=>[
                    'delete' => function($url,$model,$key){
                        return Yii::$app->user->identity->id == 1 ? Html::a('ยกเลิกวันลา',['delete','id'=>$model->va_id],
                            [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'ต้องการยกเลิกวันลา ใช่หรือไม่?',
                                    'method' => 'post',
                                ],
                            ]):'';
                    }
                ]
            ],

        ],
    ]); ?>


</div>
