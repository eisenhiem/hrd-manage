<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Employees;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VacationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$emp = Employees::findOne($emp_id);
$this->title = 'วันลา '.$emp->getFullName();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacation-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if(Yii::$app->user->identity->id == 1){echo Html::a('จัดการวันลา', ['initla','id'=>$emp_id], ['class' => 'btn btn-success']);} ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'yearbudget',
            'brought_forward_days',
            'max_days',
            [
                'label' => 'วันลาพักผ่อนทั้งหมด',
                'value' => function($model) { return $model->getTotalVa() ;},
            ],
            [
                'label' => 'วันลาพักผ่อนคงเหลือ',
                'value' => function($model) { return $model->getRemainValue() ;},
            ],
            [
                'label' => 'วันลากิจ',
                'value' => function($model) { return $model->getGitValue() ;},
            ],
            [
                'label' => 'วันลาป่วย',
                'value' => function($model) { return $model->getSickValue() ;},
            ],
            [
                'label' => 'วันลาพักผ่อน',
                'value' => function($model) { return $model->getVacationValue() ;},
            ],
            [
                'label' => 'วันลาอื่นๆ',
                'value' => function($model) { return $model->getOtherValue() ;},
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'แก้ไขวันลา',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Yii::$app->user->identity->id == 1 ? Html::a('แก้ไขวันลา',['initupdate','id'=>$model->emp_id,'yearbudget'=>$model->yearbudget],['class' => 'btn btn-primary']):'';
                    }
                ]
            ],
        ],
    ]); ?>


</div>
