<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Departments;
use app\models\Employees;
use kartik\date\DatePicker;

$dep = ArrayHelper::map(Departments::find()->all(), 'dep_id', 'dep_name');
$emp = ArrayHelper::map(Employees::find()->all(),'emp_id','fname');

$yb = [2565 => '2565'];
/* @var $this yii\web\View */
/* @var $model app\models\Vacation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacation-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'yearbudget')->dropDownList($yb) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
        <?= $form->field($model, 'register_date')->widget(DatePicker::ClassName(),
            [
                'name' => 'register_date', 
                //'label' => 'ว้นที่เริ่มต้น',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่ส่งหนังสือ'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]); 
        ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'start_date')->widget(DatePicker::ClassName(),
            [
                'name' => 'start_date', 
                //'label' => 'ว้นที่เริ่มต้น',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่เริ่มลา'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]); 
        ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'end_date')->widget(DatePicker::ClassName(),
            [
                'name' => 'end_date', 
                //'label' => 'ว้นที่เริ่มต้น',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุลาถึงวันที่'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]); 
        ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'total_day')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'va_type')->radioList($model->getItemType()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('บันทึกวันลา', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
