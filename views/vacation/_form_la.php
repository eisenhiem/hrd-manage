<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$yb = [2565 => '2565'];

/* @var $this yii\web\View */
/* @var $model app\models\Vacation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'yearbudget')->dropDownList($yb) ?>

    <?= $form->field($model, 'brought_forward_days')->textInput() ?>

    <?= $form->field($model, 'max_days')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
