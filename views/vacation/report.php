<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VacationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายงานวันลา';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search_report', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'emp_id',
                'label' => 'ชื่อ-สกุล',
                'value' => function($model) { return $model->emp->getFullName() ;},
            ],
            'yearbudget',
            'brought_forward_days',
            'max_days',
            [
                'label' => 'วันลาพักผ่อนทั้งหมด',
                'value' => function($model) { return $model->getTotalVa() ;},
            ],
            [
                'label' => 'วันลาพักผ่อนคงเหลือ',
                'value' => function($model) { return $model->getRemainValue() ;},
            ],
            [
                'label' => 'วันลากิจ',
                'value' => function($model) { return $model->getGitValue() ;},
            ],
            [
                'label' => 'วันลาป่วย',
                'value' => function($model) { return $model->getSickValue() ;},
            ],
            [
                'label' => 'วันลาพักผ่อน',
                'value' => function($model) { return $model->getVacationValue() ;},
            ],
            [
                'label' => 'วันลาอื่นๆ',
                'value' => function($model) { return $model->getOtherValue() ;},
            ],
        ],
    ]); ?>

</div>
