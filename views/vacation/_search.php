<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Employees;

$emp = ArrayHelper::map(Employees::find()->all(),'emp_id','fname');

$yb = [2565 => '2565'];

/* @var $this yii\web\View */
/* @var $model app\models\VacationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'yearbudget')->dropDownList($yb) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'emp_id')->dropDownList($emp,['prompt'=>'']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'va_type')->dropDownList($model->getItemType()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
