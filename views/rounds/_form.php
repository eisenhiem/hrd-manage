<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rounds */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rounds-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'r_yearbudget')->textInput() ?>

    <?= $form->field($model, 'r_round')->textInput() ?>

    <?= $form->field($model, 'r_detail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->radioList(['0' => 'ไม่เปิดให้ประเมิน','1'=>'เปิดให้ประเมิน']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
