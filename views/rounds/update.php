<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rounds */

$this->title = 'Update Rounds: ' . $model->r_id;
$this->params['breadcrumbs'][] = ['label' => 'Rounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_id, 'url' => ['view', 'id' => $model->r_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rounds-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
