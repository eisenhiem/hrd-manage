<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rounds */

$this->title = 'เพิ่มรอบประเมิน';
$this->params['breadcrumbs'][] = ['label' => 'รอบประเมิน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rounds-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
