<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SumEva */

$this->title = 'Update Sum Eva: ' . $model->r_id;
$this->params['breadcrumbs'][] = ['label' => 'Sum Evas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_id, 'url' => ['view', 'r_id' => $model->r_id, 'emp_id' => $model->emp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sum-eva-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
