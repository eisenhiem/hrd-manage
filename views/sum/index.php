<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SumEvaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'แบบสรุปการประเมินความรู้';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sum-eva-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel'=>[
            'before'=>' '
            ],
        'options' => [ 'style' => 'table-layout:fixed;' ],
        'columns' => [
            [
                'attribute' => 'r_id',
                'headerOptions' => ['style' => 'width:12%'],
                'value' => function($model) { return $model->getRoundName(); },
            ],
            [
                'attribute' => 'emp_id',
                'headerOptions' => ['style' => 'width:11%'],
                'value' => function($model) { return $model->getFullName(); },
            ],
            [
                'attribute' => 'k1_gap',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'k2_gap',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'com_gap',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'eng_gap',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'cal_gap',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'data_gap',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'c1_score',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'c2_score',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'c3_score',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'c4_score',
                'headerOptions' => ['style' => 'width:7%'],
            ],
            [
                'attribute' => 'c5_score',
                'headerOptions' => ['style' => 'width:7%'],
            ],
        ],
    ]); ?>

</div>
