<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SumEva */

$this->title = $model->r_id;
$this->params['breadcrumbs'][] = ['label' => 'Sum Evas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sum-eva-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'r_id' => $model->r_id, 'emp_id' => $model->emp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'r_id' => $model->r_id, 'emp_id' => $model->emp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'r_id',
            'dep_id',
            'emp_id',
            'k1_gap',
            'k2_gap',
            'com_gap',
            'eng_gap',
            'cal_gap',
            'data_gap',
            'c1_score',
            'c2_score',
            'c3_score',
            'c4_score',
            'c5_score',
        ],
    ]) ?>

</div>
