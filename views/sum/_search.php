<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rounds;
$r = ArrayHelper::map(Rounds::find()->all(), 'r_id', 'r_detail');

use app\models\Departments;
$dep = ArrayHelper::map(Departments::find()->all(), 'dep_id', 'dep_name');

/* @var $this yii\web\View */
/* @var $model app\models\SumEvaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sum-eva-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'r_id')->dropDownList($r,['prompt'=>'']) ?>

    <?= $form->field($model, 'dep_id')->dropDownList($dep,['prompt'=>'']) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
