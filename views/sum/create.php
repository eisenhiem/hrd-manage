<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SumEva */

$this->title = 'Create Sum Eva';
$this->params['breadcrumbs'][] = ['label' => 'Sum Evas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sum-eva-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
