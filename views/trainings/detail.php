<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Trainings */

$this->title = $model->emp_id;
$this->params['breadcrumbs'][] = ['label' => 'แบบสำรวจความจำเป็นในการฝึกอบรมเพื่อพัฒนาความรู้', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

function getLevelName($l){
    return $l == 1 ? 'ระดับต้น' : 'ระดับสูง';
}

?>
<div class="trainings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach($model as $m){ ?>
        <table width="100%">
        <tr>
            <td style="text-align:center" colspan="5">
                <b><?= $m->getRoundName() ?></b>
            </td>
            <td style="text-align:right">
                <?= Html::a('พิมพ์', ['trainings/print', 'emp_id' => $m->emp_id, 'r_id' => $m->r_id], ['class' => 'btn btn-info']) ?>
            </td>
        </tr>    

        <tr style="height:50px">
            <th colspan="3" width="50%">
                หลักสูตรการพัฒนาสมรรถนะหลัก
            </th>
            <th colspan="3" width="50%">
                หลักสูตรการพัฒนา ภาษาอังกฤษ
            </th>
        </tr>
        <tr>
            <td width="60"><?= $m->service_mind ?></td>
            <td colspan="2">การมุ่งผลสัมฤทธิ์ (Achievement)</td>
            <td width="60"><?= $m->listening ?></td>
            <td >การฟัง</td>
            <td style="vertical-align:bottom"><?= getLevelName($m->listening_level) ?></td>
        </tr>
        <tr>
            <td width="60"><?= $m->achievment ?></td>
            <td colspan="2">การบริการดี (Service Mind)</td>
            <td width="60"><?= $m->reading ?></td>
            <td >การอ่าน</td>
            <td style="vertical-align:bottom"><?= getLevelName($m->reading_level) ?></td>
        </tr>
        <tr>
            <td width="60"><?= $m->expertise ?></td>
            <td colspan="2">การสั่งสมความเชี่ยวชาญในงานสายอาชีพ(Expertise)</td>
            <td width="60"><?= $m->speaking ?></td>
            <td >การพูด</td>
            <td style="vertical-align:bottom"><?= getLevelName($m->speaking_level) ?></td>
        </tr>
        <tr>
            <td width="60"><?= $m->integrity ?></td>
            <td colspan="2">จริยธรรม (Integrity)</td>
            <td width="60"><?= $m->writing ?></td>
            <td >การเขียน</td>
            <td style="vertical-align:bottom"><?= getLevelName($m->wrinting_level) ?></td>
        </tr>
        <tr>
            <td width="60"><?= $m->teamwork ?></td>
            <td colspan="2">ความร่วมแรงร่วมใจ (Team Work)</td>
            <td colspan="3"></td>
        </tr>
        <tr style="height:50px">
            <th colspan="3" width="50%">
                หลักสูตรการพัฒนาทักษะการใช้คอมพิวเตอร์
            </th>
            <th colspan="3" width="50%">
                ความรู้ที่จำเป็น
            </th>
        </tr>
        <tr>
            <td width="60"><?= $m->ms_word ?></td>
            <td >Microsoft Office Word</td>
            <td style="vertical-align:bottom"><?= getLevelName($m->ms_word_level) ?></td>
            <td width="60"><?= $m->laws ?></td>
            <td colspan="2">กฎหมาย ระเบียบที่สำคัญและเกี่ยวข้องกับการปฏิบัติราชการ</td>
        </tr>
        <tr>
            <td width="60"><?= $m->ms_excel ?></td>
            <td >Microsoft Office Excel</td>
            <td style="vertical-align:bottom"><?= getLevelName($m->ms_excel_level) ?></td>
            <td width="60"><?= $m->rm ?></td>
            <td colspan="2">การบริหารความเสี่ยง (Risk Managent)</td>
        </tr>
        <tr>
            <td width="60"><?= $m->ms_powerpoint ?></td>
            <td >Microsoft Office Powerpoint</td>
            <td style="vertical-align:bottom"><?= getLevelName($m->ms_powerpoint_level) ?></td>
            <td width="60"><?= $m->sm ?></td>
            <td colspan="2">แผนที่ยุทธศาสตร์ (Strategic Managent)</td>
        </tr>
        <tr>
            <td width="60"><?= $m->ms_access ?></td>
            <td >Microsoft Office Access</td>
            <td style="vertical-align:bottom"><?= getLevelName($m->ms_access_level) ?></td>
            <td width="60"><?= $m->cm ?></td>
            <td colspan="2">การบริหารเปลี่ยนแปลง (Change Managent)</td>
        </tr>
        <tr>
            <td width="60"><?= $m->e_mail ?></td>
            <td >E Mail</td>
            <td style="vertical-align:bottom"><?= getLevelName($m->e_mail_level) ?></td>
            <td width="60"><?= $m->pmqa ?></td>
            <td colspan="2">การบริหารจัดการภาครัฐ (PMQA)</td>
        </tr>
        <tr>
            <td><?= $m->other1_level ?></td>
            <td colspan="2" style="vertical-align:bottom"><?= $m->other1 ?></td>
            <td width="60"><?= $m->km ?></td>
            <td colspan="2">การจัดการความรู้ (Knowledge Managent)</td>
        </tr>
        <tr>
            <td colspan="3" rowspan="4"></td>
            <td width="60"><?= $m->bs ?></td>
            <td colspan="2">Balance Scorecard</td>
        </tr>
        <tr>
            <td width="60"><?= $m->sp ?></td>
            <td colspan="2">คุณธรรม/จริยธรรมในการทำงาน</td>
        </tr>
        <tr>
            <td width="60"><?= $m->hr ?></td>
            <td colspan="2">การบริหารทรัพยากรบุคคล</td>
        </tr>
        <tr>
            <td colspan="3"><?= $m->other2 ?></td>
        </tr>

    </table>
    <?php } ?>
</div>
