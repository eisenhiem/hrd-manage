<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Trainings */

$this->title = $model->emp_id;
$this->params['breadcrumbs'][] = ['label' => 'Trainings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="trainings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'emp_id' => $model->emp_id, 'r_id' => $model->r_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'emp_id' => $model->emp_id, 'r_id' => $model->r_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'emp_id',
            'r_id',
            'achievment',
            'service_mind',
            'expertise',
            'integrity',
            'teamwork',
            'reading',
            'reading_level',
            'listening',
            'listening_level',
            'speaking',
            'speaking_level',
            'writing',
            'wrinting_level',
            'ms_word',
            'ms_word_level',
            'ms_excel',
            'ms_excel_level',
            'ms_powerpoint',
            'ms_powerpoint_level',
            'e_mail',
            'e_mail_level',
            'other1',
            'other1_level',
            'laws',
            'rm',
            'sm',
            'cm',
            'pmqa',
            'km',
            'bs',
            'sp',
            'hr',
            'other2',
        ],
    ]) ?>

</div>
