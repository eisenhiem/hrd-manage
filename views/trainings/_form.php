<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Rounds;

$r = ArrayHelper::map(Rounds::find()->where(['is_active'=>'1'])->all(), 'r_id', 'r_detail');
$l = [1 => 'ระดับพื้นฐาน', 2 =>'ระดับสูง'];
/* @var $this yii\web\View */
/* @var $model app\models\Trainings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trainings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'r_id')->dropDownList($r,['prompt'=>$r[0]]) ?>

    ## โปรดพิจารณารายละเอียดในคอลัมน์ข้อสมรรถนะหลักข้อราชการ ความรู้ที่จำเป็น และทักษะที่ำเป็นในหัวข้อย่อยใดบ้างที่จำเป็นต่อการพัฒนาตนเอง <i>โดยใส่หมายเลย 1</i> ลงใน [ &nbsp; ] หน้าข้อที่เห็นว่าจำเป็นมากที่สุด และ 2,3,... ตามลำดับ<br>
    <b><font color="red">*</font> ท่านสามารถระบุและเลือกหัวข้อความรู้ที่ต้องการฝึกอบรมได้มากกว่า 1 รายการ</b>
    <table width="100%">
        <tr style="height:50px">
            <th colspan="3" width="50%">
                หลักสูตรการพัฒนาสมรรถนะหลัก
            </th>
            <th colspan="3" width="50%">
                หลักสูตรการพัฒนา ภาษาอังกฤษ
            </th>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'service_mind')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">การมุ่งผลสัมฤทธิ์ (Achievement)</td>
            <td width="60"><?= $form->field($model, 'listening')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td >การฟัง</td>
            <td style="vertical-align:bottom"><?= $form->field($model, 'listening_level')->radioList($l)->label(false) ?></td>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'achievment')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">การบริการดี (Service Mind)</td>
            <td width="60"><?= $form->field($model, 'reading')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td >การอ่าน</td>
            <td style="vertical-align:bottom"><?= $form->field($model, 'reading_level')->radioList($l)->label(false) ?></td>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'expertise')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">การสั่งสมความเชี่ยวชาญในงานสายอาชีพ(Expertise)</td>
            <td width="60"><?= $form->field($model, 'speaking')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td >การพูด</td>
            <td style="vertical-align:bottom"><?= $form->field($model, 'speaking_level')->radioList($l)->label(false) ?></td>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'integrity')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">จริยธรรม (Integrity)</td>
            <td width="60"><?= $form->field($model, 'writing')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td >การเขียน</td>
            <td style="vertical-align:bottom"><?= $form->field($model, 'wrinting_level')->radioList($l)->label(false) ?></td>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'teamwork')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">ความร่วมแรงร่วมใจ (Team Work)</td>
            <td colspan="3"></td>
        </tr>
        <tr style="height:50px">
            <th colspan="3" width="50%">
                หลักสูตรการพัฒนาทักษะการใช้คอมพิวเตอร์
            </th>
            <th colspan="3" width="50%">
                ความรู้ที่จำเป็น
            </th>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'ms_word')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td >Microsoft Office Word</td>
            <td style="vertical-align:bottom"><?= $form->field($model, 'ms_word_level')->radioList($l)->label(false) ?></td>
            <td width="60"><?= $form->field($model, 'laws')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">กฎหมาย ระเบียบที่สำคัญและเกี่ยวข้องกับการปฏิบัติราชการ</td>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'ms_excel')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td >Microsoft Office Excel</td>
            <td style="vertical-align:bottom"><?= $form->field($model, 'ms_excel_level')->radioList($l)->label(false) ?></td>
            <td width="60"><?= $form->field($model, 'rm')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">การบริหารความเสี่ยง (Risk Managent)</td>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'ms_powerpoint')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td >Microsoft Office Powerpoint</td>
            <td style="vertical-align:bottom"><?= $form->field($model, 'ms_powerpoint_level')->radioList($l)->label(false) ?></td>
            <td width="60"><?= $form->field($model, 'sm')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">แผนที่ยุทธศาสตร์ (Strategic Managent)</td>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'ms_access')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td >Microsoft Office Access</td>
            <td style="vertical-align:bottom"><?= $form->field($model, 'ms_access_level')->radioList($l)->label(false) ?></td>
            <td width="60"><?= $form->field($model, 'cm')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">การบริหารเปลี่ยนแปลง (Change Managent)</td>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'e_mail')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td >E Mail</td>
            <td style="vertical-align:bottom"><?= $form->field($model, 'e_mail_level')->radioList($l)->label(false) ?></td>
            <td width="60"><?= $form->field($model, 'pmqa')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">การบริหารจัดการภาครัฐ (PMQA)</td>
        </tr>
        <tr>
            <td><?= $form->field($model, 'other1_level')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2" style="vertical-align:bottom"><?= $form->field($model, 'other1')->textInput(['style'=>'width:300px'])->label('อื่นๆ',['options' => ['class' => 'form-group form-inline'],]) ?></td>
            <td width="60"><?= $form->field($model, 'km')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">การจัดการความรู้ (Knowledge Managent)</td>
        </tr>
        <tr>
            <td colspan="3" rowspan="4"></td>
            <td width="60"><?= $form->field($model, 'bs')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">Balance Scorecard</td>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'sp')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">คุณธรรม/จริยธรรมในการทำงาน</td>
        </tr>
        <tr>
            <td width="60"><?= $form->field($model, 'hr')->textInput(['style'=>'width:50px'])->label(false) ?></td>
            <td colspan="2">การบริหารทรัพยากรบุคคล</td>
        </tr>
        <tr>
            <td colspan="3"><?= $form->field($model, 'other2')->textInput(['style'=>'width:300px'])->label('อื่นๆ') ?></td>
        </tr>

    </table>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
