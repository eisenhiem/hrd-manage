<?php
use yii\helpers\Html;

function getLevelName($l){
    return $l == 1 ? 'ระดับต้น' : 'ระดับสูง';
}

?>
<div style="font-size:20px; text-align:center;"><b>แบบสำรวจความจำเป็นในการฝึกอบรมเพื่อพัฒนาความรู้</b></div>
<div style="text-align:center;">สำนักงานสาธารณสุขจังหวัดอุบล <?= $model->getRoundName(); ?></div>
<p>
<b>ชื่อ - นามสกุล</b> <?= $employee->pname.$employee->fname.' '.$employee->lname ?><br>
<b>ตำแหน่ง</b> <?= $employee->position ?> <b>ระดับ</b> <?= $employee->getLevelName() ?> <b>ประเภทตำแหน่ง</b> <?= $employee->getTypeName() ?><br>
<b>อายุ</b> <?= $employee->getAge()?> ปี <b>อายุราชการ</b> <?= $employee->getAgeJob() ?> ปี <b>อายุราชการที่เหลือ</b> <?= 60-$employee->getAge() ?> ปี<br>
<b>วุฒิการศึกษาสูงสุด</b><?= $employee->education ?> <b>สาขา</b> <?= $employee->branch ?><br>
<b>กลุ่มงาน/งาน</b> <?= $employee->getDepName() ?><br>

<table width="100%">
        <tr>
            <td style="text-align:center" colspan="6">
                <b><?= $model->getRoundName() ?></b>
            </td>
        </tr>    
        <tr style="height:50px">
            <th colspan="3" width="50%">
                หลักสูตรการพัฒนาสมรรถนะหลัก
            </th>
            <th colspan="3" width="50%">
                หลักสูตรการพัฒนา ภาษาอังกฤษ
            </th>
        </tr>
        <tr>
            <td width="5%" style="text-align:center" style="text-align:center"><?= $model->service_mind ?></td>
            <td width="45%" colspan="2">การมุ่งผลสัมฤทธิ์ (Achievement)</td>
            <td width="5%" style="text-align:center"><?= $model->listening ?></td>
            <td width="35%">การฟัง</td>
            <td width="10%"><?= getLevelName($model->listening_level) ?></td>
        </tr>
        <tr>
            <td width="5%" style="text-align:center"><?= $model->achievment ?></td>
            <td width="45%" colspan="2">การบริการดี (Service Mind)</td>
            <td width="5%" style="text-align:center"><?= $model->reading ?></td>
            <td width="35%">การอ่าน</td>
            <td width="10%"><?= getLevelName($model->reading_level) ?></td>
        </tr>
        <tr>
            <td  width="5%" style="text-align:center"><?= $model->expertise ?></td>
            <td colspan="2">การสั่งสมความเชี่ยวชาญในงานสายอาชีพ(Expertise)</td>
            <td  width="5%" style="text-align:center"><?= $model->speaking ?></td>
            <td >การพูด</td>
            <td width="10%"><?= getLevelName($model->speaking_level) ?></td>
        </tr>
        <tr>
            <td  width="5%" style="text-align:center"><?= $model->integrity ?></td>
            <td colspan="2">จริยธรรม (Integrity)</td>
            <td  width="5%" style="text-align:center"><?= $model->writing ?></td>
            <td >การเขียน</td>
            <td width="10%"><?= getLevelName($model->wrinting_level) ?></td>
        </tr>
        <tr>
            <td  width="5%" style="text-align:center"><?= $model->teamwork ?></td>
            <td colspan="2">ความร่วมแรงร่วมใจ (Team Work)</td>
            <td colspan="3"></td>
        </tr>
        <tr style="height:50px">
            <th colspan="3" width="50%">
                หลักสูตรการพัฒนาทักษะการใช้คอมพิวเตอร์
            </th>
            <th colspan="3" width="50%">
                ความรู้ที่จำเป็น
            </th>
        </tr>
        <tr>
            <td  width="5%" style="text-align:center"><?= $model->ms_word ?></td>
            <td >Microsoft Office Word</td>
            <td width="10%"><?= getLevelName($model->ms_word_level) ?></td>
            <td  width="5%" style="text-align:center"><?= $model->laws ?></td>
            <td colspan="2">กฎหมาย ระเบียบที่สำคัญและเกี่ยวข้องกับการปฏิบัติราชการ</td>
        </tr>
        <tr>
            <td  width="5%" style="text-align:center"><?= $model->ms_excel ?></td>
            <td >Microsoft Office Excel</td>
            <td width="10%"><?= getLevelName($model->ms_excel_level) ?></td>
            <td  width="5%" style="text-align:center"><?= $model->rm ?></td>
            <td colspan="2">การบริหารความเสี่ยง (Risk Managent)</td>
        </tr>
        <tr>
            <td  width="5%" style="text-align:center"><?= $model->ms_powerpoint ?></td>
            <td >Microsoft Office Powerpoint</td>
            <td width="10%"><?= getLevelName($model->ms_powerpoint_level) ?></td>
            <td  width="5%" style="text-align:center"><?= $model->sm ?></td>
            <td colspan="2">แผนที่ยุทธศาสตร์ (Strategic Managent)</td>
        </tr>
        <tr>
            <td  width="5%" style="text-align:center"><?= $model->ms_access ?></td>
            <td >Microsoft Office Access</td>
            <td width="10%"><?= getLevelName($model->ms_access_level) ?></td>
            <td  width="5%" style="text-align:center"><?= $model->cm ?></td>
            <td colspan="2">การบริหารเปลี่ยนแปลง (Change Managent)</td>
        </tr>
        <tr>
            <td  width="5%" style="text-align:center"><?= $model->e_mail ?></td>
            <td >E Mail</td>
            <td width="10%"><?= getLevelName($model->e_mail_level) ?></td>
            <td  width="5%" style="text-align:center"><?= $model->pmqa ?></td>
            <td colspan="2">การบริหารจัดการภาครัฐ (PMQA)</td>
        </tr>
        <tr>
            <td width="5%" style="text-align:center"><?= $model->other1_level ?></td>
            <td colspan="2" width="10%"><?= $model->other1 ?></td>
            <td  width="5%" style="text-align:center"><?= $model->km ?></td>
            <td colspan="2">การจัดการความรู้ (Knowledge Managent)</td>
        </tr>
        <tr>
            <td colspan="3" rowspan="4"></td>
            <td  width="5%" style="text-align:center"><?= $model->bs ?></td>
            <td colspan="2">Balance Scorecard</td>
        </tr>
        <tr>
            <td  width="5%" style="text-align:center"><?= $model->sp ?></td>
            <td colspan="2">คุณธรรม/จริยธรรมในการทำงาน</td>
        </tr>
        <tr>
            <td  width="5%" style="text-align:center"><?= $model->hr ?></td>
            <td colspan="2">การบริหารทรัพยากรบุคคล</td>
        </tr>
        <tr>
            <td colspan="3"><?= $model->other2 ?></td>
        </tr>

    </table>
