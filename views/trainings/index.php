<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trainings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainings-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มการเรียนรู้', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'emp_id',
            //'r_id',
            'achievment',
            'service_mind',
            'expertise',
            //'integrity',
            //'teamwork',
            //'reading',
            //'reading_level',
            //'listening',
            //'listening_level',
            //'speaking',
            //'speaking_level',
            //'writing',
            //'wrinting_level',
            //'ms_word',
            //'ms_word_level',
            //'ms_excel',
            //'ms_excel_level',
            //'ms_powerpoint',
            //'ms_powerpoint_level',
            //'e_mail',
            //'e_mail_level',
            //'other1',
            //'other1_level',
            //'laws',
            //'rm',
            //'sm',
            //'cm',
            //'pmqa',
            //'km',
            //'bs',
            //'sp',
            //'hr',
            //'other2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
