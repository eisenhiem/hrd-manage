<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trainings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'emp_id') ?>

    <?= $form->field($model, 'r_id') ?>

    <?= $form->field($model, 'achievment') ?>

    <?= $form->field($model, 'service_mind') ?>

    <?= $form->field($model, 'expertise') ?>

    <?php // echo $form->field($model, 'integrity') ?>

    <?php // echo $form->field($model, 'teamwork') ?>

    <?php // echo $form->field($model, 'reading') ?>

    <?php // echo $form->field($model, 'reading_level') ?>

    <?php // echo $form->field($model, 'listening') ?>

    <?php // echo $form->field($model, 'listening_level') ?>

    <?php // echo $form->field($model, 'speaking') ?>

    <?php // echo $form->field($model, 'speaking_level') ?>

    <?php // echo $form->field($model, 'writing') ?>

    <?php // echo $form->field($model, 'wrinting_level') ?>

    <?php // echo $form->field($model, 'ms_word') ?>

    <?php // echo $form->field($model, 'ms_word_level') ?>

    <?php // echo $form->field($model, 'ms_excel') ?>

    <?php // echo $form->field($model, 'ms_excel_level') ?>

    <?php // echo $form->field($model, 'ms_powerpoint') ?>

    <?php // echo $form->field($model, 'ms_powerpoint_level') ?>

    <?php // echo $form->field($model, 'e_mail') ?>

    <?php // echo $form->field($model, 'e_mail_level') ?>

    <?php // echo $form->field($model, 'other1') ?>

    <?php // echo $form->field($model, 'other1_level') ?>

    <?php // echo $form->field($model, 'laws') ?>

    <?php // echo $form->field($model, 'rm') ?>

    <?php // echo $form->field($model, 'sm') ?>

    <?php // echo $form->field($model, 'cm') ?>

    <?php // echo $form->field($model, 'pmqa') ?>

    <?php // echo $form->field($model, 'km') ?>

    <?php // echo $form->field($model, 'bs') ?>

    <?php // echo $form->field($model, 'sp') ?>

    <?php // echo $form->field($model, 'hr') ?>

    <?php // echo $form->field($model, 'other2') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
