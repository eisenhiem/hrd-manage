<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Trainings */

$this->title = 'แบบสำรวจความจำเป็นในการฝึกอบรมเพื่อพัฒนาความรู้';
$this->params['breadcrumbs'][] = ['label' => 'การฝึกอบรม', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
