<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompetencySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="competency-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'comp_id') ?>

    <?= $form->field($model, 'r_id') ?>

    <?= $form->field($model, 'emp_id') ?>

    <?= $form->field($model, 'comp_no') ?>

    <?= $form->field($model, 'evaluate_by') ?>

    <?php // echo $form->field($model, 'level') ?>

    <?php // echo $form->field($model, 'evaluate') ?>

    <?php // echo $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
