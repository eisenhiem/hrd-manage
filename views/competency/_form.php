<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Rounds;
use app\models\Core;

/* @var $this yii\web\View */
/* @var $model app\models\Competency */
/* @var $form yii\widgets\ActiveForm */
$r = ArrayHelper::map(Rounds::find()->where(['is_active'=>'1'])->all(), 'r_id', 'r_detail');
$c = ArrayHelper::map(Core::find()->all(), 'core_id', 'core_name');
$score = [0 => '0', 1=>'1', 2=>'2', 3=>'3', 4=>'4', 5=>'5'];

?>

<div class="competency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'r_id')->dropDownList($r,['prompt'=>$r[0]]) ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'core_id')->dropDownList($c, ['prompt' => '']) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'level')->textInput() ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'evaluate')->textInput() ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'weight')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'comment')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
