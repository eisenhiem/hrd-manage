<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Eva */

$this->title = 'แบบฟอร์มการประเมินความรู้ ทักษะบุคลากร';
$this->params['breadcrumbs'][] = ['label' => 'แบบประเมิน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eva-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
