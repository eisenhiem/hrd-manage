<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EvaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Evas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eva-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Eva', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'eva_id',
            'emp_id',
            'r_id',
            'k1_std_score',
            'k1_eva_score',
            //'k1_gap',
            //'k1_subject',
            //'k1_self',
            //'k1_boss',
            //'k1_workshop',
            //'k2_std_score',
            //'k2_eva_score',
            //'k2_gap',
            //'k2_subject',
            //'k2_self',
            //'k2_boss',
            //'k2_workshop',
            //'com_std_score',
            //'com_eva_score',
            //'com_gap',
            //'com_subject',
            //'com_self',
            //'com_boss',
            //'com_workshop',
            //'eng_std_score',
            //'eng_eva_score',
            //'eng_gap',
            //'eng_subject',
            //'eng_self',
            //'eng_boss',
            //'eng_workshop',
            //'cal_std_score',
            //'cal_eva_score',
            //'cal_gap',
            //'cal_subject',
            //'cal_self',
            //'cal_boss',
            //'cal_workshop',
            //'data_std_score',
            //'data_eva_score',
            //'data_gap',
            //'data_subject',
            //'data_self',
            //'data_boss',
            //'data_workshop',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
