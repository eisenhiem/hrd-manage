<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EvaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eva-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'eva_id') ?>

    <?= $form->field($model, 'emp_id') ?>

    <?= $form->field($model, 'r_id') ?>

    <?= $form->field($model, 'k1_std_score') ?>

    <?= $form->field($model, 'k1_eva_score') ?>

    <?php // echo $form->field($model, 'k1_gap') ?>

    <?php // echo $form->field($model, 'k1_subject') ?>

    <?php // echo $form->field($model, 'k1_self') ?>

    <?php // echo $form->field($model, 'k1_boss') ?>

    <?php // echo $form->field($model, 'k1_workshop') ?>

    <?php // echo $form->field($model, 'k2_std_score') ?>

    <?php // echo $form->field($model, 'k2_eva_score') ?>

    <?php // echo $form->field($model, 'k2_gap') ?>

    <?php // echo $form->field($model, 'k2_subject') ?>

    <?php // echo $form->field($model, 'k2_self') ?>

    <?php // echo $form->field($model, 'k2_boss') ?>

    <?php // echo $form->field($model, 'k2_workshop') ?>

    <?php // echo $form->field($model, 'com_std_score') ?>

    <?php // echo $form->field($model, 'com_eva_score') ?>

    <?php // echo $form->field($model, 'com_gap') ?>

    <?php // echo $form->field($model, 'com_subject') ?>

    <?php // echo $form->field($model, 'com_self') ?>

    <?php // echo $form->field($model, 'com_boss') ?>

    <?php // echo $form->field($model, 'com_workshop') ?>

    <?php // echo $form->field($model, 'eng_std_score') ?>

    <?php // echo $form->field($model, 'eng_eva_score') ?>

    <?php // echo $form->field($model, 'eng_gap') ?>

    <?php // echo $form->field($model, 'eng_subject') ?>

    <?php // echo $form->field($model, 'eng_self') ?>

    <?php // echo $form->field($model, 'eng_boss') ?>

    <?php // echo $form->field($model, 'eng_workshop') ?>

    <?php // echo $form->field($model, 'cal_std_score') ?>

    <?php // echo $form->field($model, 'cal_eva_score') ?>

    <?php // echo $form->field($model, 'cal_gap') ?>

    <?php // echo $form->field($model, 'cal_subject') ?>

    <?php // echo $form->field($model, 'cal_self') ?>

    <?php // echo $form->field($model, 'cal_boss') ?>

    <?php // echo $form->field($model, 'cal_workshop') ?>

    <?php // echo $form->field($model, 'data_std_score') ?>

    <?php // echo $form->field($model, 'data_eva_score') ?>

    <?php // echo $form->field($model, 'data_gap') ?>

    <?php // echo $form->field($model, 'data_subject') ?>

    <?php // echo $form->field($model, 'data_self') ?>

    <?php // echo $form->field($model, 'data_boss') ?>

    <?php // echo $form->field($model, 'data_workshop') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
