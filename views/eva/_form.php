<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Rounds;

$r = ArrayHelper::map(Rounds::find()->where(['is_active'=>'1'])->all(), 'r_id', 'r_detail');
$score = [0 => '0', 1=>'1', 2=>'2', 3=>'3', 4=>'4', 5=>'5'];
$self = ['S'=>'เรียนรู้ด้วยตนเอง','C'=>'เข้ารับการศึกษาต่อนื่อง','E'=>'เรียนทางอิเล็คทรอนิก'];
$boss = ['O'=>'สอนขณะปฏิบัติงานจริง','A' => 'มอบหมายงาน/โครงการ', 'J' => 'การหมุนเวียนงาน'];

/* @var $this yii\web\View */
/* @var $model app\models\Eva */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eva-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'r_id')->dropDownList($r,['prompt'=>$r[0]]) ?>

    <table width="100%">
        <tr bgcolor="Cornsilk">
            <th style="text-align:center" width="20%">ความรู้/ทักษะ</th>
            <th style="text-align:center" width="10%">ค่ามาตรฐาน</th>
            <th style="text-align:center" width="10%">ระดับผลการประเมิน</th>
            <th style="text-align:center" width="20%">ประเด็นพัฒนา</th>
            <th style="text-align:center" width="10%">พัฒนาด้วย<br>ตนเอง</th>
            <th style="text-align:center" width="10%">พัฒนาด้วย<br>ผู้บังคับบัญชา</th>
            <th style="text-align:center" width="20%">อบรม</th>
        </tr>
        <tr>
            <td colspan="7" bgcolor="Navy"><font size=3 color="white">&nbsp &nbsp<u><b>ความรู้</b></u></font></td>
        </tr>
        <tr>
            <td>ความรู้ที่จำเป็นในการปฏิบัติงานตามตำแหน่ง</td>
            <td>
                <?= $form->field($model, 'k1_std_score')->dropDownList($score,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'k1_eva_score')->dropDownList($score,['prompt'=>''])->label(false) ?>            
            </td>
            <td>
                <?= $form->field($model, 'k1_subject')->textInput(['maxlength' => true])->label(false) ?>
            </td>
            <td>
            <?= $form->field($model, 'k1_self')->dropDownList($self,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'k1_boss')->dropDownList($boss,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'k1_workshop')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td>ความรู้เกี่ยวกับกฎหมายและระเบียบราชการ</td>
            <td>
                <?= $form->field($model, 'k2_std_score')->dropDownList($score,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'k2_eva_score')->dropDownList($score,['prompt'=>''])->label(false) ?>            
            </td>
            <td>
                <?= $form->field($model, 'k2_subject')->textInput(['maxlength' => true])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'k2_self')->dropDownList($self,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'k2_boss')->dropDownList($boss,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'k2_workshop')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
        <tr>
        <td colspan="7" bgcolor="Navy"><font size=3 color="white">&nbsp &nbsp<u><b>ทักษะ</b></u></font></td>
        </tr>
        <tr>
            <td>การใช้คอมพิวเตอร์</td>
            <td>
                <?= $form->field($model, 'com_std_score')->dropDownList($score,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'com_eva_score')->dropDownList($score,['prompt'=>''])->label(false) ?>            
            </td>
            <td>
                <?= $form->field($model, 'com_subject')->textInput(['maxlength' => true])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'com_self')->dropDownList($self,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'com_boss')->dropDownList($boss,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'com_workshop')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td>การใช้ภาษาอังกฤษ</td>
            <td>
                <?= $form->field($model, 'eng_std_score')->dropDownList($score,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'eng_eva_score')->dropDownList($score,['prompt'=>''])->label(false) ?>            
            </td>
            <td>
                <?= $form->field($model, 'eng_subject')->textInput(['maxlength' => true])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'eng_self')->dropDownList($self,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'eng_boss')->dropDownList($boss,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'eng_workshop')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td>การคำนวน</td>
            <td>
                <?= $form->field($model, 'cal_std_score')->dropDownList($score,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'cal_eva_score')->dropDownList($score,['prompt'=>''])->label(false) ?>            
            </td>
            <td>
                <?= $form->field($model, 'cal_subject')->textInput(['maxlength' => true])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'cal_self')->dropDownList($self,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'cal_boss')->dropDownList($boss,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'cal_workshop')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td>การจัดการข้อมูล</td>
            <td>
                <?= $form->field($model, 'data_std_score')->dropDownList($score,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'data_eva_score')->dropDownList($score,['prompt'=>''])->label(false) ?>            
            </td>
            <td>
                <?= $form->field($model, 'data_subject')->textInput(['maxlength' => true])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'data_self')->dropDownList($self,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'data_boss')->dropDownList($boss,['prompt'=>''])->label(false) ?>
            </td>
            <td>
                <?= $form->field($model, 'data_workshop')->textInput(['maxlength' => true])->label(false) ?>
            </td>
        </tr>
    </table>
    <p>
    <h3>คำอธิบาย ค่ามาตรฐานความรู้ ทักษะ และสมรรถนะของตำแหน่ง ตาม ก.พ. กำหนด</h3>
    <table width="100%" border="1" cellpadding="2" cellspacing="0">
        <tr>
            <th rowspan="2" width="15%" style="text-align:center">ประเภท/ระดับ</th>
            <th colspan="2" width="36%" style="text-align:center">ระดับความรู้ความสามารถ</th>
            <th colspan="4" width="44%" style="text-align:center">ระดับทักษะ</th>
            <th rowspan="2" width="10%" style="text-align:center">สมรรถนะหลัก</th>
        </tr>
        <tr>
            <th style="text-align:center">ความรู้ความสามารถที่จำเป็น สำหรับการปฏิบัติงาน</th>
            <th style="text-align:center">ความรู้เรื่องกฎหมาย กฎระเบียบราชการ</th>
            <th style="text-align:center">การใช้คอมพิวเตอร์</th>
            <th style="text-align:center">การใช้ภาษาอังกฤษ</th>
            <th style="text-align:center">การคำนวน</th>
            <th style="text-align:center">การจัดการข้อมูล</th>
        </tr>
        <tr>
            <td colspan="8" style="text-align:center">ประเภททั่วไป</td>
        </tr>
        <tr>
            <td>ปฏิบัติงาน</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
        </tr>
        <tr>
            <td>ชำนาญงาน</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">1</td>
        </tr>
        <tr>
            <td>อาวุโส</td>
            <td style="text-align:center">3</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
        </tr>
        <tr>
            <td>ทักษะพิเศษ</td>
            <td style="text-align:center">4</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">3</td>
        </tr>
        <tr>
            <td colspan="8" style="text-align:center">ประเภทวิชาการ</td>
        </tr>
        <tr>
            <td>ปฏิบัติการ</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">1</td>
        </tr>
        <tr>
            <td>ชำนาญการ</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
        </tr>
        <tr>
            <td>ชำนาญการพิเศษ</td>
            <td style="text-align:center">3</td>
            <td style="text-align:center">3</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">3</td>
        </tr>
        <tr>
            <td>เชี่ยวชาญ</td>
            <td style="text-align:center">4</td>
            <td style="text-align:center">3</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">4</td>
        </tr>
        <tr>
            <td colspan="8" style="text-align:center">ประเภทอำนวยการ</td>
        </tr>
        <tr>
            <td>ระดับต้น</td>
            <td style="text-align:center">1</td>
            <td style="text-align:center">3</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">3</td>
        </tr>
        <tr>
            <td>ระดับสูง</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">3</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">2</td>
            <td style="text-align:center">4</td>
        </tr>
    </table>
</p>
    <?= $form->field($model, 'emp_id')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึกผลการประเมิน', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
