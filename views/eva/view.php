<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Eva */

$this->title = $model->eva_id;
$this->params['breadcrumbs'][] = ['label' => 'Evas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="eva-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('พิมพ์', ['print', 'id' => $model->eva_id], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->eva_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->eva_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= $model->r_id ?>

    <table width="100%">
        <tr bgcolor="Cornsilk">
            <th style="text-align:center" width="20%">ความรู้/ทักษะ</th>
            <th style="text-align:center" width="8%">ค่ามาตรฐาน</th>
            <th style="text-align:center" width="8%">ระดับผลการประเมิน</th>
            <th style="text-align:center" width="4%">Gap</th>
            <th style="text-align:center" width="20%">ประเด็นพัฒนา</th>
            <th style="text-align:center" width="10%">พัฒนาด้วย<br>ตนเอง</th>
            <th style="text-align:center" width="10%">พัฒนาด้วย<br>ผู้บังคับบัญชา</th>
            <th style="text-align:center" width="20%">อบรม</th>
        </tr>
        <tr>
            <td colspan="8" bgcolor="Navy"><font size=3 color="white">&nbsp &nbsp<u><b>ความรู้</b></u></font></td>
        </tr>
        <tr>
            <td>ความรู้ที่จำเป็นในการปฏิบัติงานตามตำแหน่ง</td>
            <td style="text-align:center">
                <?= $model->k1_std_score ?>
            </td>
            <td style="text-align:center"> 
                <?= $model->k1_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->k1_gap ?>
            </td>
            <td>
                <?= $model->k1_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->k1_self ?>
            </td>
            <td style="text-align:center">
                <?= $model->k1_boss ?>
            </td>
            <td>
                <?= $model->k1_workshop ?>
            </td>
        </tr>
        <tr>
            <td>ความรู้เกี่ยวกับกฎหมายและระเบียบราชการ</td>
            <td style="text-align:center">
                <?= $model->k2_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $model->k2_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->k2_gap ?>
            </td>
            <td>
                <?= $model->k2_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->k2_self ?>
            </td>
            <td style="text-align:center">
                <?= $model->k2_boss ?>
            </td>
            <td>
                <?= $model->k2_workshop ?>
            </td>
        </tr>
        <tr>
        <td colspan="8" bgcolor="Navy"><font size=3 color="white">&nbsp &nbsp<u><b>ทักษะ</b></u></font></td>
        </tr>
        <tr>
            <td>การใช้คอมพิวเตอร์</td>
            <td style="text-align:center">
                <?= $model->com_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $model->com_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->com_gap ?>
            </td>
            <td>
                <?= $model->com_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->com_self ?>
            </td>
            <td style="text-align:center">
                <?= $model->com_boss ?>
            </td>
            <td>
                <?= $model->com_workshop ?>
            </td>
        <tr>
            <td>การใช้ภาษาอังกฤษ</td>
            <td style="text-align:center">
                <?= $model->eng_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $model->eng_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->eng_gap ?>
            </td>
            <td>
                <?= $model->eng_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->eng_self ?>
            </td>
            <td style="text-align:center">
                <?= $model->eng_boss ?>
            </td>
            <td>
                <?= $model->eng_workshop ?>
            </td>
        </tr>
        <tr>
            <td>การคำนวน</td>
            <td style="text-align:center">
                <?= $model->cal_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $model->cal_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->cal_gap ?>
            </td>
            <td>
                <?= $model->cal_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->cal_self ?>
            </td>
            <td style="text-align:center">
                <?= $model->cal_boss ?>
            </td>
            <td>
                <?= $model->cal_workshop ?>
            </td>
        </tr>
        <tr>
            <td>การจัดการข้อมูล</td>
            <td style="text-align:center">
                <?= $model->data_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $model->data_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->data_gap ?>
            </td>
            <td>
                <?= $model->data_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->data_self ?>
            </td>
            <td style="text-align:center">
                <?= $model->data_boss ?>
            </td>
            <td>
                <?= $model->data_workshop ?>
            </td>
        </tr>
    </table>

</div>
