<?php
use yii\helpers\Html;
?>
<div style="font-size:20px; text-align:center;"><b>แบบฟอร์มประเมินความรู้ ทักษะบุคลากร</b></div>
<div style="text-align:center;">สำนักงานสาธารณสุขจังหวัดอุบล <?= $model->getRoundName(); ?></div>
<p>
<b>ชื่อ - นามสกุล</b> <?= $employee->pname.$employee->fname.' '.$employee->lname ?><br>
<b>ตำแหน่ง</b> <?= $employee->position ?> <b>ระดับ</b> <?= $employee->getLevelName() ?> <b>ประเภทตำแหน่ง</b> <?= $employee->getTypeName() ?><br>
<b>อายุ</b> <?= $employee->getAge()?> ปี <b>อายุราชการ</b> <?= $employee->getAgeJob() ?> ปี <b>อายุราชการที่เหลือ</b> <?= 60-$employee->getAge() ?> ปี<br>
<b>วุฒิการศึกษาสูงสุด</b><?= $employee->education ?> <b>สาขา</b> <?= $employee->branch ?><br>
<b>กลุ่มงาน/งาน</b> <?= $employee->getDepName() ?><br>
<b>งานที่รับผิดชอบ</b>
<div class="col-md-6">
    1. <?= $employee->job1 ?>
</div>
<div class="col-md-6">
    2. <?= $employee->job2 ?>
</div>
<div class="col-md-6">
    3. <?= $employee->job3 ?>
</div>
<div class="col-md-6">
    4. <?= $employee->job4 ?>
</div>
<div class="col-md-6">
    5. <?= $employee->job5 ?>
</div>
<div class="col-md-6">
    6. <?= $employee->job6 ?>
</div>
</p>
<br>
<table width="100%" border="1" cellpadding="2" cellspacing="0">
        <tr>
            <th style="text-align:center" width="200" rowspan="3">ความรู้/ทักษะ</th>
            <th style="text-align:center; font-size:10px;" width="70" rowspan="3">ค่ามาตรฐานของตำแหน่ง</th>
            <th style="text-align:center; font-size:10px;" width="60" rowspan="3">ระดับผลการประเมิน</th>
            <th style="text-align:center" width="50" rowspan="3">Gap</th>
            <th style="text-align:center" width="150" rowspan="3">ประเด็นพัฒนา</th>
            <th style="text-align:center" width="150" colspan="7">วิธีการพัฒนา</th>
        </tr>
        <tr>            
            <th style="text-align:center" width="90" colspan="3">พัฒนาด้วย<br>ตนเอง</th>
            <th style="text-align:center" width="90" colspan="3">พัฒนาด้วย<br>ผู้บังคับบัญชา</th>
            <th style="text-align:center" width="30">อบรม</th>
        </tr>
        <tr>            
            <th style="text-align:center" width="30">S</th>
            <th style="text-align:center" width="30">C</th>
            <th style="text-align:center" width="30">E</th>
            <th style="text-align:center" width="30">OJT</th>
            <th style="text-align:center" width="30">A</th>
            <th style="text-align:center" width="30">J</th>
            <th style="text-align:center" width="30">W</th>
        </tr>

        <tr>
            <td colspan="12"><font size=3> <u><b>ความรู้</b></u></font></td>
        </tr>
        <tr>
            <td>ความรู้ที่จำเป็นในการปฏิบัติงานตามตำแหน่ง</td>
            <td style="text-align:center">
                <?= $model->k1_std_score ?>
            </td>
            <td style="text-align:center"> 
                <?= $model->k1_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->k1_gap ?>
            </td>
            <td>
                <?= $model->k1_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->k1_self == 'S' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->k1_self == 'C' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->k1_self == 'E' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->k1_boss == 'O' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->k1_boss == 'A' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->k1_boss == 'J' ? "/":'' ?>
            </td>
            <td>
                <?= $model->k1_workshop ? "/":'' ?>
            </td>
        </tr>
        <tr>
            <td>ความรู้เกี่ยวกับกฎหมายและระเบียบราชการ</td>
            <td style="text-align:center">
                <?= $model->k2_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $model->k2_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->k2_gap ?>
            </td>
            <td>
                <?= $model->k2_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->k2_self == 'S' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->k2_self == 'C' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->k2_self == 'E' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->k2_boss == 'O' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->k2_boss == 'A' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->k2_boss == 'J' ? "/":'' ?>
            </td>
            <td>
                <?= $model->k2_workshop ? "/":'' ?>
            </td>
        </tr>
        <tr>
        <td colspan="12"><font size=3> <u><b>ทักษะ</b></u></font></td>
        </tr>
        <tr>
            <td>การใช้คอมพิวเตอร์</td>
            <td style="text-align:center">
                <?= $model->com_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $model->com_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->com_gap ?>
            </td>
            <td>
                <?= $model->com_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->com_self == 'S' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->com_self == 'C' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->com_self == 'E' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->com_boss == 'O' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->com_boss == 'A' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->com_boss == 'J' ? "/":'' ?>
            </td>
            <td>
                <?= $model->com_workshop ? "/":'' ?>
            </td>
        <tr>
            <td>การใช้ภาษาอังกฤษ</td>
            <td style="text-align:center">
                <?= $model->eng_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $model->eng_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->eng_gap ?>
            </td>
            <td>
                <?= $model->eng_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->eng_self == 'S' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->eng_self == 'C' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->eng_self == 'E' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->eng_boss == 'O' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->eng_boss == 'A' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->eng_boss == 'J' ? "/":'' ?>
            </td>
            <td>
                <?= $model->eng_workshop ? "/":'' ?>
            </td>
        </tr>
        <tr>
            <td>การคำนวน</td>
            <td style="text-align:center">
                <?= $model->cal_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $model->cal_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->cal_gap ?>
            </td>
            <td>
                <?= $model->cal_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->cal_self == 'S' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->cal_self == 'C' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->cal_self == 'E' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->cal_boss == 'O' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->cal_boss == 'A' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->cal_boss == 'J' ? "/":'' ?>
            </td>
            <td>
                <?= $model->cal_workshop ? "/":'' ?>
            </td>
        </tr>
        <tr>
            <td>การจัดการข้อมูล</td>
            <td style="text-align:center">
                <?= $model->data_std_score ?>
            </td>
            <td style="text-align:center">
                <?= $model->data_eva_score ?>            
            </td>
            <td style="text-align:center">
                <?= $model->data_gap ?>
            </td>
            <td>
                <?= $model->data_subject ?>
            </td>
            <td style="text-align:center">
                <?= $model->data_self == 'S' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->data_self == 'C' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->data_self == 'E' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->data_boss == 'O' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->data_boss == 'A' ? "/":'' ?>
            </td>
            <td style="text-align:center">
                <?= $model->data_boss == 'J' ? "/":'' ?>
            </td>
            <td>
                <?= $model->data_workshop ? "/":'' ?>
            </td>
        </tr>
    </table>
    <p>
        <u>หมายเหตุ</u> GAP ได้ค่าคะแนนเต็ม (5 คะแนน) ลบระดับผลการประเมิน<br>
        <u>วิธีการพัฒนา</u> ประกอบด้วย S = Self-Directed Study(เรียนด้วยตนเอง), C = Countinuing (เข้ารับการศึกษาต่อเนื่อง),
        E = E-Learning (เรียนรู้ผ่านช่องทางอิเล็กทรอนิกส์), OTJ = On Job Training (สอนงานในการปฏิบัติจริง), A = Assignment (มอบหมายงาน/โครงการ),
        J = Job Rotation (การหมุนเวียนงาน), W = Workshop, Class, Seminar (ฝึกอบรม, สัมมนา)
    </p>