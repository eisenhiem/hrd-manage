<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Eva */

$this->title = 'Update Eva: ' . $model->eva_id;
$this->params['breadcrumbs'][] = ['label' => 'Evas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->eva_id, 'url' => ['view', 'id' => $model->eva_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="eva-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
