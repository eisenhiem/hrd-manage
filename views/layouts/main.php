<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'บุคคลากร', 'url' => ['/employees/index']],
            ['label' => 'ประเมินขั้นเงินเดือน', 'visible' => !Yii::$app->user->isGuest ,'url' => ['/employees/evaluate','id' => Yii::$app->user->identity->id]],
            ['label' => 'รายงาน', 'items' => [
                ['label' => 'รายการวันลา', 'url' => ['/vacation/index']],
                ['label' => 'รายงานสรุปวันลา', 'url' => ['/vacation/report']],
                ['label' => 'รายงานการเข้าออก', 'url' => ['/check/index']],
            ]],
            ['label' => 'ตั้งค่า', 'visible' => Yii::$app->user->identity->username == 'admin', 'items' => [
                ['label' => 'ตั้งค่ากลุ่มงาน', 'url' => ['/departments/index']],
                ['label' => 'ตั้งค่ารอบประเมิน', 'url' => ['/rounds/index']],
                ['label' => 'ตั้งค่าระดับ', 'url' => ['/levels/index']],
                ['label' => 'ตั้งค่าประเภท', 'url' => ['/types/index']],
                ['label' => 'ตั้งค่าความรู้/ทักษะ', 'url' => ['/knowledge/index']],
                ['label' => 'ตั้งค่าสมรถนะ', 'url' => ['/core/index']],
                ['label' => 'จัดการผู้ใช้', 'url' => ['/user/admin/index']],
            ] ],
            Yii::$app->user->isGuest ?
            ['label' => 'Sign in', 'url' => ['/user/security/login']] :
            ['label' => 'Account(' . Yii::$app->user->identity->username . ')', 'items'=>[
                ['label' => 'Account', 'url' => ['/user/settings/account']],
                ['label' => 'Logout', 'url' => ['/user/security/logout'],'linkOptions' => ['data-method' => 'post']],
            ]],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
