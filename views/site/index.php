<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

<div class="body-content">

<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">บุคลากร</div>
            <div class="panel-body">
                <?= Html::img('images/personal-icon.png',['width' => '200px']);?>
                </br>
                <?= Html::a('บันทึกข้อมูลบุคลากร', ['employees/index'], ['class' => 'btn btn-info','style' =>['width'=>'200px']]) ?>
            </div>
        </div>
    </div>
    <?php if(!Yii::$app->user->isGuest){ ?>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">ประเมินผลการปฏิบัติงาน</div>
            <div class="panel-body">
                <?= Html::img('images/evaluate.png',['width' => '200px']);?>
                </br>
                <?= Html::a('บันทึกผลการประเมิน', ['/employees/evaluate','id' => Yii::$app->user->identity->id], ['class' => 'btn btn-success','style' =>['width'=>'200px']]) ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-warning">
            <div class="panel-heading">รายงานการบันทึกลงเวลางาน</div>
            <div class="panel-body">
                <?= Html::img('images/finger-scan.png',['width' => '200px']);?>
                </br>
                <?= Html::a('รายงานการลงเวลาทำงาน', ['check/index'], ['class' => 'btn btn-warning','style' =>['width'=>'200px']]) ?>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-warning">
            <div class="panel-heading">ส่งออกรายงานบันทึกลงเวลา</div>
            <div class="panel-body">
                <?= Html::img('images/finger-scan.png',['width' => '200px']);?>
                </br>
                <?= Html::a('พิมพ์ข้อมูลบันทึกลงเวลา', ['check/report'], ['class' => 'btn btn-warning','style' =>['width'=>'200px']]) ?>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-warning">
            <div class="panel-heading">รายงานบันทึกลงเวลารายเดือน</div>
            <div class="panel-body">
                <?= Html::img('images/finger-scan.png',['width' => '200px']);?>
                </br>
                <?= Html::a('รายงานข้อมูลบันทึกลงเวลา', ['check/reportall'], ['class' => 'btn btn-warning','style' =>['width'=>'200px']]) ?>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">รายงานวันลา</div>
            <div class="panel-body">
                <?= Html::img('images/la.png',['width' => '200px']);?>
                </br>
                <?= Yii::$app->user->identity->id == 1 ? Html::a('รายงานข้อมูลวันลา', ['employees/vacation'], ['class' => 'btn btn-success','style' =>['width'=>'200px']]):Html::a('รายงานข้อมูลวันลา', ['vacation/index'], ['class' => 'btn btn-success','style' =>['width'=>'200px']]) ?>
            </div>
        </div>
    </div>
    <?php if(Yii::$app->user->identity->id == 1) { ?>  
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">ทำเงินเดือน</div>
            <div class="panel-body">
                <?= Html::img('images/salary.png',['width' => '200px']);?>
                </br>
                <?= Html::a('จัดการเงินเดือน', ['base-salary/index'], ['class' => 'btn btn-success','style' =>['width'=>'200px']]) ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-danger">
            <div class="panel-heading">รายงาน</div>
            <div class="panel-body">
                <?= Html::img('images/report.png',['width' => '200px']);?>
                </br>
                <?= Html::a('รายงานการประเมิน', ['sum/index'], ['class' => 'btn btn-danger','style' =>['width'=>'200px']]) ?>
            </div>
        </div>
    </div>
</div>

</div>
</div>
