<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Rounds;

/* @var $this yii\web\View */
/* @var $model app\models\Plan */
/* @var $form yii\widgets\ActiveForm */

$r = ArrayHelper::map(Rounds::find()->where(['is_active'=>'1'])->all(), 'r_id', 'r_detail');

?>

<div class="plan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'r_id')->dropDownList($r,['prompt'=>$r[0]]) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'plan_development')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'deverlop_by')->textInput(['maxlength' => true]) ?>
            </div>
        <div class="col-md-3">
            <?= $form->field($model, 'duration')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
