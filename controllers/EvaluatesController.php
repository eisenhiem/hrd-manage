<?php

namespace app\controllers;

use Yii;
use app\models\Evaluates;
use app\models\EvaluatesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Exception;

use app\models\Knowledge;

/**
 * EvaluatesController implements the CRUD actions for Evaluates model.
 */
class EvaluatesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Evaluates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EvaluatesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Evaluates model.
     * @param integer $emp_id
     * @param integer $r_id
     * @param integer $k_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($emp_id, $r_id, $k_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($emp_id, $r_id, $k_id),
        ]);
    }

    /**
     * Creates a new Evaluates model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Evaluates();
        $model->emp_id = $id;
        $k = Knowledge::find()->all();
           
        if($model->load(Yii::$app->request->post()))
        {
            
            $transaction = Yii::$app->db->beginTransaction();
            
            try {
                // $model->save(); //บันทึกใบแบบประเมิน

                $items = Yii::$app->request->post();
                
                //var_dump($items['Order']['items']);
                
                foreach($items['emp_id']['k_id'] as $key => $val){ //นำรายการประเมินที่เลือกมา loop บันทึก
                    if(empty($val['emp_id'] && $val['r_id'] && $val['k_id'])){
                        $detail = new Evaluates();
                    }else{
                        $detail = Evaluates::findOne(['emp_id' => $val['emp_id'],'r_id' => $val['r_id'],'k_id'=>$val['k_id']]);
                    }
                    $detail->emp_id = $id;
                    $detail->r_id = $model->r_id;
                    $detail->k_id = $val['k_id'];
                    $detail->standard_score = $val['standard_score'];
                    $detail->evaluate_score = $val['evaluate_score'];
                    $detail->subject_training = $val['subject_training'];
                    $detail->by_self = $val['by_self'];
                    $detail->by_boss = $val['by_boss'];
                    $detail->workshop = $val['workshop'];

                    $detail->save();
                    
                }

                $transaction->commit();
                Yii::$app->session->setFlash('success', 'บันทึกข้อมูลเรียบร้อย');
                return $this->redirect(['index']);
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'มีข้อผิดพลาดในการบันทึก');
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Evaluates model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $emp_id
     * @param integer $r_id
     * @param integer $k_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($emp_id, $r_id, $k_id)
    {
        $model = $this->findModel($emp_id, $r_id, $k_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'emp_id' => $model->emp_id, 'r_id' => $model->r_id, 'k_id' => $model->k_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Evaluates model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $emp_id
     * @param integer $r_id
     * @param integer $k_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($emp_id, $r_id, $k_id)
    {
        $this->findModel($emp_id, $r_id, $k_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Evaluates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $emp_id
     * @param integer $r_id
     * @param integer $k_id
     * @return Evaluates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($emp_id, $r_id, $k_id)
    {
        if (($model = Evaluates::findOne(['emp_id' => $emp_id, 'r_id' => $r_id, 'k_id' => $k_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
