<?php

namespace app\controllers;

use Yii;
use app\models\SumEva;
use app\models\SumEvaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SumController implements the CRUD actions for SumEva model.
 */
class SumController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SumEva models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SumEvaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SumEva model.
     * @param integer $r_id
     * @param integer $emp_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($r_id, $emp_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($r_id, $emp_id),
        ]);
    }

    /**
     * Creates a new SumEva model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SumEva();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'r_id' => $model->r_id, 'emp_id' => $model->emp_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SumEva model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $r_id
     * @param integer $emp_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($r_id, $emp_id)
    {
        $model = $this->findModel($r_id, $emp_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'r_id' => $model->r_id, 'emp_id' => $model->emp_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SumEva model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $r_id
     * @param integer $emp_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($r_id, $emp_id)
    {
        $this->findModel($r_id, $emp_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SumEva model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $r_id
     * @param integer $emp_id
     * @return SumEva the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($r_id, $emp_id)
    {
        if (($model = SumEva::findOne(['r_id' => $r_id, 'emp_id' => $emp_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
