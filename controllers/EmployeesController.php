<?php

namespace app\controllers;

use Yii;

use app\models\Employees;
use app\models\EmployeesSearch;
use yii\debug\models\timeline\DataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

use app\models\User;
use app\models\KpiSearch;
use app\models\CompetencySearch;
use app\models\PlanSearch;
use app\models\EmpVacationSearch;
use app\models\Plan;
use app\models\Kpi;
use app\models\Competency;
use app\models\Rounds;

/**
 * EmployeesController implements the CRUD actions for Employees model.
 */
class EmployeesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Employees models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Employees models.
     * @return mixed
     */
    public function actionVacation()
    {
        $searchModel = new EmployeesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('vacation', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Employees models for evaluate.
     * @return mixed
     */
    public function actionEvaluate($id)
    {
        $model = User::findOne($id);
        $searchModel = new EmployeesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if($model->dep_id){
            $dataProvider->query->andWhere(['dep_id' => $model->dep_id]);
        } else {
            $dataProvider->query->andWhere(['emp_id' => $model->emp_id]);
        }
        return $this->render('evaluate', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employees model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single Employees model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDetail($id)
    {
        return $this->render('view_eva', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Displays a single Employees model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionKpi($emp_id,$r_id)
    {
        $model = $this->findModel($emp_id); 
        $searchModel = new KpiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['emp_id' => $emp_id,'r_id'=>$r_id]) ;
        return $this->render('kpi', [
            'model' => $model,
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employees model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionComp($emp_id,$r_id)
    {
        $model = $this->findModel($emp_id); 
        $searchModel = new CompetencySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['emp_id' => $emp_id,'r_id'=>$r_id]) ;
        return $this->render('comp', [
            'model' => $model,
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employees model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPlan($emp_id,$r_id)
    {
        $model = $this->findModel($emp_id); 
        $searchModel = new PlanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['emp_id' => $emp_id,'r_id'=>$r_id]) ;
        return $this->render('plan', [
            'model' => $model,
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionPrint($emp_id,$r_id) {
        $model = $this->findModel($emp_id);
        $plan = Plan::find()->where(['emp_id' => $emp_id,'r_id' => $r_id])->all();
        $kpi = Kpi::find()->where(['emp_id' => $emp_id,'r_id' => $r_id])->all();
        $comp = Competency::find()->where(['emp_id' => $emp_id,'r_id' => $r_id])->all();
        $round = Rounds::findOne($r_id);
        $eva = User::findOne($kpi[0]->evaluate_by);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('_print',[
            'model' => $model,
            'plan' => $plan,
            'kpi' => $kpi,
            'comp' => $comp,
            'round' => $round,
            'eva' => $eva,
        ]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'แบบสรุปการประเมินผลการปฏิบัติราชการ'],
             // call mPDF methods on the fly
            'methods' => [ 
                //'SetHeader'=>['แบบฟอร์มที่ 1/1'], 
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }
    
    /**
     * Creates a new Employees model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Employees();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Employees model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Employees model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Employees model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employees the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employees::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
