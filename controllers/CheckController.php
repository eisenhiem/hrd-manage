<?php

namespace app\controllers;

use Yii;
use app\models\Check;
use app\models\CheckSearch;
use app\models\RepCheckAllSearch;
use app\models\RepCheckAll;
use app\models\Departments;
use app\models\Employees;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

/**
 * CheckController implements the CRUD actions for Check model.
 */
class CheckController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Check models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CheckSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

        /**
     * Lists all Check models.
     * @return mixed
     */
    public function actionReport()
    {
        $searchModel = new CheckSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->groupBy(['dep_id','REGIST_DATE']);

        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Check models.
     * @return mixed
     */
    public function actionPrint($dep,$date)
    {
        $months = ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $thyear = substr($date,0,4)+543;
        $thmonth = $months[substr($date,5,2)-1];
        $thday = intVal(substr($date,8,2));
        $workdate = $thday.' '.$thmonth.' พ.ศ. '.$thyear; 

        $model = Check::find()->where(['REGIST_DATE' => $date,'dep_id'=>$dep])->orderBy(['PCODE'=>SORT_ASC,'REGIST_TIME'=>SORT_ASC])->all();

        $department = Departments::findOne($dep);
        $content = $this->renderPartial('_print',[
            'model' => $model,
            'department' => $department,
            'workdate' => $workdate,
        ]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'แบบสรุปวันทำการ โรงพยาบาลเหล่าเสือโก้ก'],
             // call mPDF methods on the fly
            'methods' => [ 
                //'SetHeader'=>['แบบฟอร์มที่ 1/1'], 
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render(); 

    }

    public function actionReportall()
    {
        $searchModel = new RepCheckAllSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->groupBy(['finger_id']);

        return $this->render('reportall', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

        /**
     * Lists all Check models.
     * @return mixed
     */
    public function actionPrintrep($id,$date)
    {
        $months = ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $thyear = substr($date,0,4)+543;
        $thmonth = $months[substr($date,5,2)-1];
        $workdate = $thmonth.' พ.ศ. '.$thyear; 
        $month = substr($date,0,7);
        $model = RepCheckAll::find()->where(['like','reg_date',$month])->andWhere(['finger_id'=>$id])->orderBy(['reg_date'=>SORT_ASC,'reg_time'=>SORT_ASC])->all();
        $emp = Employees::find()->where(['finger_id'=>$id])->one();
        $content = $this->renderPartial('_print_check',[
            'model' => $model,
            'workdate' => $workdate,
            'emp' => $emp,
        ]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'แบบสรุปวันทำการ โรงพยาบาลเหล่าเสือโก้ก'],
             // call mPDF methods on the fly
            'methods' => [ 
                //'SetHeader'=>['แบบฟอร์มที่ 1/1'], 
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render(); 

    }

    /**
     * Displays a single Check model.
     * @param string $PCODE
     * @param string $REGIST_DATE
     * @param string $REGIST_TIME
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($PCODE, $REGIST_DATE, $REGIST_TIME)
    {
        return $this->render('view', [
            'model' => $this->findModel($PCODE, $REGIST_DATE, $REGIST_TIME),
        ]);
    }

    /**
     * Creates a new Check model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Check();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PCODE' => $model->PCODE, 'REGIST_DATE' => $model->REGIST_DATE, 'REGIST_TIME' => $model->REGIST_TIME]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Check model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $PCODE
     * @param string $REGIST_DATE
     * @param string $REGIST_TIME
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($PCODE, $REGIST_DATE, $REGIST_TIME)
    {
        $model = $this->findModel($PCODE, $REGIST_DATE, $REGIST_TIME);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PCODE' => $model->PCODE, 'REGIST_DATE' => $model->REGIST_DATE, 'REGIST_TIME' => $model->REGIST_TIME]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Check model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $PCODE
     * @param string $REGIST_DATE
     * @param string $REGIST_TIME
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($PCODE, $REGIST_DATE, $REGIST_TIME)
    {
        $this->findModel($PCODE, $REGIST_DATE, $REGIST_TIME)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Check model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $PCODE
     * @param string $REGIST_DATE
     * @param string $REGIST_TIME
     * @return Check the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($PCODE, $REGIST_DATE, $REGIST_TIME)
    {
        if (($model = Check::findOne(['PCODE' => $PCODE, 'REGIST_DATE' => $REGIST_DATE, 'REGIST_TIME' => $REGIST_TIME])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
