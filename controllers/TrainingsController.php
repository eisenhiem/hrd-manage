<?php

namespace app\controllers;

use Yii;
use app\models\Trainings;
use app\models\TrainingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Employees;

use kartik\mpdf\Pdf;

/**
 * TrainingsController implements the CRUD actions for Trainings model.
 */
class TrainingsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trainings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Trainings model.
     * @param integer $emp_id
     * @param integer $r_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($emp_id, $r_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($emp_id, $r_id),
        ]);
    }

    /**
     * Creates a new Trainings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Trainings();
        $model->emp_id = $id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'emp_id' => $model->emp_id, 'r_id' => $model->r_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Trainings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $emp_id
     * @param integer $r_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($emp_id, $r_id)
    {
        $model = $this->findModel($emp_id, $r_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'emp_id' => $model->emp_id, 'r_id' => $model->r_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Trainings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $emp_id
     * @param integer $r_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($emp_id, $r_id)
    {
        $this->findModel($emp_id, $r_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Trainings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $emp_id
     * @param integer $r_id
     * @return Trainings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($emp_id, $r_id)
    {
        if (($model = Trainings::findOne(['emp_id' => $emp_id, 'r_id' => $r_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Displays a single Trainings model.
     * @param integer $emp_id
     * @param integer $r_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDetail($id)
    {
        $model = Trainings::find()->where(['emp_id'=>$id])->all();
        
        return $this->render('detail', [
            'model' => $model,
        ]);
    }

    public function actionPrint($emp_id,$r_id) {
        $model = $this->findModel($emp_id,$r_id);
        $employee = Employees::findOne($model->emp_id);

        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('_print',[
            'model' => $model,
            'employee' => $employee,
        ]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'แบบสำรวจความจำเป็นในการฝึกอบรมเพื่อพัฒนาความรู้'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['แบบฟอร์มที่ 1/3'], 
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

}
