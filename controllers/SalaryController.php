<?php

namespace app\controllers;

use app\models\Employees;
use app\models\EmpSalary;
use Yii;
use app\models\Salary;
use app\models\SalarySearch;
use kartik\mpdf\Pdf;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SalaryController implements the CRUD actions for Salary model.
 */
class SalaryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Salary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SalarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Salary model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Salary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Salary();
        $emp = EmpSalary::find()->where(['emp_id'=>$id])->one();

        $model->emp_id = $emp->emp_id;
        $model->salary_date = date('Y-m-d');
        $model->base_salary = $emp->base_salary;
        $model->extra_salary = $emp->extra_salary;
        $model->ot_salary = $emp->ot_salary;
        $model->vp_salary = $emp->vp_salary;
        $model->v11_salary = $emp->v11_salary;
        $model->pts_salary = $emp->pts_salary;
        $model->pts_extra_salary = $emp->pts_extra_salary;
        $model->other_salary = $emp->other_salary;
        $model->tax_pay = $emp->tax_pay;
        $model->kbk_pay = $emp->kbk_pay;
        $model->sso_pay = $emp->sso_pay;
        $model->om_pay = $emp->om_pay;
        $model->dt_pay = $emp->dt_pay;
        $model->elec_pay = $emp->elec_pay;
        $model->bank_pay = $emp->bank_pay;
        $model->insurance_pay = $emp->insurance_pay;
        $model->other_pay = $emp->other_pay;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/base-salary/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Salary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->salary_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Salary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Salary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Salary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Salary::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPrint($id) {
        
        $model = $this->findModel($id);
        $employee = Employees::findOne($model->emp_id);

        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('_print',[
            'model' => $model,
            'employee' => $employee,
        ]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Payment Slip'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['ใบรับรองการจ่ายเงินเดือนและเงินอื่น'], 
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }
}
