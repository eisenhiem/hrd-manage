/*
Navicat MySQL Data Transfer

Source Server         : Local_DB
Source Server Version : 50565
Source Host           : localhost:3306
Source Database       : hrd

Target Server Type    : MYSQL
Target Server Version : 50565
File Encoding         : 65001

Date: 2021-03-11 13:11:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `competency`
-- ----------------------------
DROP TABLE IF EXISTS `competency`;
CREATE TABLE `competency` (
`comp_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`r_id`  int(11) NOT NULL ,
`emp_id`  int(11) NOT NULL ,
`core_id`  tinyint(4) NOT NULL ,
`evaluate_by`  int(11) NOT NULL ,
`level`  tinyint(4) NULL DEFAULT NULL ,
`evaluate`  tinyint(4) NULL DEFAULT NULL ,
`weight`  tinyint(4) NULL DEFAULT NULL ,
`total`  decimal(11,2) NULL DEFAULT NULL ,
`comment`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
PRIMARY KEY (`comp_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of competency
-- ----------------------------
BEGIN;
INSERT INTO `competency` VALUES ('1', '1', '1', '1', '1', '2', '5', '25', '1.25', '-'), ('2', '1', '1', '2', '1', '4', '4', '25', '1.00', '');
COMMIT;

-- ----------------------------
-- Table structure for `core`
-- ----------------------------
DROP TABLE IF EXISTS `core`;
CREATE TABLE `core` (
`core_id`  int(11) NOT NULL DEFAULT 0 ,
`core_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`core_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of core
-- ----------------------------
BEGIN;
INSERT INTO `core` VALUES ('1', 'การมุ่งผลสัมฤทธิ์'), ('2', 'การบริการที่ดี'), ('3', 'การสั่งสมความเชี่ยวชาญในงานอาชีพ'), ('4', 'การยึดมั่นในความถูกต้องชอบธรรมและจริยธรรม'), ('5', 'การทำงานเป็นทีม'), ('6', 'อื่นๆ');
COMMIT;

-- ----------------------------
-- Table structure for `core_competency`
-- ----------------------------
DROP TABLE IF EXISTS `core_competency`;
CREATE TABLE `core_competency` (
`r_id`  int(11) NOT NULL ,
`emp_id`  int(11) NOT NULL ,
`c1_score`  int(11) NULL DEFAULT NULL ,
`c2_score`  int(11) NULL DEFAULT NULL ,
`c3_score`  int(11) NULL DEFAULT NULL ,
`c4_score`  int(11) NULL DEFAULT NULL ,
`c5_score`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`r_id`, `emp_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of core_competency
-- ----------------------------
BEGIN;
INSERT INTO `core_competency` VALUES ('1', '1', '2', '3', '4', '2', '3');
COMMIT;

-- ----------------------------
-- Table structure for `departments`
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
`dep_id`  int(2) NOT NULL AUTO_INCREMENT ,
`dep_name`  varchar(255) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL ,
PRIMARY KEY (`dep_id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=11

;

-- ----------------------------
-- Records of departments
-- ----------------------------
BEGIN;
INSERT INTO `departments` VALUES ('1', 'กลุ่มงานการพยาบาล'), ('2', 'กลุ่มงานบริหารทั่วไป'), ('3', 'กลุ่มงานทันตกรรม'), ('4', 'องค์กรแพทย์'), ('5', 'กลุ่มงานประกันสุขภาพ ยุทธศาสตร์ฯ'), ('6', 'กลุ่มงานเทคนิคการแพทย์'), ('7', 'กลุ่มงานบริการด้านปฐมภูมิและองค์รวม'), ('8', 'กลุ่มงานเภสัชกรรมและคุ้มครองผู้บริโภค'), ('9', 'กลุ่มงานฟื้นฟูสมรรถภาพ'), ('10', 'กลุ่มงานแพทย์แผนไทยและแพทย์ทางเลือก');
COMMIT;

-- ----------------------------
-- Table structure for `employees`
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
`emp_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`pname`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`fname`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`lname`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`position`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`level`  int(11) NOT NULL ,
`position_type`  int(11) NOT NULL ,
`birthdate`  date NOT NULL ,
`start_date`  date NOT NULL ,
`education`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`branch`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`dep_id`  int(11) NOT NULL ,
`job1`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`job2`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`job3`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`job4`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`job5`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`job6`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`emp_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of employees
-- ----------------------------
BEGIN;
INSERT INTO `employees` VALUES ('1', 'นาย', 'จักรพงษ์', 'วงศ์กมลาไสย', 'นักวิชาการคอมพิวเตอร์', '6', '2', '1979-11-22', '2016-09-02', 'ปริญญาวิทยาศาสตร์บัณฑิต', 'เทคโนโลยีสารสนเทศ', '5', 'ดูแล รักษา อุปกรณ์คอมพิวเตอร์ และเครือข่ายสัญญาณอินเตอร์เน็ต', 'ส่งออกข้อมูล 43 แฟ้ม ไปยังส่วนกลาง', 'จัดทำรายงานข้อมูลจากระบบ HIS', 'จัดทำระบบสนับสนุนการทำงานตามที่ได้รับมอบหมาย', 'งานพัฒนาคุณภาพของโรงพยาบาล', 'งานพัฒนาระบบเวชระเบียน');
COMMIT;

-- ----------------------------
-- Table structure for `eva`
-- ----------------------------
DROP TABLE IF EXISTS `eva`;
CREATE TABLE `eva` (
`eva_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`emp_id`  int(11) NOT NULL ,
`r_id`  int(11) NOT NULL ,
`k1_std_score`  int(11) NOT NULL ,
`k1_eva_score`  int(11) NOT NULL ,
`k1_gap`  int(11) NOT NULL ,
`k1_subject`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`k1_self`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`k1_boss`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`k1_workshop`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`k2_std_score`  int(11) NOT NULL ,
`k2_eva_score`  int(11) NOT NULL ,
`k2_gap`  int(11) NOT NULL ,
`k2_subject`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`k2_self`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`k2_boss`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`k2_workshop`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`com_std_score`  int(11) NOT NULL ,
`com_eva_score`  int(11) NOT NULL ,
`com_gap`  int(11) NOT NULL ,
`com_subject`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`com_self`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`com_boss`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`com_workshop`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`eng_std_score`  int(11) NOT NULL ,
`eng_eva_score`  int(11) NOT NULL ,
`eng_gap`  int(11) NOT NULL ,
`eng_subject`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`eng_self`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`eng_boss`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`eng_workshop`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`cal_std_score`  int(11) NOT NULL ,
`cal_eva_score`  int(11) NOT NULL ,
`cal_gap`  int(11) NOT NULL ,
`cal_subject`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`cal_self`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`cal_boss`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`cal_workshop`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`data_std_score`  int(11) NOT NULL ,
`data_eva_score`  int(11) NOT NULL ,
`data_gap`  int(11) NOT NULL ,
`data_subject`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`data_self`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`data_boss`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`data_workshop`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`eva_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of eva
-- ----------------------------
BEGIN;
INSERT INTO `eva` VALUES ('1', '1', '1', '1', '3', '-2', '', '', '', '', '2', '2', '0', '', '', '', '', '2', '3', '-1', '', '', '', '', '2', '2', '0', '', '', '', '', '2', '3', '-1', '', '', '', '', '2', '3', '-1', '', '', '', '');
COMMIT;

-- ----------------------------
-- Table structure for `evaluates`
-- ----------------------------
DROP TABLE IF EXISTS `evaluates`;
CREATE TABLE `evaluates` (
`emp_id`  int(11) NOT NULL DEFAULT 0 ,
`r_id`  int(11) NOT NULL DEFAULT 0 ,
`k_id`  int(11) NOT NULL DEFAULT 0 ,
`standard_score`  int(11) NULL DEFAULT NULL ,
`evaluate_score`  int(11) NULL DEFAULT NULL ,
`subject_training`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`by_self`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`by_boss`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`workshop`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`emp_id`, `r_id`, `k_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of evaluates
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `jobs`
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
`job_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`emp_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
`job_detail`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`job_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=16

;

-- ----------------------------
-- Records of jobs
-- ----------------------------
BEGIN;
INSERT INTO `jobs` VALUES ('11', '1', null), ('12', '1', null), ('13', '1', null), ('14', '1', null), ('15', '3', null);
COMMIT;

-- ----------------------------
-- Table structure for `knowledge`
-- ----------------------------
DROP TABLE IF EXISTS `knowledge`;
CREATE TABLE `knowledge` (
`k_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`k_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`k_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=7

;

-- ----------------------------
-- Records of knowledge
-- ----------------------------
BEGIN;
INSERT INTO `knowledge` VALUES ('1', 'ความรู้ที่จำเป็นในการปฏิบัติงานตามตำแหน่ง'), ('2', 'ความรู้เรื่องกฎหมายและระเบียบราชการ'), ('3', 'ทักษะการใช้คอมพิวเตอร์'), ('4', 'ทักษะการใช้ภาษาอังกฤษ'), ('5', 'ทักษะการคำนวน'), ('6', 'ทักษะการจัดการข้อมูล');
COMMIT;

-- ----------------------------
-- Table structure for `kpi`
-- ----------------------------
DROP TABLE IF EXISTS `kpi`;
CREATE TABLE `kpi` (
`kpi_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`r_id`  int(11) NULL DEFAULT NULL ,
`emp_id`  int(11) NOT NULL ,
`kpi_no`  enum('5','4','3','2','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`kpi_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`kpi_template`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`kpi_score`  tinyint(4) NOT NULL ,
`kpi_weight`  tinyint(4) NOT NULL ,
`kpi_1`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
`kpi_2`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
`kpi_3`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
`kpi_4`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
`kpi_5`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`kpi_total`  decimal(11,2) NULL DEFAULT NULL ,
`kpi_active`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`evaluate_by`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`kpi_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=5

;

-- ----------------------------
-- Records of kpi
-- ----------------------------
BEGIN;
INSERT INTO `kpi` VALUES ('1', '1', '1', '1', 'ทดสอบเพิ่ม', 'qwertyuiop[', '3', '20', '50', '60', '70', '80', '90', '0.60', null, '1'), ('2', '1', '1', '2', 'ทดสอบเพิ่มข้อ 2', 'what the fuck!!!!', '4', '20', '5', '4', '3', '2', '1', '0.80', null, '1'), ('3', '1', '1', '3', 'ทดสอบเพิ่มข้อ 3', '!@#$%^&*(O)P', '3', '20', '55', '60', '65', '70', '80', '0.60', null, '1'), ('4', '1', '1', '4', 'ทดสอบเพิ่มข้อ 4', 'aesyrdutfgohiujoimk,l', '5', '30', '<30', '31-40', '41-50', '51-60', '>60', '1.50', null, '1');
COMMIT;

-- ----------------------------
-- Table structure for `levels`
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels` (
`level`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`level_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`level`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=13

;

-- ----------------------------
-- Records of levels
-- ----------------------------
BEGIN;
INSERT INTO `levels` VALUES ('1', '-'), ('2', 'ปฏิบัติงาน'), ('3', 'ชำนาญงาน'), ('4', 'อาวุโส'), ('5', 'ทักษะพิเศษ'), ('6', 'ปฏิบัติการ'), ('7', 'ชำนาญการ'), ('8', 'ชำนาญการพิเศษ'), ('9', 'เชี่ยวชาญ'), ('10', 'ทรงคุณวุฒิ'), ('11', 'ต้น'), ('12', 'สูง');
COMMIT;

-- ----------------------------
-- Table structure for `migration`
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
`version`  varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`apply_time`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`version`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of migration
-- ----------------------------
BEGIN;
INSERT INTO `migration` VALUES ('m000000_000000_base', '1615301045'), ('m140209_132017_init', '1615301053'), ('m140403_174025_create_account_table', '1615301054'), ('m140504_113157_update_tables', '1615301055'), ('m140504_130429_create_token_table', '1615301055'), ('m140830_171933_fix_ip_field', '1615301055'), ('m140830_172703_change_account_table_name', '1615301056'), ('m141222_110026_update_ip_field', '1615301056'), ('m141222_135246_alter_username_length', '1615301056'), ('m150614_103145_update_social_account_table', '1615301057'), ('m150623_212711_fix_username_notnull', '1615301057'), ('m151218_234654_add_timezone_to_profile', '1615301057'), ('m160929_103127_add_last_login_at_to_user_table', '1615301057');
COMMIT;

-- ----------------------------
-- Table structure for `plan`
-- ----------------------------
DROP TABLE IF EXISTS `plan`;
CREATE TABLE `plan` (
`plan_id`  int(11) NOT NULL AUTO_INCREMENT ,
`emp_id`  int(11) NOT NULL ,
`r_id`  int(11) NOT NULL ,
`plan_development`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`deverlop_by`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`duration`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`plan_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of plan
-- ----------------------------
BEGIN;
INSERT INTO `plan` VALUES ('1', '1', '1', 'การทำงานเป็นทีม', 'การมอบหมายงาน', 'ไตรมาส 3');
COMMIT;

-- ----------------------------
-- Table structure for `profile`
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
`user_id`  int(11) NOT NULL ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`public_email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`gravatar_email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`gravatar_id`  varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`location`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`website`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`bio`  text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
`timezone`  varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
PRIMARY KEY (`user_id`),
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci

;

-- ----------------------------
-- Records of profile
-- ----------------------------
BEGIN;
INSERT INTO `profile` VALUES ('1', null, null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
-- Table structure for `rounds`
-- ----------------------------
DROP TABLE IF EXISTS `rounds`;
CREATE TABLE `rounds` (
`r_id`  int(11) NOT NULL AUTO_INCREMENT ,
`r_yearbudget`  int(11) NULL DEFAULT NULL ,
`r_round`  int(11) NULL DEFAULT NULL ,
`r_detail`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`is_active`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' ,
PRIMARY KEY (`r_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of rounds
-- ----------------------------
BEGIN;
INSERT INTO `rounds` VALUES ('1', '2564', '1', 'ประเมินรอบที่ 1 (ตุลาคม 2563 - มีนาคม 2564)', '1'), ('2', '2564', '2', 'ประเมินรอบที่ 2 (เมษายน 2564 - กันยายน 2564)', '0');
COMMIT;

-- ----------------------------
-- Table structure for `social_account`
-- ----------------------------
DROP TABLE IF EXISTS `social_account`;
CREATE TABLE `social_account` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`user_id`  int(11) NULL DEFAULT NULL ,
`provider`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`client_id`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`data`  text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
`code`  varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`created_at`  int(11) NULL DEFAULT NULL ,
`email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`username`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
UNIQUE INDEX `account_unique` (`provider`, `client_id`) USING BTREE ,
UNIQUE INDEX `account_unique_code` (`code`) USING BTREE ,
INDEX `fk_user_account` (`user_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of social_account
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `token`
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
`user_id`  int(11) NOT NULL ,
`code`  varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`created_at`  int(11) NOT NULL ,
`type`  smallint(6) NOT NULL ,
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
UNIQUE INDEX `token_unique` (`user_id`, `code`, `type`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci

;

-- ----------------------------
-- Records of token
-- ----------------------------
BEGIN;
INSERT INTO `token` VALUES ('1', 'cq_UQQw_G9ssXm_SSPgPhdv8sXJXpB9b', '1615301772', '0');
COMMIT;

-- ----------------------------
-- Table structure for `trainings`
-- ----------------------------
DROP TABLE IF EXISTS `trainings`;
CREATE TABLE `trainings` (
`emp_id`  int(11) NOT NULL DEFAULT 0 ,
`r_id`  int(11) NOT NULL DEFAULT 0 ,
`achievment`  int(11) NULL DEFAULT NULL ,
`service_mind`  int(11) NULL DEFAULT NULL ,
`expertise`  int(11) NULL DEFAULT NULL ,
`integrity`  int(11) NULL DEFAULT NULL ,
`teamwork`  int(11) NULL DEFAULT NULL ,
`reading`  int(11) NULL DEFAULT NULL ,
`reading_level`  int(11) NULL DEFAULT NULL ,
`listening`  int(11) NULL DEFAULT NULL ,
`listening_level`  int(11) NULL DEFAULT NULL ,
`speaking`  int(11) NULL DEFAULT NULL ,
`speaking_level`  int(11) NULL DEFAULT NULL ,
`writing`  int(11) NULL DEFAULT NULL ,
`wrinting_level`  int(11) NULL DEFAULT NULL ,
`ms_word`  int(11) NULL DEFAULT NULL ,
`ms_word_level`  int(11) NULL DEFAULT NULL ,
`ms_excel`  int(11) NULL DEFAULT NULL ,
`ms_excel_level`  int(11) NULL DEFAULT NULL ,
`ms_powerpoint`  int(11) NULL DEFAULT NULL ,
`ms_powerpoint_level`  int(11) NULL DEFAULT NULL ,
`e_mail`  int(11) NULL DEFAULT NULL ,
`e_mail_level`  int(11) NULL DEFAULT NULL ,
`other1`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`other1_level`  int(11) NULL DEFAULT NULL ,
`laws`  int(11) NULL DEFAULT NULL ,
`rm`  int(11) NULL DEFAULT NULL ,
`sm`  int(11) NULL DEFAULT NULL ,
`cm`  int(11) NULL DEFAULT NULL ,
`pmqa`  int(11) NULL DEFAULT NULL ,
`km`  int(11) NULL DEFAULT NULL ,
`bs`  int(11) NULL DEFAULT NULL ,
`sp`  int(11) NULL DEFAULT NULL ,
`hr`  int(11) NULL DEFAULT NULL ,
`other2`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`ms_access`  int(11) NULL DEFAULT NULL ,
`ms_access_level`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`emp_id`, `r_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of trainings
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `types`
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types` (
`position_type`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`position_type_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`position_type`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=5

;

-- ----------------------------
-- Records of types
-- ----------------------------
BEGIN;
INSERT INTO `types` VALUES ('1', 'ทั่วไป'), ('2', 'วิชาการ'), ('3', 'อำนวยการ'), ('4', 'บริหาร');
COMMIT;

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`username`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`password_hash`  varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`auth_key`  varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`confirmed_at`  int(11) NULL DEFAULT NULL ,
`unconfirmed_email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`blocked_at`  int(11) NULL DEFAULT NULL ,
`registration_ip`  varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`created_at`  int(11) NOT NULL ,
`updated_at`  int(11) NOT NULL ,
`flags`  int(11) NOT NULL DEFAULT 0 ,
`last_login_at`  int(11) NULL DEFAULT NULL ,
`dep_id`  int(2) NULL DEFAULT NULL ,
`emp_id`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
UNIQUE INDEX `user_unique_username` (`username`) USING BTREE ,
UNIQUE INDEX `user_unique_email` (`email`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', 'admin', 'lsk.hospital@gmail.com', '$2y$12$dI98GVzJ5Zyv1EJDYRcAeeuH0hlYklXZI7KiNrPggLe7bWo5zdue6', 'bylfadG2nkgPJcywRIF3fCjTuKhvNPKU', null, null, null, '::1', '1615301772', '1615301772', '0', '1615380777', '5', '1');
COMMIT;

-- ----------------------------
-- View structure for `sum_eva`
-- ----------------------------
DROP VIEW IF EXISTS `sum_eva`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sum_eva` AS select `e`.`r_id` AS `r_id`,`p`.`dep_id` AS `dep_id`,`p`.`emp_id` AS `emp_id`,`e`.`k1_gap` AS `k1_gap`,`e`.`k2_gap` AS `k2_gap`,`e`.`com_gap` AS `com_gap`,`e`.`eng_gap` AS `eng_gap`,`e`.`cal_gap` AS `cal_gap`,`e`.`data_gap` AS `data_gap`,`c`.`c1_score` AS `c1_score`,`c`.`c2_score` AS `c2_score`,`c`.`c3_score` AS `c3_score`,`c`.`c4_score` AS `c4_score`,`c`.`c5_score` AS `c5_score` from ((`employees` `p` left join `eva` `e` on((`p`.`emp_id` = `e`.`emp_id`))) left join `core_competency` `c` on(((`e`.`r_id` = `c`.`r_id`) and (`p`.`emp_id` = `c`.`emp_id`)))) ;

-- ----------------------------
-- Auto increment value for `competency`
-- ----------------------------
ALTER TABLE `competency` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for `departments`
-- ----------------------------
ALTER TABLE `departments` AUTO_INCREMENT=11;

-- ----------------------------
-- Auto increment value for `employees`
-- ----------------------------
ALTER TABLE `employees` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for `eva`
-- ----------------------------
ALTER TABLE `eva` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for `jobs`
-- ----------------------------
ALTER TABLE `jobs` AUTO_INCREMENT=16;

-- ----------------------------
-- Auto increment value for `knowledge`
-- ----------------------------
ALTER TABLE `knowledge` AUTO_INCREMENT=7;

-- ----------------------------
-- Auto increment value for `kpi`
-- ----------------------------
ALTER TABLE `kpi` AUTO_INCREMENT=5;

-- ----------------------------
-- Auto increment value for `levels`
-- ----------------------------
ALTER TABLE `levels` AUTO_INCREMENT=13;

-- ----------------------------
-- Auto increment value for `plan`
-- ----------------------------
ALTER TABLE `plan` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for `rounds`
-- ----------------------------
ALTER TABLE `rounds` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for `social_account`
-- ----------------------------
ALTER TABLE `social_account` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `types`
-- ----------------------------
ALTER TABLE `types` AUTO_INCREMENT=5;

-- ----------------------------
-- Auto increment value for `user`
-- ----------------------------
ALTER TABLE `user` AUTO_INCREMENT=2;
