<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "emp_salary".
 *
 * @property int $emp_id
 * @property float|null $base_salary
 * @property float|null $ot_salary
 * @property float|null $vp_salary
 * @property float|null $v11_salary
 * @property float|null $pts_salary
 * @property float|null $other_salary
 * @property float|null $tax_pay
 * @property float|null $kbk_pay
 * @property float|null $sso_pay
 * @property float|null $om_pay
 * @property float|null $dt_pay
 * @property float|null $elec_pay
 * @property float|null $bank_pay
 * @property float|null $insurance_pay
 * @property float|null $other_pay
 * @property string|null $d_update
 * @property string|null $account_no
 */
class EmpSalary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emp_salary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id'], 'required'],
            [['emp_id'], 'integer'],
            [['base_salary', 'extra_salary', 'ot_salary', 'v11_salary', 'vp_salary', 'pts_salary', 'pts_extra_salary', 'other_salary', 'tax_pay', 'kbk_pay', 'sso_pay', 'om_pay', 'dt_pay', 'elec_pay', 'bank_pay', 'insurance_pay', 'other_pay'], 'number'],
            [['d_update'], 'safe'],
            [['emp_status'], 'string' ,'max' => 1],
            [['account_no','account_bank'], 'string' ,'max' => 255],
            [['emp_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_id' => 'Emp ID',
            'base_salary' => 'เงินเดือน',
            'extra_salary' => 'เงินเดือน (ตกเบิก)',
            'ot_salary' => 'เงินตอบแทนนอกเวลา',
            'vp_salary' => 'ไม่ทำเวชปฏิบัติ',
            'v11_salary' => 'เบี้ยเลี้ยงเหมาจ่าย (ฉ11)',
            'pts_salary' => 'เงิน พตส. (งบร.)',
            'pts_extra_salary' => 'เงิน พตส. (งปม.)',
            'other_salary' => 'อื่นๆ',
            'tax_pay' => 'ภาษี',
            'kbk_pay' => 'ค่าบ้านพัก',
            'sso_pay' => 'ประกันสังคม',
            'om_pay' => 'ค่าทุนเรือนหุ้น-เงินกู้สหกรณ์',
            'dt_pay' => 'ฌกส.',
            'elec_pay' => 'ค่าไฟฟ้า',
            'bank_pay' => 'กยศ.',
            'insurance_pay' => 'ค่าประกันต่างๆ',
            'other_pay' => 'อื่นๆ',
            'd_update' => 'วันที่ปรับปรุงข้อมูลล่าสุด',
            'account_no' => 'เลขบัญชี',
            'account_bank' => 'ธนาคาร',
        ];
    }

    public static function itemsAlias($key){

        $items = [
          'c'=>[
            1 => 10,
            2 => 15,
            3 => 30,
            4 => 90,
            5 => 15,
          ],
      ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemC()
    {
      return self::itemsAlias('c');
    }

    public function getEmp()
    {
        return $this->hasOne(Employees::className(), ['emp_id' => 'emp_id']);
    }

    public function getEmpName()
    {
        return $this->emp->pname.$this->emp->fname.' '.$this->emp->lname;
    }

    public function getDepName()
    {
        return $this->emp->getDepName();
    }

    public function getLastPay()
    {
        $salary = Salary::find()->where(['emp_id'=>$this->emp_id])->orderBy(['salary_date'=>SORT_DESC])->one();
        return $salary ? $salary->salary_date:'NA';
    }

    public function getThaiDate($date){
        $months = ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $thyear = substr($date,0,4)+543;
        $thmonth = $months[substr($date,5,2)-1];
        $thday = intVal(substr($date,8,2));
        return $thday.' '.$thmonth.' พ.ศ. '.$thyear; 
    }
}
