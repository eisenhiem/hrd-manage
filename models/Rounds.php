<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rounds".
 *
 * @property int $r_id
 * @property int|null $r_yearbudget
 * @property int|null $r_round
 * @property string|null $r_detail
 * @property string|null $is_active
 */
class Rounds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rounds';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_yearbudget', 'r_round'], 'integer'],
            [['is_active'], 'string'],
            [['r_detail'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'r_id' => 'R ID',
            'r_yearbudget' => 'ปีงบประมาณ',
            'r_round' => 'รอบที่ประเมิน',
            'r_detail' => 'รายละเอียด',
            'is_active' => 'สถานะการเปิดใช้งาน',
        ];
    }
}
