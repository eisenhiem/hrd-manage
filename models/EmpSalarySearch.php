<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmpSalary;

/**
 * EmpSalarySearch represents the model behind the search form of `app\models\EmpSalary`.
 */
class EmpSalarySearch extends EmpSalary
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id'], 'integer'],
            [['base_salary', 'ot_salary', 'v11_salary', 'pts_salary', 'other_salary', 'tax_pay', 'kbk_pay', 'sso_pay', 'om_pay', 'dt_pay', 'elec_pay', 'bank_pay', 'insurance_pay', 'other_pay'], 'number'],
            [['d_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmpSalary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'emp_id' => $this->emp_id,
            'base_salary' => $this->base_salary,
            'ot_salary' => $this->ot_salary,
            'v11_salary' => $this->v11_salary,
            'pts_salary' => $this->pts_salary,
            'other_salary' => $this->other_salary,
            'tax_pay' => $this->tax_pay,
            'kbk_pay' => $this->kbk_pay,
            'sso_pay' => $this->sso_pay,
            'om_pay' => $this->om_pay,
            'dt_pay' => $this->dt_pay,
            'elec_pay' => $this->elec_pay,
            'bank_pay' => $this->bank_pay,
            'insurance_pay' => $this->insurance_pay,
            'other_pay' => $this->other_pay,
            'd_update' => $this->d_update,
        ]);

        return $dataProvider;
    }
}
