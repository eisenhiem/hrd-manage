<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sum_eva".
 *
 * @property int|null $r_id
 * @property int $dep_id
 * @property int $emp_id
 * @property int|null $k1_gap
 * @property int|null $k2_gap
 * @property int|null $com_gap
 * @property int|null $eng_gap
 * @property int|null $cal_gap
 * @property int|null $data_gap
 * @property int|null $c1_score
 * @property int|null $c2_score
 * @property int|null $c3_score
 * @property int|null $c4_score
 * @property int|null $c5_score
 */
class SumEva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sum_eva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_id', 'dep_id', 'emp_id', 'k1_gap', 'k2_gap', 'com_gap', 'eng_gap', 'cal_gap', 'data_gap', 'c1_score', 'c2_score', 'c3_score', 'c4_score', 'c5_score'], 'integer'],
            [['dep_id'], 'required'],
        ];
    }

    public static function primaryKey()
    {
        return ['r_id','emp_id'];
    } 
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'r_id' => 'รอบประเมิน',
            'dep_id' => 'กลุ่มงาน',
            'emp_id' => 'บุคลากร',
            'k1_gap' => 'ความรู้ที่จำเป็นในการปฏิบัติงานตามตำแหน่ง',
            'k2_gap' => 'ความรู้เกี่ยวกับกฎหมายและระเบียบราชการ',
            'com_gap' => 'คอมพิวเตอร์',
            'eng_gap' => 'ภาษาอังกฤษ',
            'cal_gap' => 'คำนวน',
            'data_gap' => 'จัดการข้อมูล',
            'c1_score' => 'การมุ่งผลสัมฤทธิ์',
            'c2_score' => 'บริการที่ดี',
            'c3_score' => 'การสั่งสมความเชี่ยวชาญในอาชีพ',
            'c4_score' => 'จริยธรรม',
            'c5_score' => 'ความร่วมแรงร่วมใจ',
        ];
    }
    /**
     * Gets รอบประเมิน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRounds()
    {
        return $this->hasOne(Rounds::className(), ['r_id' => 'r_id']);
    }

    public function getRoundName()
    {
        return $this->rounds->r_detail;
    }

    /**
     * Gets Employee
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasOne(Employees::className(), ['emp_id' => 'emp_id']);
    }

    public function getFullName()
    {
        return $this->employees->pname.$this->employees->fname.' '.$this->employees->lname;
    }

        /**
     * Gets Employee
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments()
    {
        return $this->hasOne(Departments::className(), ['dep_id' => 'dep_id']);
    }

}
