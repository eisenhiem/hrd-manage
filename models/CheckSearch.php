<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Check;

/**
 * CheckSearch represents the model behind the search form of `app\models\Check`.
 */
class CheckSearch extends Check
{
    public $start_date;
    public $end_date;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PCODE', 'emp_name', 'REGIST_DATE', 'REGIST_TIME', 'CHECKTYPE','start_date','end_date'], 'safe'],
            [['dep_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Check::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dep_id' => $this->dep_id,
            'REGIST_DATE' => $this->REGIST_DATE,
//            'REGIST_TIME' => $this->REGIST_TIME,
        ]);

        $query->andFilterWhere(['like', 'PCODE', $this->PCODE])
            ->andFilterWhere(['like', 'emp_name', $this->emp_name])
            ->andFilterWhere(['>=', 'REGIST_DATE',$this->start_date])
            ->andFilterWhere(['<=', 'REGIST_DATE',$this->end_date ])
            ->andFilterWhere(['like', 'CHECKTYPE', $this->CHECKTYPE]);

        return $dataProvider;
    }
}
