<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "check".
 *
 * @property string $PCODE
 * @property string|null $emp_name
 * @property int|null $dep_id
 * @property string $REGIST_DATE
 * @property string $REGIST_TIME
 * @property string $CHECKTYPE
 */
class Check extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'check';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dep_id'], 'integer'],
            [['REGIST_DATE', 'REGIST_TIME'], 'required'],
            [['REGIST_DATE', 'REGIST_TIME'], 'safe'],
            [['PCODE'], 'string', 'max' => 10],
            [['emp_name'], 'string', 'max' => 101],
            [['CHECKTYPE'], 'string', 'max' => 1],
        ];
    }

    public static function primaryKey()
    {
        return ['PCODE','REGIST_DATE','REGIST_TIME'];
    } 

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PCODE' => 'รหัสลายนิ้มือ',
            'emp_name' => 'ชื่อ-สกุล',
            'dep_id' => 'กลุ่มงาน/แผนก',
            'REGIST_DATE' => 'วันที่',
            'REGIST_TIME' => 'เวลา',
            'CHECKTYPE' => 'ประเภทการลงเวลา',
        ];
    }

    public function getCheckName(){
        return $this->CHECKTYPE == 0 ? 'เข้า':'ออก';
    }
    
    /**
     * Gets กลุ่มงาน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments()
    {
        return $this->hasOne(Departments::className(), ['dep_id' => 'dep_id']);
    }

    public function getDepName()
    {
        return $this->departments->dep_name;
    }

    public function getThaiDate(){
        $months = ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $thyear = substr($this->REGIST_DATE,0,4)+543;
        $thmonth = $months[substr($this->REGIST_DATE,5,2)-1];
        $thday = intVal(substr($this->REGIST_DATE,8,2));
        return $thday.' '.$thmonth.' พ.ศ. '.$thyear; 
    }

}
