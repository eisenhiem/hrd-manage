<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Core;

/**
 * CoreSearch represents the model behind the search form of `app\models\Core`.
 */
class CoreSearch extends Core
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['core_id'], 'integer'],
            [['core_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Core::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'core_id' => $this->core_id,
        ]);

        $query->andFilterWhere(['like', 'core_name', $this->core_name]);

        return $dataProvider;
    }
}
