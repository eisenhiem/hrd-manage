<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_time".
 *
 * @property int $work_time_id
 * @property string|null $work_time_name
 * @property string|null $work_code
 */
class WorkTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['work_time_name'], 'string', 'max' => 255],
            [['work_code'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'work_time_id' => 'Work Time ID',
            'work_time_name' => 'Work Time Name',
            'work_code' => 'Work Code',
        ];
    }
}
