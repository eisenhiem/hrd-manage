<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "evaluates".
 *
 * @property int $emp_id
 * @property int $r_id
 * @property int $k_id
 * @property int|null $standard_score
 * @property int|null $evaluate_score
 * @property string|null $subject_training
 * @property string|null $by_self
 * @property string|null $by_boss
 * @property string|null $workshop
 */
class Evaluates extends \yii\db\ActiveRecord
{
    public $items;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'evaluates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'r_id', 'k_id'], 'required'],
            [['emp_id', 'r_id', 'k_id', 'standard_score', 'evaluate_score'], 'integer'],
            [['subject_training'], 'string'],
            [['by_self', 'by_boss' ], 'string', 'max' => 1],
            [['workshop'], 'string', 'max' => 30],
            [['emp_id', 'r_id', 'k_id'], 'unique', 'targetAttribute' => ['emp_id', 'r_id', 'k_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_id' => 'Emp ID',
            'r_id' => 'R ID',
            'k_id' => 'K ID',
            'standard_score' => 'ค่ามาตรฐาน (Standard Score)',
            'evaluate_score' => 'ค่าประเมิน (Evaluate Score)',
            'subject_training' => 'ประเด็น (Subject Training)',
            'by_self' => 'พัฒนาด้วยตนเอง',
            'by_boss' => 'พัฒนาโดยหัวหน้างาน',
            'workshop' => 'พัฒนาอบรม',
        ];
    }
    /**
     * Gets รอบประเมิน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRounds()
    {
        return $this->hasOne(Rounds::className(), ['r_id' => 'r_id']);
    }

    public function getRoundName()
    {
        return $this->rounds->r_detail;
    }

}
