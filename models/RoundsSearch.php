<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rounds;

/**
 * RoundsSearch represents the model behind the search form of `app\models\Rounds`.
 */
class RoundsSearch extends Rounds
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_id', 'r_yearbudget', 'r_round'], 'integer'],
            [['r_detail','is_active'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rounds::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'r_id' => $this->r_id,
            'r_yearbudget' => $this->r_yearbudget,
            'r_round' => $this->r_round,
        ]);

        $query->andFilterWhere(['like', 'r_detail', $this->r_detail])
            ->andFilterWhere(['like', 'is_active', $this->is_active]);
        ;

        return $dataProvider;
    }
}
