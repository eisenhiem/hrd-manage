<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Employees;

/**
 * EmployeesSearch represents the model behind the search form of `app\models\Employees`.
 */
class EmployeesSearch extends Employees
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'level', 'position_type', 'dep_id'], 'integer'],
            [['pname', 'fname', 'lname', 'position','finger_id', 'birthdate', 'start_date', 'education', 'branch', 'job1', 'job2', 'job3', 'job4', 'job5', 'job6'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employees::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'emp_id' => $this->emp_id,
            'level' => $this->level,
            'position_type' => $this->position_type,
            'birthdate' => $this->birthdate,
            'start_date' => $this->start_date,
            'dep_id' => $this->dep_id,
        ]);

        $query->andFilterWhere(['like', 'pname', $this->pname])
            ->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'lname', $this->lname])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'branch', $this->branch])
            ->andFilterWhere(['like', 'finger_id', $this->finger_id])
            ->andFilterWhere(['like', 'job1', $this->job1])
            ->andFilterWhere(['like', 'job2', $this->job2])
            ->andFilterWhere(['like', 'job3', $this->job3])
            ->andFilterWhere(['like', 'job4', $this->job4])
            ->andFilterWhere(['like', 'job5', $this->job5])
            ->andFilterWhere(['like', 'job6', $this->job6]);

        return $dataProvider;
    }
}
