<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kpi".
 *
 * @property int $kpi_id
 * @property int|null $r_id
 * @property int $emp_id
 * @property string $kpi_no
 * @property string $kpi_name
 * @property string|null $kpi_template
 * @property int $kpi_ratio
 * @property int $kpi_weight
 * @property int|null $kpi_1
 * @property int|null $kpi_2
 * @property int|null $kpi_3
 * @property int|null $kpi_4
 * @property int|null $kpi_5
 * @property int|null $kpi_total
 * @property string|null $kpi_active
 * @property int|null $evaluate_by
 */
class Kpi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kpi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_id', 'emp_id', 'kpi_score', 'kpi_weight', 'kpi_total', 'evaluate_by'], 'integer'],
            [['emp_id', 'kpi_no', 'kpi_name', 'kpi_ratio', 'kpi_weight'], 'required'],
            [['kpi_no', 'kpi_template', 'kpi_active', 'kpi_1', 'kpi_2', 'kpi_3', 'kpi_4', 'kpi_5',], 'string'],
            [['kpi_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kpi_id' => 'Kpi ID',
            'r_id' => 'รอบประเมิน',
            'emp_id' => 'Emp ID',
            'kpi_no' => 'ตัวชี้วัดที่',
            'kpi_name' => 'ชื่อตัวชี้วัด',
            'kpi_template' => 'Template',
            'kpi_score' => 'คะแนน',
            'kpi_weight' => 'น้ำหนัก',
            'kpi_1' => 'คะแนน 1',
            'kpi_2' => 'คะแนน 2',
            'kpi_3' => 'คะแนน 3',
            'kpi_4' => 'คะแนน 4',
            'kpi_5' => 'คะแนน 5',
            'kpi_total' => 'คะแนนรวม',
            'kpi_active' => 'ใช้งาน',
            'evaluate_by' => 'ประเมินโดย',
        ];
    }
    
    /**
     * Gets รอบประเมิน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRounds()
    {
        return $this->hasOne(Rounds::className(), ['r_id' => 'r_id']);
    }

    public function getRoundName()
    {
        return $this->rounds->r_detail;
    }

}
