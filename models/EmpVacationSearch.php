<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmpVacation;

/**
 * EmpVacationSearch represents the model behind the search form of `app\models\EmpVacation`.
 */
class EmpVacationSearch extends EmpVacation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'yearbudget', 'type_1_days', 'type_2_days', 'type_3_days', 'type_4_days', 'type_5_days', 'type_6_days', 'brought_forward_days', 'max_days', 'remain_days'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmpVacation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'emp_id' => $this->emp_id,
            'yearbudget' => $this->yearbudget,
            'type_1_days' => $this->type_1_days,
            'type_2_days' => $this->type_2_days,
            'type_3_days' => $this->type_3_days,
            'type_4_days' => $this->type_4_days,
            'type_5_days' => $this->type_5_days,
            'type_6_days' => $this->type_6_days,
            'brought_forward_days' => $this->brought_forward_days,
            'max_days' => $this->max_days,
            'remain_days' => $this->remain_days,
        ]);

        return $dataProvider;
    }
}
