<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Evaluates;

/**
 * EvaluatesSearch represents the model behind the search form of `app\models\Evaluates`.
 */
class EvaluatesSearch extends Evaluates
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'r_id', 'k_id', 'standard_score', 'evaluate_score'], 'integer'],
            [['subject_training', 'by_self', 'by_boss', 'workshop'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Evaluates::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'emp_id' => $this->emp_id,
            'r_id' => $this->r_id,
            'k_id' => $this->k_id,
            'standard_score' => $this->standard_score,
            'evaluate_score' => $this->evaluate_score,
        ]);

        $query->andFilterWhere(['like', 'subject_training', $this->subject_training])
            ->andFilterWhere(['like', 'by_self', $this->by_self])
            ->andFilterWhere(['like', 'by_boss', $this->by_boss])
            ->andFilterWhere(['like', 'workshop', $this->workshop]);

        return $dataProvider;
    }
}
