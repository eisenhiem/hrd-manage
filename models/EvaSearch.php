<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Eva;

/**
 * EvaSearch represents the model behind the search form of `app\models\Eva`.
 */
class EvaSearch extends Eva
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eva_id', 'emp_id', 'r_id', 'k1_std_score', 'k1_eva_score', 'k1_gap', 'k2_std_score', 'k2_eva_score', 'k2_gap', 'com_std_score', 'com_eva_score', 'com_gap', 'eng_std_score', 'eng_eva_score', 'eng_gap', 'cal_std_score', 'cal_eva_score', 'cal_gap', 'data_std_score', 'data_eva_score', 'data_gap'], 'integer'],
            [['k1_subject', 'k1_self', 'k1_boss', 'k1_workshop', 'k2_subject', 'k2_self', 'k2_boss', 'k2_workshop', 'com_subject', 'com_self', 'com_boss', 'com_workshop', 'eng_subject', 'eng_self', 'eng_boss', 'eng_workshop', 'cal_subject', 'cal_self', 'cal_boss', 'cal_workshop', 'data_subject', 'data_self', 'data_boss', 'data_workshop'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Eva::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'eva_id' => $this->eva_id,
            'emp_id' => $this->emp_id,
            'r_id' => $this->r_id,
            'k1_std_score' => $this->k1_std_score,
            'k1_eva_score' => $this->k1_eva_score,
            'k1_gap' => $this->k1_gap,
            'k2_std_score' => $this->k2_std_score,
            'k2_eva_score' => $this->k2_eva_score,
            'k2_gap' => $this->k2_gap,
            'com_std_score' => $this->com_std_score,
            'com_eva_score' => $this->com_eva_score,
            'com_gap' => $this->com_gap,
            'eng_std_score' => $this->eng_std_score,
            'eng_eva_score' => $this->eng_eva_score,
            'eng_gap' => $this->eng_gap,
            'cal_std_score' => $this->cal_std_score,
            'cal_eva_score' => $this->cal_eva_score,
            'cal_gap' => $this->cal_gap,
            'data_std_score' => $this->data_std_score,
            'data_eva_score' => $this->data_eva_score,
            'data_gap' => $this->data_gap,
        ]);

        $query->andFilterWhere(['like', 'k1_subject', $this->k1_subject])
            ->andFilterWhere(['like', 'k1_self', $this->k1_self])
            ->andFilterWhere(['like', 'k1_boss', $this->k1_boss])
            ->andFilterWhere(['like', 'k1_workshop', $this->k1_workshop])
            ->andFilterWhere(['like', 'k2_subject', $this->k2_subject])
            ->andFilterWhere(['like', 'k2_self', $this->k2_self])
            ->andFilterWhere(['like', 'k2_boss', $this->k2_boss])
            ->andFilterWhere(['like', 'k2_workshop', $this->k2_workshop])
            ->andFilterWhere(['like', 'com_subject', $this->com_subject])
            ->andFilterWhere(['like', 'com_self', $this->com_self])
            ->andFilterWhere(['like', 'com_boss', $this->com_boss])
            ->andFilterWhere(['like', 'com_workshop', $this->com_workshop])
            ->andFilterWhere(['like', 'eng_subject', $this->eng_subject])
            ->andFilterWhere(['like', 'eng_self', $this->eng_self])
            ->andFilterWhere(['like', 'eng_boss', $this->eng_boss])
            ->andFilterWhere(['like', 'eng_workshop', $this->eng_workshop])
            ->andFilterWhere(['like', 'cal_subject', $this->cal_subject])
            ->andFilterWhere(['like', 'cal_self', $this->cal_self])
            ->andFilterWhere(['like', 'cal_boss', $this->cal_boss])
            ->andFilterWhere(['like', 'cal_workshop', $this->cal_workshop])
            ->andFilterWhere(['like', 'data_subject', $this->data_subject])
            ->andFilterWhere(['like', 'data_self', $this->data_self])
            ->andFilterWhere(['like', 'data_boss', $this->data_boss])
            ->andFilterWhere(['like', 'data_workshop', $this->data_workshop]);

        return $dataProvider;
    }
}
