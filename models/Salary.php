<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "salary".
 *
 * @property int $salary_id
 * @property int $emp_id
 * @property string $salary_date
 * @property float|null $base_salary
 * @property float|null $ot_salary
 * @property float|null $vp_salary
 * @property float|null $v11_salary
 * @property float|null $pts_salary
 * @property float|null $other_salary
 * @property float|null $tax_pay
 * @property float|null $kbk_pay
 * @property float|null $sso_pay
 * @property float|null $om_pay
 * @property float|null $dt_pay
 * @property float|null $elec_pay
 * @property float|null $bank_pay
 * @property float|null $insurance_pay
 * @property float|null $other_pay
 */
class Salary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'salary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'salary_date'], 'required'],
            [['emp_id'], 'integer'],
            [['salary_date'], 'safe'],
            [['base_salary', 'extra_salary', 'ot_salary', 'v11_salary','vp_salary', 'pts_salary', 'pts_extra_salary', 'other_salary', 'tax_pay', 'kbk_pay', 'sso_pay', 'om_pay', 'dt_pay', 'elec_pay', 'bank_pay', 'insurance_pay', 'other_pay'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'salary_id' => 'Salary ID',
            'emp_id' => 'Emp ID',
            'base_salary' => 'เงินเดือน',
            'extra_salary' => 'เงินเดือน (ตกเบิก)',
            'ot_salary' => 'เงินตอบแทนนอกเวลา',
            'vp_salary' => 'ไม่ทำเวชปฏิบัติ',
            'v11_salary' => 'เบี้ยเลี้ยงเหมาจ่าย (ฉ11)',
            'pts_salary' => 'เงิน พตส. (งบร.)',
            'pts_extra_salary' => 'เงิน พตส. (งปม.)',
            'other_salary' => 'อื่นๆ',
            'tax_pay' => 'ภาษี',
            'kbk_pay' => 'ค่าบ้านพัก',
            'sso_pay' => 'ประกันสังคม',
            'om_pay' => 'ค่าทุนเรือนหุ้น-เงินกู้สหกรณ์',
            'dt_pay' => 'ฌกส.',
            'elec_pay' => 'ค่าไฟฟ้า',
            'bank_pay' => 'กยศ.',
            'insurance_pay' => 'ค่าประกันต่างๆ',
            'other_pay' => 'อื่นๆ',
            'd_update' => 'วันที่ปรับปรุงข้อมูลล่าสุด',
        ];
    }

    public function getEmp()
    {
        return $this->hasOne(Employees::className(), ['emp_id' => 'emp_id']);
    }

    public function getBase()
    {
        return $this->hasOne(EmpSalary::className(), ['emp_id' => 'emp_id']);
    }

    public function getEmpName()
    {
        return $this->emp->pname.$this->emp->fname.' '.$this->emp->lname;
    }

    public function getDepName()
    {
        return $this->emp->getDepName();
    }
    public function getThaiMonth($date){
        $months = ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $thyear = substr($date,0,4)+543;
        $thmonth = $months[substr($date,5,2)-1];
        return $thmonth.' พ.ศ. '.$thyear; 
    }

    public function getSumSalary()
    {
        $total = $this->base_salary + $this->extra_salary + $this->other_salary + $this->ot_salary + $this->v11_salary + $this->vp_salary + $this->pts_salary + $this->pts_extra_salary;
        return $total;
    }

    public function getSumPay()
    {
        $total = $this->tax_pay + $this->om_pay + $this->kbk_pay + $this->sso_pay + $this->elec_pay + $this->dt_pay + $this->insurance_pay + $this->other_pay + $this->bank_pay;
        return $total;
    }

}
