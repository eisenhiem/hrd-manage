<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "emp_vacation".
 *
 * @property int $emp_id
 * @property int $yearbudget
 * @property int|null $type_1_days
 * @property int|null $type_2_days
 * @property int|null $type_3_days
 * @property int|null $type_4_days
 * @property int|null $type_5_days
 * @property int|null $type_6_days
 * @property int|null $brought_forward_days
 * @property int|null $max_days
 * @property int|null $remain_days
 */
class EmpVacation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emp_vacation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'yearbudget'], 'required'],
            [['emp_id', 'yearbudget', 'type_1_days', 'type_2_days', 'type_3_days', 'type_4_days', 'type_5_days', 'type_6_days', 'brought_forward_days', 'max_days', 'remain_days'], 'integer'],
            [['emp_id', 'yearbudget'], 'unique', 'targetAttribute' => ['emp_id', 'yearbudget']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_id' => 'Emp ID',
            'yearbudget' => 'ปีงบประมาณ',
            'type_1_days' => 'ลากิจ',
            'type_2_days' => 'ลาป่วย',
            'type_3_days' => 'ลาพักผ่อน',
            'type_4_days' => 'ลาบวช/ลาคลอด',
            'type_5_days' => 'ลาไปราชการ',
            'type_6_days' => 'ลาอื่นๆ',
            'brought_forward_days' => 'วันลาสะสมยกมาจากปีที่แล้ว',
            'max_days' => 'วันลาในปี',
            'remain_days' => 'วันลาคงเหลือ',
        ];
    }

    public static function itemsAlias($key){

        $items = [
          'maxdays'=>[
            1 => 10,
            2 => 15,
            3 => 30,
            4 => 90,
            5 => 15,
            6 => 'ลาไปราชการ/ลาศึกษาต่อ',
          ],
      ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemType()
    {
      return self::itemsAlias('type');
    }

    public function getTypeName(){
        return ArrayHelper::getValue($this->getItemType(),$this->va_type);
    }


    public function getEmp()
    {
        return $this->hasOne(Employees::className(), ['emp_id' => 'emp_id']);
    }

    public function getEmpName()
    {
        return $this->emp->pname.$this->emp->fname.' '.$this->emp->lname;
    }

    public function getGitValue()
    {
        $days = Vacation::find()->where(['va_type' => 1,'emp_id' => $this->emp_id, 'yearbudget' => $this->yearbudget])->sum('total_day');
        $total = Vacation::find()->where(['va_type' => 1,'emp_id' => $this->emp_id, 'yearbudget' => $this->yearbudget])->count('va_id');
        if($days){
            return $total.'/'.$days;
        }
        else {
            return '0/0';
        }
    }

    public function getSickValue()
    {
        $days = Vacation::find()->where(['va_type' => 2,'emp_id' => $this->emp_id, 'yearbudget' => $this->yearbudget])->sum('total_day');
        $total = Vacation::find()->where(['va_type' => 2,'emp_id' => $this->emp_id, 'yearbudget' => $this->yearbudget])->count('va_id');
        if($days){
            return $total.'/'.$days;
        }
        else {
            return '0/0';
        }
    }

    public function getVacationValue()
    {
        $days = Vacation::find()->where(['va_type' => 3,'emp_id' => $this->emp_id, 'yearbudget' => $this->yearbudget])->sum('total_day');
        $total = Vacation::find()->where(['va_type' => 3,'emp_id' => $this->emp_id, 'yearbudget' => $this->yearbudget])->count('va_id');
        if($days){
            return $total.'/'.$days;
        }
        else {
            return '0/0';
        }
    }

    public function getOtherValue()
    {
        $days = Vacation::find()->where(['emp_id' => $this->emp_id, 'yearbudget' => $this->yearbudget])->andWhere(['>','va_type',3])->sum('total_day');
        $total = Vacation::find()->where(['emp_id' => $this->emp_id, 'yearbudget' => $this->yearbudget])->andWhere(['>','va_type',3])->count('va_id');
        if($days){
            return $total.'/'.$days;
        }
        else {
            return '0/0';
        }
    }

    public function getRemainValue()
    {
        $days = Vacation::find()->where(['va_type' => 3,'emp_id' => $this->emp_id, 'yearbudget' => $this->yearbudget])->sum('total_day');
        if($days){
            return $this->getTotalVa()-$days;
        }
        else {
            return $this->getTotalVa();
        }
    }

    public function getTotalVa()
    {
        return $this->max_days+$this->brought_forward_days;
    }
}
