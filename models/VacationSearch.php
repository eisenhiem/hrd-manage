<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vacation;

/**
 * VacationSearch represents the model behind the search form of `app\models\Vacation`.
 */
class VacationSearch extends Vacation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['va_id', 'emp_id', 'yearbudget','va_type'], 'integer'],
            [['start_date', 'register_date', 'end_date', 'd_update'], 'safe'],
            [['total_day'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vacation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'va_id' => $this->va_id,
            'emp_id' => $this->emp_id,
            'yearbudget' => $this->yearbudget,
            'va_type' => $this->va_type,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'register_date' => $this->register_date,
            'total_day' => $this->total_day,
            'd_update' => $this->d_update,
        ]);

        return $dataProvider;
    }
}
