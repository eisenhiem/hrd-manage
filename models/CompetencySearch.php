<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Competency;

/**
 * CompetencySearch represents the model behind the search form of `app\models\Competency`.
 */
class CompetencySearch extends Competency
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comp_id', 'r_id', 'emp_id','core_id', 'evaluate_by', 'level', 'evaluate', 'weight', 'total'], 'integer'],
            [['comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Competency::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'comp_id' => $this->comp_id,
            'r_id' => $this->r_id,
            'emp_id' => $this->emp_id,
            'core_id' => $this->core_id,
            'evaluate_by' => $this->evaluate_by,
            'level' => $this->level,
            'evaluate' => $this->evaluate,
            'weight' => $this->weight,
            'total' => $this->total,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
