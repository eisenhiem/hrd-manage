<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "vacation".
 *
 * @property int $va_id
 * @property int $emp_id
 * @property int $va_type
 * @property string $register_date
 * @property string $start_date
 * @property float|null $total_day
 * @property string|null $d_update
 */
class Vacation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'va_type', 'start_date', 'register_date', 'end_date'], 'required'],
            [['emp_id', 'yearbudget','va_type'], 'integer'],
            [['start_date', 'register_date','end_date', 'd_update'], 'safe'],
            [['total_day'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'va_id' => 'Va ID',
            'emp_id' => 'Emp ID',
            'yearbudget' => 'ปีงบประมาณ',
            'va_type' => 'ประเภทวันลา',
            'start_date' => 'วันที่เริ่มลา',
            'end_date' => 'ลาถึงวันที่',
            'register_date' => 'วันที่ส่งหนังสือ',
            'total_day' => 'จำนวนวันลา',
            'd_update' => 'D Update',
        ];
    }

    public static function itemsAlias($key){

        $items = [
          'type'=>[
            1 => 'ลากิจ',
            2 => 'ลาป่วย',
            3 => 'ลาพักผ่อน',
            4 => 'ลาบวช/ลาคลอด',
            5 => 'ลาไปราชการ',
            6 => 'ลาอื่นๆ',
          ],
      ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemType()
    {
      return self::itemsAlias('type');
    }

    public function getTypeName(){
        return ArrayHelper::getValue($this->getItemType(),$this->va_type);
    }

    public function getEmployees()
    {
        return $this->hasOne(Employees::className(), ['emp_id' => 'emp_id']);
    }

    public function getThaiDate($date){
        $months = ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $thyear = substr($date,0,4)+543;
        $thmonth = $months[substr($date,5,2)-1];
        $thday = intVal(substr($date,8,2));
        return $thday.' '.$thmonth.' พ.ศ. '.$thyear; 
    }

}
