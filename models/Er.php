<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "er".
 *
 * @property int $emp_id
 * @property int|null $rate_ot
 * @property int|null $rate_bd
 */
class Er extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'er';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id'], 'required'],
            [['emp_id', 'rate_ot', 'rate_bd'], 'integer'],
            [['emp_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_id' => 'Emp ID',
            'rate_ot' => 'Rate Ot',
            'rate_bd' => 'Rate Bd',
        ];
    }

    /**
     * Gets เจ้าหน้าที่
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employees::className(), ['emp_id' => 'emp_id']);
    }

    public function getName()
    {
        return $this->employee->fname.' '.$this->employee->lname;
    }

}
