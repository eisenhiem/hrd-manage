<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plan".
 *
 * @property int $plan_id
 * @property int $emp_id
 * @property int $r_id
 * @property string|null $plan_development
 * @property string|null $deverlop_by
 * @property string|null $duration
 */
class Plan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'r_id'], 'required'],
            [['emp_id', 'r_id'], 'integer'],
            [['plan_development'], 'string'],
            [['deverlop_by', 'duration'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'Plan ID',
            'emp_id' => 'Emp ID',
            'r_id' => 'R ID',
            'plan_development' => 'แผนพัฒนาสมรรถนะ/ความรู้/ทักษะ',
            'deverlop_by' => 'รูปแบบพัฒนา',
            'duration' => 'ช่วงเวลาที่พัฒนา',
        ];
    }
}
