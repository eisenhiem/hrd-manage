<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "knowledge".
 *
 * @property int $k_id
 * @property string|null $k_name
 */
class Knowledge extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'knowledge';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['k_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'k_id' => 'K ID',
            'k_name' => 'องค์ความรู้',
        ];
    }
}
