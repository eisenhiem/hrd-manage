<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_type".
 *
 * @property int $work_type_id
 * @property string|null $work_type_name
 */
class WorkType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['work_type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'work_type_id' => 'Work Type ID',
            'work_type_name' => 'Work Type Name',
        ];
    }
}
