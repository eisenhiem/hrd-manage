<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rep_check_all".
 *
 * @property string|null $finger_id
 * @property string|null $fullname
 * @property int $dep_id
 * @property string|null $dep_name
 * @property string $reg_date
 * @property string|null $reg_time
 * @property string $check_status
 * @property string $status_name
 */
class RepCheckAll extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rep_check_all';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dep_id', 'reg_date'], 'required'],
            [['dep_id'], 'integer'],
            [['reg_date'], 'safe'],
            [['finger_id'], 'string', 'max' => 10],
            [['fullname'], 'string', 'max' => 356],
            [['dep_name'], 'string', 'max' => 255],
            [['reg_time'], 'string', 'max' => 5],
            [['check_status'], 'string', 'max' => 1],
            [['status_name'], 'string', 'max' => 13],
        ];
    }

    public static function primaryKey()
    {
        return ['finger_id','reg_date','reg_time','check_status'];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'finger_id' => 'รหัสลายนิ้วมือ',
            'fullname' => 'ชื่อ-สกุล',
            'dep_id' => 'Dep ID',
            'dep_name' => 'แผนก/ฝ่าย',
            'reg_date' => 'วันที่',
            'reg_time' => 'เวลา',
            'check_status' => 'Check Status',
            'status_name' => 'สถานะเข้า/ออก',
        ];
    }

    public function getRegisterDate()
    {
        $months = ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];
        $thyear = substr($this->reg_date,0,4)+543;
        $thmonth = $months[substr($this->reg_date,5,2)-1];
        $thday = intVal(substr($this->reg_date,8,2));
        return $thday.' '.$thmonth.' '.$thyear; 
    }
}
