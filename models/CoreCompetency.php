<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "core_competency".
 *
 * @property int $r_id
 * @property int $emp_id
 * @property int|null $c1_score
 * @property int|null $c2_score
 * @property int|null $c3_score
 * @property int|null $c4_score
 * @property int|null $c5_score
 */
class CoreCompetency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_competency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_id', 'emp_id'], 'required'],
            [['r_id', 'emp_id', 'c1_score', 'c2_score', 'c3_score', 'c4_score', 'c5_score'], 'integer'],
            [['r_id', 'emp_id'], 'unique', 'targetAttribute' => ['r_id', 'emp_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'r_id' => 'รอบประเมิน',
            'emp_id' => 'Emp ID',
            'c1_score' => 'การมุ่งผลสัมฤทธิ์',
            'c2_score' => 'บริการที่ดี',
            'c3_score' => 'การสั่งสมความเชี่ยวชาญในอาชีพ',
            'c4_score' => 'จริยธรรม',
            'c5_score' => 'ความร่วมแรงร่วมใจ',
        ];
    }
    /**
     * Gets รอบประเมิน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRounds()
    {
        return $this->hasOne(Rounds::className(), ['r_id' => 'r_id']);
    }

    public function getRoundName()
    {
        return $this->rounds->r_detail;
    }

    /**
     * Gets รอบประเมิน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmp()
    {
        return $this->hasOne(Employees::className(), ['emp_id' => 'emp_id']);
    }

    public function getEmpName()
    {
        return $this->emp->pname.$this->emp->fname.' '.$this->emp->lname;
    }


}
