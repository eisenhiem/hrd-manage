<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "core".
 *
 * @property int $core_id
 * @property string|null $core_name
 */
class Core extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['core_id'], 'required'],
            [['core_id'], 'integer'],
            [['core_name'], 'string', 'max' => 255],
            [['core_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'core_id' => 'Core ID',
            'core_name' => 'หัวข้อสมรรถนะ',
        ];
    }
}
