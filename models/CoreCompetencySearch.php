<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CoreCompetency;

/**
 * CoreCompetencySearch represents the model behind the search form of `app\models\CoreCompetency`.
 */
class CoreCompetencySearch extends CoreCompetency
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_id', 'emp_id', 'c1_score', 'c2_score', 'c3_score', 'c4_score', 'c5_score'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoreCompetency::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'r_id' => $this->r_id,
            'emp_id' => $this->emp_id,
            'c1_score' => $this->c1_score,
            'c2_score' => $this->c2_score,
            'c3_score' => $this->c3_score,
            'c4_score' => $this->c4_score,
            'c5_score' => $this->c5_score,
        ]);

        return $dataProvider;
    }
}
