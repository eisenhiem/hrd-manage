<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "eva".
 *
 * @property int $eva_id
 * @property int $emp_id
 * @property int $r_id
 * @property int $k1_std_score
 * @property int $k1_eva_score
 * @property int $k1_gap
 * @property string|null $k1_subject
 * @property string|null $k1_self
 * @property string|null $k1_boss
 * @property string|null $k1_workshop
 * @property int $k2_std_score
 * @property int $k2_eva_score
 * @property int $k2_gap
 * @property string|null $k2_subject
 * @property string|null $k2_self
 * @property string|null $k2_boss
 * @property string|null $k2_workshop
 * @property int $com_std_score
 * @property int $com_eva_score
 * @property int $com_gap
 * @property string|null $com_subject
 * @property string|null $com_self
 * @property string|null $com_boss
 * @property string|null $com_workshop
 * @property int $eng_std_score
 * @property int $eng_eva_score
 * @property int $eng_gap
 * @property string|null $eng_subject
 * @property string|null $eng_self
 * @property string|null $eng_boss
 * @property string|null $eng_workshop
 * @property int $cal_std_score
 * @property int $cal_eva_score
 * @property int $cal_gap
 * @property string|null $cal_subject
 * @property string|null $cal_self
 * @property string|null $cal_boss
 * @property string|null $cal_workshop
 * @property int $data_std_score
 * @property int $data_eva_score
 * @property int $data_gap
 * @property string|null $data_subject
 * @property string|null $data_self
 * @property string|null $data_boss
 * @property string|null $data_workshop
 */
class Eva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'r_id', 'k1_std_score', 'k1_eva_score', 'k1_gap', 'k2_std_score', 'k2_eva_score', 'k2_gap', 'com_std_score', 'com_eva_score', 'com_gap', 'eng_std_score', 'eng_eva_score', 'eng_gap', 'cal_std_score', 'cal_eva_score', 'cal_gap', 'data_std_score', 'data_eva_score', 'data_gap'], 'required'],
            [['emp_id', 'r_id', 'k1_std_score', 'k1_eva_score', 'k1_gap', 'k2_std_score', 'k2_eva_score', 'k2_gap', 'com_std_score', 'com_eva_score', 'com_gap', 'eng_std_score', 'eng_eva_score', 'eng_gap', 'cal_std_score', 'cal_eva_score', 'cal_gap', 'data_std_score', 'data_eva_score', 'data_gap'], 'integer'],
            [['k1_subject', 'k1_workshop', 'k2_subject', 'k2_workshop', 'com_subject', 'com_workshop', 'eng_subject', 'eng_workshop', 'cal_subject', 'cal_workshop', 'data_subject', 'data_workshop'], 'string', 'max' => 255],
            [['k1_self', 'k1_boss', 'k2_self', 'k2_boss', 'com_self', 'com_boss', 'eng_self', 'eng_boss', 'cal_self', 'cal_boss', 'data_self', 'data_boss'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'eva_id' => 'ID',
            'emp_id' => 'บุคคลากร',
            'r_id' => 'รอบที่ประเมิน',
            'k1_std_score' => 'ค่ามาตรฐานของตำแหน่ง (Standard Score)',
            'k1_eva_score' => 'ระดับผลการประเมิน (Evaluate Score)',
            'k1_gap' => 'Gap',
            'k1_subject' => 'ประเด็นการพัฒนา',
            'k1_self' => 'พัฒนาด้วยตนเอง',
            'k1_boss' => 'พัฒนาด้วยผู้บังคับบัญชา',
            'k1_workshop' => 'อบรม',
            'k2_std_score' => 'ค่ามาตรฐานของตำแหน่ง (Standard Score)',
            'k2_eva_score' => 'ระดับผลการประเมิน (Evaluate Score)',
            'k2_gap' => 'Gap',
            'k2_subject' => 'ประเด็นการพัฒนา',
            'k2_self' => 'พัฒนาด้วยตนเอง',
            'k2_boss' => 'พัฒนาด้วยผู้บังคับบัญชา',
            'k2_workshop' => 'อบรม',
            'com_std_score' => 'ค่ามาตรฐานของตำแหน่ง (Standard Score)',
            'com_eva_score' => 'ระดับผลการประเมิน (Evaluate Score)',
            'com_gap' => 'Gap',
            'com_subject' => 'ประเด็นการพัฒนา',
            'com_self' => 'พัฒนาด้วยตนเอง',
            'com_boss' => 'พัฒนาด้วยผู้บังคับบัญชา',
            'com_workshop' => 'อบรม',
            'eng_std_score' => 'ค่ามาตรฐานของตำแหน่ง (Standard Score)',
            'eng_eva_score' => 'ระดับผลการประเมิน (Evaluate Score)',
            'eng_gap' => 'Gap',
            'eng_subject' => 'ประเด็นการพัฒนา',
            'eng_self' => 'พัฒนาด้วยตนเอง',
            'eng_boss' => 'พัฒนาด้วยผู้บังคับบัญชา',
            'eng_workshop' => 'อบรม',
            'cal_std_score' => 'ค่ามาตรฐานของตำแหน่ง (Standard Score)',
            'cal_eva_score' => 'ระดับผลการประเมิน (Evaluate Score)',
            'cal_gap' => 'Gap',
            'cal_subject' => 'ประเด็นการพัฒนา',
            'cal_self' => 'พัฒนาด้วยตนเอง',
            'cal_boss' => 'พัฒนาด้วยผู้บังคับบัญชา',
            'cal_workshop' => 'อบรม',
            'data_std_score' => 'ค่ามาตรฐานของตำแหน่ง (Standard Score)',
            'data_eva_score' => 'ระดับผลการประเมิน (Evaluate Score)',
            'data_gap' => 'Gap',
            'data_subject' => 'ประเด็นการพัฒนา',
            'data_self' => 'พัฒนาด้วยตนเอง',
            'data_boss' => 'พัฒนาด้วยผู้บังคับบัญชา',
            'data_workshop' => 'อบรม',
        ];
    }
    
    /**
     * Gets รอบประเมิน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRounds()
    {
        return $this->hasOne(Rounds::className(), ['r_id' => 'r_id']);
    }

    public function getRoundName()
    {
        return $this->rounds->r_detail;
    }

}
