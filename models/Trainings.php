<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trainings".
 *
 * @property int $emp_id
 * @property int $r_id
 * @property int|null $achievment
 * @property int|null $service_mind
 * @property int|null $expertise
 * @property int|null $integrity
 * @property int|null $teamwork
 * @property int|null $reading
 * @property int|null $reading_level
 * @property int|null $listening
 * @property int|null $listening_level
 * @property int|null $speaking
 * @property int|null $speaking_level
 * @property int|null $writing
 * @property int|null $wrinting_level
 * @property int|null $ms_word
 * @property int|null $ms_word_level
 * @property int|null $ms_excel
 * @property int|null $ms_excel_level
 * @property int|null $ms_powerpoint
 * @property int|null $ms_powerpoint_level
 * @property int|null $ms_access
 * @property int|null $ms_access_level
 * @property int|null $e_mail
 * @property int|null $e_mail_level
 * @property string|null $other1
 * @property int|null $other1_level
 * @property int|null $laws
 * @property int|null $rm
 * @property int|null $sm
 * @property int|null $cm
 * @property int|null $pmqa
 * @property int|null $km
 * @property int|null $bs
 * @property int|null $sp
 * @property int|null $hr
 * @property string|null $other2
 */
class Trainings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trainings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'r_id'], 'required'],
            [['emp_id', 'r_id', 'achievment', 'service_mind', 'expertise', 'integrity', 'teamwork', 'reading', 'reading_level', 'listening', 'listening_level', 'speaking', 'speaking_level', 'writing', 'wrinting_level', 'ms_word', 'ms_word_level', 'ms_excel', 'ms_excel_level', 'ms_powerpoint', 'ms_powerpoint_level', 'ms_access', 'ms_access_level', 'e_mail', 'e_mail_level', 'other1_level', 'laws', 'rm', 'sm', 'cm', 'pmqa', 'km', 'bs', 'sp', 'hr'], 'integer'],
            [['other1', 'other2'], 'string', 'max' => 255],
            [['emp_id', 'r_id'], 'unique', 'targetAttribute' => ['emp_id', 'r_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_id' => 'Emp ID',
            'r_id' => 'รอบที่ประเมิน',
            'achievment' => 'Achievment',
            'service_mind' => 'Service Mind',
            'expertise' => 'Expertise',
            'integrity' => 'Integrity',
            'teamwork' => 'Teamwork',
            'reading' => 'Reading',
            'reading_level' => 'Reading Level',
            'listening' => 'Listening',
            'listening_level' => 'Listening Level',
            'speaking' => 'Speaking',
            'speaking_level' => 'Speaking Level',
            'writing' => 'Writing',
            'wrinting_level' => 'Wrinting Level',
            'ms_word' => 'Ms Word',
            'ms_word_level' => 'Ms Word Level',
            'ms_excel' => 'Ms Excel',
            'ms_excel_level' => 'Ms Excel Level',
            'ms_powerpoint' => 'Ms Powerpoint',
            'ms_powerpoint_level' => 'Ms Powerpoint Level',
            'ms_access' => 'Ms Access',
            'ms_access_level' => 'Ms Access Level',
            'e_mail' => 'E Mail',
            'e_mail_level' => 'E Mail Level',
            'other1' => 'Other1',
            'other1_level' => 'Other1 Level',
            'laws' => 'Laws',
            'rm' => 'Rm',
            'sm' => 'Sm',
            'cm' => 'Cm',
            'pmqa' => 'Pmqa',
            'km' => 'Km',
            'bs' => 'Bs',
            'sp' => 'Sp',
            'hr' => 'Hr',
            'other2' => 'Other2',
        ];
    }
    
    /**
     * Gets รอบประเมิน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRounds()
    {
        return $this->hasOne(Rounds::className(), ['r_id' => 'r_id']);
    }

    public function getRoundName()
    {
        return $this->rounds->r_detail;
    }

    public function getLevelName($l){
        return $l == 1 ? 'ระดับต้น' : 'ระดับสูง';
    }

}
