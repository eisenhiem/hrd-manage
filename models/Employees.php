<?php

namespace app\models;

use Yii;
use yii\i18n\Formatter;
/**
 * This is the model class for table "employees".
 *
 * @property int $emp_id
 * @property string $pname
 * @property string $fname
 * @property string $lname
 * @property string $position
 * @property int $level
 * @property int $position_type
 * @property string $birthdate
 * @property string $start_date
 * @property string|null $education
 * @property string|null $branch
 * @property int $dep_id
 * @property string|null $job1
 * @property string|null $job2
 * @property string|null $job3
 * @property string|null $job4
 * @property string|null $job5
 * @property string|null $job6
 */
class Employees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pname', 'fname', 'lname', 'position', 'level', 'position_type', 'birthdate', 'start_date', 'dep_id'], 'required'],
            [['level', 'position_type', 'dep_id'], 'integer'],
            [['birthdate', 'start_date'], 'safe'],
            [['finger_id'],'string','max'=>10],
            [['pname', 'education', 'branch', 'job1', 'job2', 'job3', 'job4', 'job5', 'job6'], 'string', 'max' => 255],
            [['fname', 'lname', 'position'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_id' => 'ID',
            'pname' => 'คำนำหน้า',
            'fname' => 'ชื่อ',
            'lname' => 'สกุล',
            'position' => 'ตำแหน่ง',
            'level' => 'ระดับ',
            'position_type' => 'ประเภท',
            'birthdate' => 'วันเกิด',
            'start_date' => 'วันที่เริ่มทำงาน',
            'education' => 'การศึกษา',
            'branch' => 'สาขา',
            'dep_id' => 'กลุ่มงาน',
            'job1' => '1',
            'job2' => '2',
            'job3' => '3',
            'job4' => '4',
            'job5' => '5',
            'job6' => '6',
            'finger_id' => 'รหัสแสกนลายนิ้วมือ'
        ];
    }

    public function getFullName()
    {
        return $this->pname.$this->fname.' '.$this->lname;
    }

    /**
     * Gets กลุ่มงาน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments()
    {
        return $this->hasOne(Departments::className(), ['dep_id' => 'dep_id']);
    }

    public function getDepName()
    {
        return $this->departments->dep_name;
    }

    /**
     * Gets ระดับงาน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLevels()
    {
        return $this->hasOne(Levels::className(), ['level' => 'level']);
    }

    public function getLevelName()
    {
        return $this->levels->level_name;
    }

    /**
     * Gets ประเภทตำแหน่ง.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTypes()
    {
        return $this->hasOne(Types::className(), ['position_type' => 'position_type']);
    }

    public function getTypeName()
    {
        return $this->types->position_type_name;
    }

    public function getAge()
    {
        $birth_date = strtotime($this->birthdate);
        $now = time();
        $age = $now-$birth_date;
        $a = $age/60/60/24/365.25;
        //$interval = date_diff($dob, date('Y-m-d'));
        return floor($a);
    }

    public function getAgejob()
    {
        $job_date = strtotime($this->start_date);
        $now = time();
        $age = $now-$job_date;
        $a = $age/60/60/24/365.25;
        //$interval = date_diff($dob, date('Y-m-d'));
        return floor($a);
    }

    /**
     * Gets กลุ่มงาน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPsn()
    {
        return $this->hasMany(PsnCheckinout::className(), ['finger_id' => 'PCODE']);
    }

}
