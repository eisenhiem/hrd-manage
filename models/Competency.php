<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "competency".
 *
 * @property int $comp_id
 * @property int $r_id
 * @property int $emp_id
 * @property string $comp_no
 * @property int $evaluate_by
 * @property int|null $level
 * @property int|null $evaluate
 * @property int|null $weight
 * @property int|null $total
 * @property string|null $comment
 */
class Competency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'competency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r_id', 'emp_id', 'core_id', 'evaluate_by'], 'required'],
            [['r_id', 'emp_id','core_id', 'evaluate_by', 'level', 'evaluate', 'weight', 'total'], 'integer'],
            [['comment'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'comp_id' => 'Comp ID',
            'r_id' => 'รอบประเมิน',
            'emp_id' => 'Emp ID',
            'core_id' => 'หัวข้อสมรรถนะ',
            'evaluate_by' => 'ประเมินโดย',
            'level' => 'ระดับความคาดหวัง',
            'evaluate' => 'คะแนน',
            'weight' => 'น้ำหนัก',
            'total' => 'รวม',
            'comment' => 'หมายเหตุ',
        ];
    }
    public function getCore()
    {
        return $this->hasOne(Core::className(), ['core_id' => 'core_id']);
    }

    public function getCoreName()
    {
        return $this->core->core_name;
    }

    /**
     * Gets รอบประเมิน
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRounds()
    {
        return $this->hasOne(Rounds::className(), ['r_id' => 'r_id']);
    }

    public function getRoundName()
    {
        return $this->rounds->r_detail;
    }

}
