<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "psn_checkinout".
 *
 * @property string $PCODE
 * @property string $REGIST_DATE
 * @property string $REGIST_TIME
 * @property string $GET_DATE
 * @property string $GET_TIME
 * @property string $IP_SERVER
 * @property string $CHECKTYPE
 */
class PsnCheckinout extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'psn_checkinout';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PCODE', 'REGIST_DATE', 'REGIST_TIME', 'GET_DATE', 'GET_TIME', 'IP_SERVER'], 'required'],
            [['REGIST_DATE', 'REGIST_TIME', 'GET_DATE', 'GET_TIME'], 'safe'],
            [['PCODE'], 'string', 'max' => 10],
            [['IP_SERVER'], 'string', 'max' => 20],
            [['CHECKTYPE'], 'string', 'max' => 1],
            [['PCODE', 'REGIST_DATE', 'REGIST_TIME'], 'unique', 'targetAttribute' => ['PCODE', 'REGIST_DATE', 'REGIST_TIME']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PCODE' => 'Pcode',
            'REGIST_DATE' => 'Regist Date',
            'REGIST_TIME' => 'Regist Time',
            'GET_DATE' => 'Get Date',
            'GET_TIME' => 'Get Time',
            'IP_SERVER' => 'Ip Server',
            'CHECKTYPE' => 'Checktype',
        ];
    }
    public function getEmp(){
        return $this->hasOne(Employees::className(), ['PCODE' => 'finger_id']);
    }
}
