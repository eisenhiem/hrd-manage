<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work".
 *
 * @property int $w_id
 * @property int|null $emp_id
 * @property string|null $work_date
 * @property int|null $work_time_id
 * @property string|null $work_ot
 * @property int|null $work_type_id
 */
class Works extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'works';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'work_time_id', 'work_type_id'], 'integer'],
            [['work_date'], 'safe'],
            [['work_ot'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'w_id' => 'W ID',
            'emp_id' => 'Emp ID',
            'work_date' => 'Work Date',
            'work_time_id' => 'Work Time ID',
            'work_ot' => 'Work Ot',
            'work_type_id' => 'Work Type ID',
        ];
    }
}
