<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PsnCheckinout;

/**
 * PsnCheckinoutSearch represents the model behind the search form of `app\models\PsnCheckinout`.
 */
class PsnCheckinoutSearch extends PsnCheckinout
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PCODE', 'REGIST_DATE', 'REGIST_TIME', 'GET_DATE', 'GET_TIME', 'IP_SERVER', 'CHECKTYPE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PsnCheckinout::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'REGIST_DATE' => $this->REGIST_DATE,
            'REGIST_TIME' => $this->REGIST_TIME,
            'GET_DATE' => $this->GET_DATE,
            'GET_TIME' => $this->GET_TIME,
        ]);

        $query->andFilterWhere(['like', 'PCODE', $this->PCODE])
            ->andFilterWhere(['like', 'IP_SERVER', $this->IP_SERVER])
            ->andFilterWhere(['like', 'CHECKTYPE', $this->CHECKTYPE]);

        return $dataProvider;
    }
}
