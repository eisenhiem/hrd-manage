<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RepCheckAll;

/**
 * RepCheckAllSearch represents the model behind the search form of `app\models\RepCheckAll`.
 */
class RepCheckAllSearch extends RepCheckAll
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['finger_id', 'fullname', 'dep_name', 'reg_date', 'reg_time', 'check_status', 'status_name'], 'safe'],
            [['dep_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepCheckAll::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dep_id' => $this->dep_id,
            'reg_date' => $this->reg_date,
        ]);

        $query->andFilterWhere(['like', 'finger_id', $this->finger_id])
            ->andFilterWhere(['like', 'fullname', $this->fullname])
            ->andFilterWhere(['like', 'dep_name', $this->dep_name])
            ->andFilterWhere(['like', 'reg_time', $this->reg_time])
            ->andFilterWhere(['like', 'check_status', $this->check_status])
            ->andFilterWhere(['like', 'status_name', $this->status_name]);

        return $dataProvider;
    }
}
