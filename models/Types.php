<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "types".
 *
 * @property int $position_type
 * @property string|null $position_type_name
 */
class Types extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position_type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'position_type' => 'Position Type',
            'position_type_name' => 'ประเภท',
        ];
    }
}
