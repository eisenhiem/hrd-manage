<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Trainings;

/**
 * TrainingsSearch represents the model behind the search form of `app\models\Trainings`.
 */
class TrainingsSearch extends Trainings
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_id', 'r_id', 'achievment', 'service_mind', 'expertise', 'integrity', 'teamwork', 'reading', 'reading_level', 'listening', 'listening_level', 'speaking', 'speaking_level', 'writing', 'wrinting_level', 'ms_access', 'ms_access_level', 'ms_word', 'ms_word_level', 'ms_excel', 'ms_excel_level', 'ms_powerpoint', 'ms_powerpoint_level', 'e_mail', 'e_mail_level', 'other1_level', 'laws', 'rm', 'sm', 'cm', 'pmqa', 'km', 'bs', 'sp', 'hr'], 'integer'],
            [['other1', 'other2'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trainings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'emp_id' => $this->emp_id,
            'r_id' => $this->r_id,
            'achievment' => $this->achievment,
            'service_mind' => $this->service_mind,
            'expertise' => $this->expertise,
            'integrity' => $this->integrity,
            'teamwork' => $this->teamwork,
            'reading' => $this->reading,
            'reading_level' => $this->reading_level,
            'listening' => $this->listening,
            'listening_level' => $this->listening_level,
            'speaking' => $this->speaking,
            'speaking_level' => $this->speaking_level,
            'writing' => $this->writing,
            'wrinting_level' => $this->wrinting_level,
            'ms_word' => $this->ms_word,
            'ms_word_level' => $this->ms_word_level,
            'ms_excel' => $this->ms_excel,
            'ms_excel_level' => $this->ms_excel_level,
            'ms_powerpoint' => $this->ms_powerpoint,
            'ms_powerpoint_level' => $this->ms_powerpoint_level,
            'ms_access' => $this->ms_access,
            'ms_access_level' => $this->ms_access_level,
            'e_mail' => $this->e_mail,
            'e_mail_level' => $this->e_mail_level,
            'other1_level' => $this->other1_level,
            'laws' => $this->laws,
            'rm' => $this->rm,
            'sm' => $this->sm,
            'cm' => $this->cm,
            'pmqa' => $this->pmqa,
            'km' => $this->km,
            'bs' => $this->bs,
            'sp' => $this->sp,
            'hr' => $this->hr,
        ]);

        $query->andFilterWhere(['like', 'other1', $this->other1])
            ->andFilterWhere(['like', 'other2', $this->other2]);

        return $dataProvider;
    }
}
