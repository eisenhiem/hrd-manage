<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kpi;

/**
 * KpiSearch represents the model behind the search form of `app\models\Kpi`.
 */
class KpiSearch extends Kpi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kpi_id', 'r_id', 'emp_id', 'kpi_score', 'kpi_weight', 'kpi_total', 'evaluate_by'], 'integer'],
            [['kpi_no', 'kpi_name', 'kpi_template', 'kpi_active', 'kpi_1', 'kpi_2', 'kpi_3', 'kpi_4', 'kpi_5'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kpi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kpi_id' => $this->kpi_id,
            'r_id' => $this->r_id,
            'emp_id' => $this->emp_id,
            'kpi_score' => $this->kpi_score,
            'kpi_weight' => $this->kpi_weight,
            'kpi_total' => $this->kpi_total,
            'evaluate_by' => $this->evaluate_by,
        ]);

        $query->andFilterWhere(['like', 'kpi_no', $this->kpi_no])
            ->andFilterWhere(['like', 'kpi_name', $this->kpi_name])
            ->andFilterWhere(['like', 'kpi_template', $this->kpi_template])
            ->andFilterWhere(['like', 'kpi_active', $this->kpi_active])
            ->andFilterWhere(['like', 'kpi_1' , $this->kpi_1])
            ->andFilterWhere(['like', 'kpi_2' , $this->kpi_2])
            ->andFilterWhere(['like', 'kpi_3' , $this->kpi_3])
            ->andFilterWhere(['like', 'kpi_4' , $this->kpi_4])
            ->andFilterWhere(['like', 'kpi_5' , $this->kpi_5]);

        return $dataProvider;
    }
}
