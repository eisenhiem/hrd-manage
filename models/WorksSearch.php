<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Works;

/**
 * WorkSearch represents the model behind the search form of `app\models\Work`.
 */
class WorksSearch extends Works
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['w_id', 'emp_id', 'work_time_id', 'work_type_id'], 'integer'],
            [['work_date', 'work_ot'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Works::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id,
            'emp_id' => $this->emp_id,
            'work_date' => $this->work_date,
            'work_time_id' => $this->work_time_id,
            'work_type_id' => $this->work_type_id,
        ]);

        $query->andFilterWhere(['like', 'work_ot', $this->work_ot]);

        return $dataProvider;
    }
}
